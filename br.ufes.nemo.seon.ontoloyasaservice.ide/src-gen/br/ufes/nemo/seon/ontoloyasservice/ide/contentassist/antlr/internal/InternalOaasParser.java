package br.ufes.nemo.seon.ontoloyasservice.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import br.ufes.nemo.seon.ontoloyasservice.services.OaasGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalOaasParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Configuration'", "'{'", "'}'", "'ontology_name:'", "'level:'", "'author:'", "'author_email:'", "'repository:'", "'ontology_short_name:'", "'software:'", "'about:'", "'#'", "'is:'", "'module'", "'.'", "'import'", "'.*'", "'entity'", "'extends'", "':'", "'OneToOne'", "'ManyToMany'", "'OneToMany'", "'function'", "'('", "')'", "','"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalOaasParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalOaasParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalOaasParser.tokenNames; }
    public String getGrammarFileName() { return "InternalOaas.g"; }


    	private OaasGrammarAccess grammarAccess;

    	public void setGrammarAccess(OaasGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleApplication"
    // InternalOaas.g:53:1: entryRuleApplication : ruleApplication EOF ;
    public final void entryRuleApplication() throws RecognitionException {
        try {
            // InternalOaas.g:54:1: ( ruleApplication EOF )
            // InternalOaas.g:55:1: ruleApplication EOF
            {
             before(grammarAccess.getApplicationRule()); 
            pushFollow(FOLLOW_1);
            ruleApplication();

            state._fsp--;

             after(grammarAccess.getApplicationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleApplication"


    // $ANTLR start "ruleApplication"
    // InternalOaas.g:62:1: ruleApplication : ( ( rule__Application__Group__0 ) ) ;
    public final void ruleApplication() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:66:2: ( ( ( rule__Application__Group__0 ) ) )
            // InternalOaas.g:67:2: ( ( rule__Application__Group__0 ) )
            {
            // InternalOaas.g:67:2: ( ( rule__Application__Group__0 ) )
            // InternalOaas.g:68:3: ( rule__Application__Group__0 )
            {
             before(grammarAccess.getApplicationAccess().getGroup()); 
            // InternalOaas.g:69:3: ( rule__Application__Group__0 )
            // InternalOaas.g:69:4: rule__Application__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Application__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getApplicationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleApplication"


    // $ANTLR start "entryRuleConfiguration"
    // InternalOaas.g:78:1: entryRuleConfiguration : ruleConfiguration EOF ;
    public final void entryRuleConfiguration() throws RecognitionException {
        try {
            // InternalOaas.g:79:1: ( ruleConfiguration EOF )
            // InternalOaas.g:80:1: ruleConfiguration EOF
            {
             before(grammarAccess.getConfigurationRule()); 
            pushFollow(FOLLOW_1);
            ruleConfiguration();

            state._fsp--;

             after(grammarAccess.getConfigurationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConfiguration"


    // $ANTLR start "ruleConfiguration"
    // InternalOaas.g:87:1: ruleConfiguration : ( ( rule__Configuration__Group__0 ) ) ;
    public final void ruleConfiguration() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:91:2: ( ( ( rule__Configuration__Group__0 ) ) )
            // InternalOaas.g:92:2: ( ( rule__Configuration__Group__0 ) )
            {
            // InternalOaas.g:92:2: ( ( rule__Configuration__Group__0 ) )
            // InternalOaas.g:93:3: ( rule__Configuration__Group__0 )
            {
             before(grammarAccess.getConfigurationAccess().getGroup()); 
            // InternalOaas.g:94:3: ( rule__Configuration__Group__0 )
            // InternalOaas.g:94:4: rule__Configuration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConfiguration"


    // $ANTLR start "entryRuleOntology"
    // InternalOaas.g:103:1: entryRuleOntology : ruleOntology EOF ;
    public final void entryRuleOntology() throws RecognitionException {
        try {
            // InternalOaas.g:104:1: ( ruleOntology EOF )
            // InternalOaas.g:105:1: ruleOntology EOF
            {
             before(grammarAccess.getOntologyRule()); 
            pushFollow(FOLLOW_1);
            ruleOntology();

            state._fsp--;

             after(grammarAccess.getOntologyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOntology"


    // $ANTLR start "ruleOntology"
    // InternalOaas.g:112:1: ruleOntology : ( ( rule__Ontology__Group__0 ) ) ;
    public final void ruleOntology() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:116:2: ( ( ( rule__Ontology__Group__0 ) ) )
            // InternalOaas.g:117:2: ( ( rule__Ontology__Group__0 ) )
            {
            // InternalOaas.g:117:2: ( ( rule__Ontology__Group__0 ) )
            // InternalOaas.g:118:3: ( rule__Ontology__Group__0 )
            {
             before(grammarAccess.getOntologyAccess().getGroup()); 
            // InternalOaas.g:119:3: ( rule__Ontology__Group__0 )
            // InternalOaas.g:119:4: rule__Ontology__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Ontology__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOntologyAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOntology"


    // $ANTLR start "entryRuleLevel"
    // InternalOaas.g:128:1: entryRuleLevel : ruleLevel EOF ;
    public final void entryRuleLevel() throws RecognitionException {
        try {
            // InternalOaas.g:129:1: ( ruleLevel EOF )
            // InternalOaas.g:130:1: ruleLevel EOF
            {
             before(grammarAccess.getLevelRule()); 
            pushFollow(FOLLOW_1);
            ruleLevel();

            state._fsp--;

             after(grammarAccess.getLevelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLevel"


    // $ANTLR start "ruleLevel"
    // InternalOaas.g:137:1: ruleLevel : ( ( rule__Level__Group__0 ) ) ;
    public final void ruleLevel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:141:2: ( ( ( rule__Level__Group__0 ) ) )
            // InternalOaas.g:142:2: ( ( rule__Level__Group__0 ) )
            {
            // InternalOaas.g:142:2: ( ( rule__Level__Group__0 ) )
            // InternalOaas.g:143:3: ( rule__Level__Group__0 )
            {
             before(grammarAccess.getLevelAccess().getGroup()); 
            // InternalOaas.g:144:3: ( rule__Level__Group__0 )
            // InternalOaas.g:144:4: rule__Level__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Level__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLevelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLevel"


    // $ANTLR start "entryRuleAuthor"
    // InternalOaas.g:153:1: entryRuleAuthor : ruleAuthor EOF ;
    public final void entryRuleAuthor() throws RecognitionException {
        try {
            // InternalOaas.g:154:1: ( ruleAuthor EOF )
            // InternalOaas.g:155:1: ruleAuthor EOF
            {
             before(grammarAccess.getAuthorRule()); 
            pushFollow(FOLLOW_1);
            ruleAuthor();

            state._fsp--;

             after(grammarAccess.getAuthorRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAuthor"


    // $ANTLR start "ruleAuthor"
    // InternalOaas.g:162:1: ruleAuthor : ( ( rule__Author__Group__0 ) ) ;
    public final void ruleAuthor() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:166:2: ( ( ( rule__Author__Group__0 ) ) )
            // InternalOaas.g:167:2: ( ( rule__Author__Group__0 ) )
            {
            // InternalOaas.g:167:2: ( ( rule__Author__Group__0 ) )
            // InternalOaas.g:168:3: ( rule__Author__Group__0 )
            {
             before(grammarAccess.getAuthorAccess().getGroup()); 
            // InternalOaas.g:169:3: ( rule__Author__Group__0 )
            // InternalOaas.g:169:4: rule__Author__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Author__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAuthorAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAuthor"


    // $ANTLR start "entryRuleAuthor_Email"
    // InternalOaas.g:178:1: entryRuleAuthor_Email : ruleAuthor_Email EOF ;
    public final void entryRuleAuthor_Email() throws RecognitionException {
        try {
            // InternalOaas.g:179:1: ( ruleAuthor_Email EOF )
            // InternalOaas.g:180:1: ruleAuthor_Email EOF
            {
             before(grammarAccess.getAuthor_EmailRule()); 
            pushFollow(FOLLOW_1);
            ruleAuthor_Email();

            state._fsp--;

             after(grammarAccess.getAuthor_EmailRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAuthor_Email"


    // $ANTLR start "ruleAuthor_Email"
    // InternalOaas.g:187:1: ruleAuthor_Email : ( ( rule__Author_Email__Group__0 ) ) ;
    public final void ruleAuthor_Email() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:191:2: ( ( ( rule__Author_Email__Group__0 ) ) )
            // InternalOaas.g:192:2: ( ( rule__Author_Email__Group__0 ) )
            {
            // InternalOaas.g:192:2: ( ( rule__Author_Email__Group__0 ) )
            // InternalOaas.g:193:3: ( rule__Author_Email__Group__0 )
            {
             before(grammarAccess.getAuthor_EmailAccess().getGroup()); 
            // InternalOaas.g:194:3: ( rule__Author_Email__Group__0 )
            // InternalOaas.g:194:4: rule__Author_Email__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Author_Email__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAuthor_EmailAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAuthor_Email"


    // $ANTLR start "entryRuleRepository"
    // InternalOaas.g:203:1: entryRuleRepository : ruleRepository EOF ;
    public final void entryRuleRepository() throws RecognitionException {
        try {
            // InternalOaas.g:204:1: ( ruleRepository EOF )
            // InternalOaas.g:205:1: ruleRepository EOF
            {
             before(grammarAccess.getRepositoryRule()); 
            pushFollow(FOLLOW_1);
            ruleRepository();

            state._fsp--;

             after(grammarAccess.getRepositoryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRepository"


    // $ANTLR start "ruleRepository"
    // InternalOaas.g:212:1: ruleRepository : ( ( rule__Repository__Group__0 ) ) ;
    public final void ruleRepository() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:216:2: ( ( ( rule__Repository__Group__0 ) ) )
            // InternalOaas.g:217:2: ( ( rule__Repository__Group__0 ) )
            {
            // InternalOaas.g:217:2: ( ( rule__Repository__Group__0 ) )
            // InternalOaas.g:218:3: ( rule__Repository__Group__0 )
            {
             before(grammarAccess.getRepositoryAccess().getGroup()); 
            // InternalOaas.g:219:3: ( rule__Repository__Group__0 )
            // InternalOaas.g:219:4: rule__Repository__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Repository__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRepositoryAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRepository"


    // $ANTLR start "entryRuleOntologyShortName"
    // InternalOaas.g:228:1: entryRuleOntologyShortName : ruleOntologyShortName EOF ;
    public final void entryRuleOntologyShortName() throws RecognitionException {
        try {
            // InternalOaas.g:229:1: ( ruleOntologyShortName EOF )
            // InternalOaas.g:230:1: ruleOntologyShortName EOF
            {
             before(grammarAccess.getOntologyShortNameRule()); 
            pushFollow(FOLLOW_1);
            ruleOntologyShortName();

            state._fsp--;

             after(grammarAccess.getOntologyShortNameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOntologyShortName"


    // $ANTLR start "ruleOntologyShortName"
    // InternalOaas.g:237:1: ruleOntologyShortName : ( ( rule__OntologyShortName__Group__0 ) ) ;
    public final void ruleOntologyShortName() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:241:2: ( ( ( rule__OntologyShortName__Group__0 ) ) )
            // InternalOaas.g:242:2: ( ( rule__OntologyShortName__Group__0 ) )
            {
            // InternalOaas.g:242:2: ( ( rule__OntologyShortName__Group__0 ) )
            // InternalOaas.g:243:3: ( rule__OntologyShortName__Group__0 )
            {
             before(grammarAccess.getOntologyShortNameAccess().getGroup()); 
            // InternalOaas.g:244:3: ( rule__OntologyShortName__Group__0 )
            // InternalOaas.g:244:4: rule__OntologyShortName__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__OntologyShortName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOntologyShortNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOntologyShortName"


    // $ANTLR start "entryRuleSoftware"
    // InternalOaas.g:253:1: entryRuleSoftware : ruleSoftware EOF ;
    public final void entryRuleSoftware() throws RecognitionException {
        try {
            // InternalOaas.g:254:1: ( ruleSoftware EOF )
            // InternalOaas.g:255:1: ruleSoftware EOF
            {
             before(grammarAccess.getSoftwareRule()); 
            pushFollow(FOLLOW_1);
            ruleSoftware();

            state._fsp--;

             after(grammarAccess.getSoftwareRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSoftware"


    // $ANTLR start "ruleSoftware"
    // InternalOaas.g:262:1: ruleSoftware : ( ( rule__Software__Group__0 ) ) ;
    public final void ruleSoftware() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:266:2: ( ( ( rule__Software__Group__0 ) ) )
            // InternalOaas.g:267:2: ( ( rule__Software__Group__0 ) )
            {
            // InternalOaas.g:267:2: ( ( rule__Software__Group__0 ) )
            // InternalOaas.g:268:3: ( rule__Software__Group__0 )
            {
             before(grammarAccess.getSoftwareAccess().getGroup()); 
            // InternalOaas.g:269:3: ( rule__Software__Group__0 )
            // InternalOaas.g:269:4: rule__Software__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Software__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSoftwareAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSoftware"


    // $ANTLR start "entryRuleAbout"
    // InternalOaas.g:278:1: entryRuleAbout : ruleAbout EOF ;
    public final void entryRuleAbout() throws RecognitionException {
        try {
            // InternalOaas.g:279:1: ( ruleAbout EOF )
            // InternalOaas.g:280:1: ruleAbout EOF
            {
             before(grammarAccess.getAboutRule()); 
            pushFollow(FOLLOW_1);
            ruleAbout();

            state._fsp--;

             after(grammarAccess.getAboutRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAbout"


    // $ANTLR start "ruleAbout"
    // InternalOaas.g:287:1: ruleAbout : ( ( rule__About__Group__0 ) ) ;
    public final void ruleAbout() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:291:2: ( ( ( rule__About__Group__0 ) ) )
            // InternalOaas.g:292:2: ( ( rule__About__Group__0 ) )
            {
            // InternalOaas.g:292:2: ( ( rule__About__Group__0 ) )
            // InternalOaas.g:293:3: ( rule__About__Group__0 )
            {
             before(grammarAccess.getAboutAccess().getGroup()); 
            // InternalOaas.g:294:3: ( rule__About__Group__0 )
            // InternalOaas.g:294:4: rule__About__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__About__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAboutAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAbout"


    // $ANTLR start "entryRuleDescription"
    // InternalOaas.g:303:1: entryRuleDescription : ruleDescription EOF ;
    public final void entryRuleDescription() throws RecognitionException {
        try {
            // InternalOaas.g:304:1: ( ruleDescription EOF )
            // InternalOaas.g:305:1: ruleDescription EOF
            {
             before(grammarAccess.getDescriptionRule()); 
            pushFollow(FOLLOW_1);
            ruleDescription();

            state._fsp--;

             after(grammarAccess.getDescriptionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDescription"


    // $ANTLR start "ruleDescription"
    // InternalOaas.g:312:1: ruleDescription : ( ( rule__Description__Group__0 ) ) ;
    public final void ruleDescription() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:316:2: ( ( ( rule__Description__Group__0 ) ) )
            // InternalOaas.g:317:2: ( ( rule__Description__Group__0 ) )
            {
            // InternalOaas.g:317:2: ( ( rule__Description__Group__0 ) )
            // InternalOaas.g:318:3: ( rule__Description__Group__0 )
            {
             before(grammarAccess.getDescriptionAccess().getGroup()); 
            // InternalOaas.g:319:3: ( rule__Description__Group__0 )
            // InternalOaas.g:319:4: rule__Description__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Description__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDescriptionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDescription"


    // $ANTLR start "entryRuleEntity_Type"
    // InternalOaas.g:328:1: entryRuleEntity_Type : ruleEntity_Type EOF ;
    public final void entryRuleEntity_Type() throws RecognitionException {
        try {
            // InternalOaas.g:329:1: ( ruleEntity_Type EOF )
            // InternalOaas.g:330:1: ruleEntity_Type EOF
            {
             before(grammarAccess.getEntity_TypeRule()); 
            pushFollow(FOLLOW_1);
            ruleEntity_Type();

            state._fsp--;

             after(grammarAccess.getEntity_TypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEntity_Type"


    // $ANTLR start "ruleEntity_Type"
    // InternalOaas.g:337:1: ruleEntity_Type : ( ( rule__Entity_Type__Group__0 ) ) ;
    public final void ruleEntity_Type() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:341:2: ( ( ( rule__Entity_Type__Group__0 ) ) )
            // InternalOaas.g:342:2: ( ( rule__Entity_Type__Group__0 ) )
            {
            // InternalOaas.g:342:2: ( ( rule__Entity_Type__Group__0 ) )
            // InternalOaas.g:343:3: ( rule__Entity_Type__Group__0 )
            {
             before(grammarAccess.getEntity_TypeAccess().getGroup()); 
            // InternalOaas.g:344:3: ( rule__Entity_Type__Group__0 )
            // InternalOaas.g:344:4: rule__Entity_Type__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Entity_Type__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEntity_TypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEntity_Type"


    // $ANTLR start "entryRuleModule"
    // InternalOaas.g:353:1: entryRuleModule : ruleModule EOF ;
    public final void entryRuleModule() throws RecognitionException {
        try {
            // InternalOaas.g:354:1: ( ruleModule EOF )
            // InternalOaas.g:355:1: ruleModule EOF
            {
             before(grammarAccess.getModuleRule()); 
            pushFollow(FOLLOW_1);
            ruleModule();

            state._fsp--;

             after(grammarAccess.getModuleRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModule"


    // $ANTLR start "ruleModule"
    // InternalOaas.g:362:1: ruleModule : ( ( rule__Module__Group__0 ) ) ;
    public final void ruleModule() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:366:2: ( ( ( rule__Module__Group__0 ) ) )
            // InternalOaas.g:367:2: ( ( rule__Module__Group__0 ) )
            {
            // InternalOaas.g:367:2: ( ( rule__Module__Group__0 ) )
            // InternalOaas.g:368:3: ( rule__Module__Group__0 )
            {
             before(grammarAccess.getModuleAccess().getGroup()); 
            // InternalOaas.g:369:3: ( rule__Module__Group__0 )
            // InternalOaas.g:369:4: rule__Module__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Module__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getModuleAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModule"


    // $ANTLR start "entryRuleAbstractElement"
    // InternalOaas.g:378:1: entryRuleAbstractElement : ruleAbstractElement EOF ;
    public final void entryRuleAbstractElement() throws RecognitionException {
        try {
            // InternalOaas.g:379:1: ( ruleAbstractElement EOF )
            // InternalOaas.g:380:1: ruleAbstractElement EOF
            {
             before(grammarAccess.getAbstractElementRule()); 
            pushFollow(FOLLOW_1);
            ruleAbstractElement();

            state._fsp--;

             after(grammarAccess.getAbstractElementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAbstractElement"


    // $ANTLR start "ruleAbstractElement"
    // InternalOaas.g:387:1: ruleAbstractElement : ( ( rule__AbstractElement__Alternatives ) ) ;
    public final void ruleAbstractElement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:391:2: ( ( ( rule__AbstractElement__Alternatives ) ) )
            // InternalOaas.g:392:2: ( ( rule__AbstractElement__Alternatives ) )
            {
            // InternalOaas.g:392:2: ( ( rule__AbstractElement__Alternatives ) )
            // InternalOaas.g:393:3: ( rule__AbstractElement__Alternatives )
            {
             before(grammarAccess.getAbstractElementAccess().getAlternatives()); 
            // InternalOaas.g:394:3: ( rule__AbstractElement__Alternatives )
            // InternalOaas.g:394:4: rule__AbstractElement__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AbstractElement__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAbstractElementAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAbstractElement"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalOaas.g:403:1: entryRuleQualifiedName : ruleQualifiedName EOF ;
    public final void entryRuleQualifiedName() throws RecognitionException {
        try {
            // InternalOaas.g:404:1: ( ruleQualifiedName EOF )
            // InternalOaas.g:405:1: ruleQualifiedName EOF
            {
             before(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalOaas.g:412:1: ruleQualifiedName : ( ( rule__QualifiedName__Group__0 ) ) ;
    public final void ruleQualifiedName() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:416:2: ( ( ( rule__QualifiedName__Group__0 ) ) )
            // InternalOaas.g:417:2: ( ( rule__QualifiedName__Group__0 ) )
            {
            // InternalOaas.g:417:2: ( ( rule__QualifiedName__Group__0 ) )
            // InternalOaas.g:418:3: ( rule__QualifiedName__Group__0 )
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup()); 
            // InternalOaas.g:419:3: ( rule__QualifiedName__Group__0 )
            // InternalOaas.g:419:4: rule__QualifiedName__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleImport"
    // InternalOaas.g:428:1: entryRuleImport : ruleImport EOF ;
    public final void entryRuleImport() throws RecognitionException {
        try {
            // InternalOaas.g:429:1: ( ruleImport EOF )
            // InternalOaas.g:430:1: ruleImport EOF
            {
             before(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getImportRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalOaas.g:437:1: ruleImport : ( ( rule__Import__Group__0 ) ) ;
    public final void ruleImport() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:441:2: ( ( ( rule__Import__Group__0 ) ) )
            // InternalOaas.g:442:2: ( ( rule__Import__Group__0 ) )
            {
            // InternalOaas.g:442:2: ( ( rule__Import__Group__0 ) )
            // InternalOaas.g:443:3: ( rule__Import__Group__0 )
            {
             before(grammarAccess.getImportAccess().getGroup()); 
            // InternalOaas.g:444:3: ( rule__Import__Group__0 )
            // InternalOaas.g:444:4: rule__Import__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // InternalOaas.g:453:1: entryRuleQualifiedNameWithWildcard : ruleQualifiedNameWithWildcard EOF ;
    public final void entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        try {
            // InternalOaas.g:454:1: ( ruleQualifiedNameWithWildcard EOF )
            // InternalOaas.g:455:1: ruleQualifiedNameWithWildcard EOF
            {
             before(grammarAccess.getQualifiedNameWithWildcardRule()); 
            pushFollow(FOLLOW_1);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // InternalOaas.g:462:1: ruleQualifiedNameWithWildcard : ( ( rule__QualifiedNameWithWildcard__Group__0 ) ) ;
    public final void ruleQualifiedNameWithWildcard() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:466:2: ( ( ( rule__QualifiedNameWithWildcard__Group__0 ) ) )
            // InternalOaas.g:467:2: ( ( rule__QualifiedNameWithWildcard__Group__0 ) )
            {
            // InternalOaas.g:467:2: ( ( rule__QualifiedNameWithWildcard__Group__0 ) )
            // InternalOaas.g:468:3: ( rule__QualifiedNameWithWildcard__Group__0 )
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup()); 
            // InternalOaas.g:469:3: ( rule__QualifiedNameWithWildcard__Group__0 )
            // InternalOaas.g:469:4: rule__QualifiedNameWithWildcard__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedNameWithWildcard__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleEntity"
    // InternalOaas.g:478:1: entryRuleEntity : ruleEntity EOF ;
    public final void entryRuleEntity() throws RecognitionException {
        try {
            // InternalOaas.g:479:1: ( ruleEntity EOF )
            // InternalOaas.g:480:1: ruleEntity EOF
            {
             before(grammarAccess.getEntityRule()); 
            pushFollow(FOLLOW_1);
            ruleEntity();

            state._fsp--;

             after(grammarAccess.getEntityRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEntity"


    // $ANTLR start "ruleEntity"
    // InternalOaas.g:487:1: ruleEntity : ( ( rule__Entity__Group__0 ) ) ;
    public final void ruleEntity() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:491:2: ( ( ( rule__Entity__Group__0 ) ) )
            // InternalOaas.g:492:2: ( ( rule__Entity__Group__0 ) )
            {
            // InternalOaas.g:492:2: ( ( rule__Entity__Group__0 ) )
            // InternalOaas.g:493:3: ( rule__Entity__Group__0 )
            {
             before(grammarAccess.getEntityAccess().getGroup()); 
            // InternalOaas.g:494:3: ( rule__Entity__Group__0 )
            // InternalOaas.g:494:4: rule__Entity__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Entity__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEntityAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEntity"


    // $ANTLR start "entryRuleAttribute"
    // InternalOaas.g:503:1: entryRuleAttribute : ruleAttribute EOF ;
    public final void entryRuleAttribute() throws RecognitionException {
        try {
            // InternalOaas.g:504:1: ( ruleAttribute EOF )
            // InternalOaas.g:505:1: ruleAttribute EOF
            {
             before(grammarAccess.getAttributeRule()); 
            pushFollow(FOLLOW_1);
            ruleAttribute();

            state._fsp--;

             after(grammarAccess.getAttributeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // InternalOaas.g:512:1: ruleAttribute : ( ( rule__Attribute__Group__0 ) ) ;
    public final void ruleAttribute() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:516:2: ( ( ( rule__Attribute__Group__0 ) ) )
            // InternalOaas.g:517:2: ( ( rule__Attribute__Group__0 ) )
            {
            // InternalOaas.g:517:2: ( ( rule__Attribute__Group__0 ) )
            // InternalOaas.g:518:3: ( rule__Attribute__Group__0 )
            {
             before(grammarAccess.getAttributeAccess().getGroup()); 
            // InternalOaas.g:519:3: ( rule__Attribute__Group__0 )
            // InternalOaas.g:519:4: rule__Attribute__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Attribute__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRuleRelation"
    // InternalOaas.g:528:1: entryRuleRelation : ruleRelation EOF ;
    public final void entryRuleRelation() throws RecognitionException {
        try {
            // InternalOaas.g:529:1: ( ruleRelation EOF )
            // InternalOaas.g:530:1: ruleRelation EOF
            {
             before(grammarAccess.getRelationRule()); 
            pushFollow(FOLLOW_1);
            ruleRelation();

            state._fsp--;

             after(grammarAccess.getRelationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRelation"


    // $ANTLR start "ruleRelation"
    // InternalOaas.g:537:1: ruleRelation : ( ( rule__Relation__Alternatives ) ) ;
    public final void ruleRelation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:541:2: ( ( ( rule__Relation__Alternatives ) ) )
            // InternalOaas.g:542:2: ( ( rule__Relation__Alternatives ) )
            {
            // InternalOaas.g:542:2: ( ( rule__Relation__Alternatives ) )
            // InternalOaas.g:543:3: ( rule__Relation__Alternatives )
            {
             before(grammarAccess.getRelationAccess().getAlternatives()); 
            // InternalOaas.g:544:3: ( rule__Relation__Alternatives )
            // InternalOaas.g:544:4: rule__Relation__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Relation__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getRelationAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRelation"


    // $ANTLR start "entryRuleOneToOne"
    // InternalOaas.g:553:1: entryRuleOneToOne : ruleOneToOne EOF ;
    public final void entryRuleOneToOne() throws RecognitionException {
        try {
            // InternalOaas.g:554:1: ( ruleOneToOne EOF )
            // InternalOaas.g:555:1: ruleOneToOne EOF
            {
             before(grammarAccess.getOneToOneRule()); 
            pushFollow(FOLLOW_1);
            ruleOneToOne();

            state._fsp--;

             after(grammarAccess.getOneToOneRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOneToOne"


    // $ANTLR start "ruleOneToOne"
    // InternalOaas.g:562:1: ruleOneToOne : ( ( rule__OneToOne__Group__0 ) ) ;
    public final void ruleOneToOne() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:566:2: ( ( ( rule__OneToOne__Group__0 ) ) )
            // InternalOaas.g:567:2: ( ( rule__OneToOne__Group__0 ) )
            {
            // InternalOaas.g:567:2: ( ( rule__OneToOne__Group__0 ) )
            // InternalOaas.g:568:3: ( rule__OneToOne__Group__0 )
            {
             before(grammarAccess.getOneToOneAccess().getGroup()); 
            // InternalOaas.g:569:3: ( rule__OneToOne__Group__0 )
            // InternalOaas.g:569:4: rule__OneToOne__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__OneToOne__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOneToOneAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOneToOne"


    // $ANTLR start "entryRuleManyToMany"
    // InternalOaas.g:578:1: entryRuleManyToMany : ruleManyToMany EOF ;
    public final void entryRuleManyToMany() throws RecognitionException {
        try {
            // InternalOaas.g:579:1: ( ruleManyToMany EOF )
            // InternalOaas.g:580:1: ruleManyToMany EOF
            {
             before(grammarAccess.getManyToManyRule()); 
            pushFollow(FOLLOW_1);
            ruleManyToMany();

            state._fsp--;

             after(grammarAccess.getManyToManyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleManyToMany"


    // $ANTLR start "ruleManyToMany"
    // InternalOaas.g:587:1: ruleManyToMany : ( ( rule__ManyToMany__Group__0 ) ) ;
    public final void ruleManyToMany() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:591:2: ( ( ( rule__ManyToMany__Group__0 ) ) )
            // InternalOaas.g:592:2: ( ( rule__ManyToMany__Group__0 ) )
            {
            // InternalOaas.g:592:2: ( ( rule__ManyToMany__Group__0 ) )
            // InternalOaas.g:593:3: ( rule__ManyToMany__Group__0 )
            {
             before(grammarAccess.getManyToManyAccess().getGroup()); 
            // InternalOaas.g:594:3: ( rule__ManyToMany__Group__0 )
            // InternalOaas.g:594:4: rule__ManyToMany__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ManyToMany__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getManyToManyAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleManyToMany"


    // $ANTLR start "entryRuleOneToMany"
    // InternalOaas.g:603:1: entryRuleOneToMany : ruleOneToMany EOF ;
    public final void entryRuleOneToMany() throws RecognitionException {
        try {
            // InternalOaas.g:604:1: ( ruleOneToMany EOF )
            // InternalOaas.g:605:1: ruleOneToMany EOF
            {
             before(grammarAccess.getOneToManyRule()); 
            pushFollow(FOLLOW_1);
            ruleOneToMany();

            state._fsp--;

             after(grammarAccess.getOneToManyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOneToMany"


    // $ANTLR start "ruleOneToMany"
    // InternalOaas.g:612:1: ruleOneToMany : ( ( rule__OneToMany__Group__0 ) ) ;
    public final void ruleOneToMany() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:616:2: ( ( ( rule__OneToMany__Group__0 ) ) )
            // InternalOaas.g:617:2: ( ( rule__OneToMany__Group__0 ) )
            {
            // InternalOaas.g:617:2: ( ( rule__OneToMany__Group__0 ) )
            // InternalOaas.g:618:3: ( rule__OneToMany__Group__0 )
            {
             before(grammarAccess.getOneToManyAccess().getGroup()); 
            // InternalOaas.g:619:3: ( rule__OneToMany__Group__0 )
            // InternalOaas.g:619:4: rule__OneToMany__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__OneToMany__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOneToManyAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOneToMany"


    // $ANTLR start "entryRuleFunction"
    // InternalOaas.g:628:1: entryRuleFunction : ruleFunction EOF ;
    public final void entryRuleFunction() throws RecognitionException {
        try {
            // InternalOaas.g:629:1: ( ruleFunction EOF )
            // InternalOaas.g:630:1: ruleFunction EOF
            {
             before(grammarAccess.getFunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleFunction();

            state._fsp--;

             after(grammarAccess.getFunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFunction"


    // $ANTLR start "ruleFunction"
    // InternalOaas.g:637:1: ruleFunction : ( ( rule__Function__Group__0 ) ) ;
    public final void ruleFunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:641:2: ( ( ( rule__Function__Group__0 ) ) )
            // InternalOaas.g:642:2: ( ( rule__Function__Group__0 ) )
            {
            // InternalOaas.g:642:2: ( ( rule__Function__Group__0 ) )
            // InternalOaas.g:643:3: ( rule__Function__Group__0 )
            {
             before(grammarAccess.getFunctionAccess().getGroup()); 
            // InternalOaas.g:644:3: ( rule__Function__Group__0 )
            // InternalOaas.g:644:4: rule__Function__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Function__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFunction"


    // $ANTLR start "rule__AbstractElement__Alternatives"
    // InternalOaas.g:652:1: rule__AbstractElement__Alternatives : ( ( ruleModule ) | ( ruleEntity ) | ( ruleImport ) );
    public final void rule__AbstractElement__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:656:1: ( ( ruleModule ) | ( ruleEntity ) | ( ruleImport ) )
            int alt1=3;
            switch ( input.LA(1) ) {
            case 22:
                {
                int LA1_1 = input.LA(2);

                if ( (LA1_1==RULE_STRING) ) {
                    int LA1_5 = input.LA(3);

                    if ( (LA1_5==24) ) {
                        alt1=1;
                    }
                    else if ( (LA1_5==28) ) {
                        alt1=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 1, 5, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 1, input);

                    throw nvae;
                }
                }
                break;
            case 24:
                {
                alt1=1;
                }
                break;
            case 28:
                {
                alt1=2;
                }
                break;
            case 26:
                {
                alt1=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalOaas.g:657:2: ( ruleModule )
                    {
                    // InternalOaas.g:657:2: ( ruleModule )
                    // InternalOaas.g:658:3: ruleModule
                    {
                     before(grammarAccess.getAbstractElementAccess().getModuleParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleModule();

                    state._fsp--;

                     after(grammarAccess.getAbstractElementAccess().getModuleParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalOaas.g:663:2: ( ruleEntity )
                    {
                    // InternalOaas.g:663:2: ( ruleEntity )
                    // InternalOaas.g:664:3: ruleEntity
                    {
                     before(grammarAccess.getAbstractElementAccess().getEntityParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleEntity();

                    state._fsp--;

                     after(grammarAccess.getAbstractElementAccess().getEntityParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalOaas.g:669:2: ( ruleImport )
                    {
                    // InternalOaas.g:669:2: ( ruleImport )
                    // InternalOaas.g:670:3: ruleImport
                    {
                     before(grammarAccess.getAbstractElementAccess().getImportParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleImport();

                    state._fsp--;

                     after(grammarAccess.getAbstractElementAccess().getImportParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AbstractElement__Alternatives"


    // $ANTLR start "rule__Relation__Alternatives"
    // InternalOaas.g:679:1: rule__Relation__Alternatives : ( ( ruleOneToOne ) | ( ruleManyToMany ) | ( ruleOneToMany ) );
    public final void rule__Relation__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:683:1: ( ( ruleOneToOne ) | ( ruleManyToMany ) | ( ruleOneToMany ) )
            int alt2=3;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_ID) ) {
                switch ( input.LA(2) ) {
                case 33:
                    {
                    alt2=3;
                    }
                    break;
                case 32:
                    {
                    alt2=2;
                    }
                    break;
                case 31:
                    {
                    alt2=1;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 1, input);

                    throw nvae;
                }

            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalOaas.g:684:2: ( ruleOneToOne )
                    {
                    // InternalOaas.g:684:2: ( ruleOneToOne )
                    // InternalOaas.g:685:3: ruleOneToOne
                    {
                     before(grammarAccess.getRelationAccess().getOneToOneParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleOneToOne();

                    state._fsp--;

                     after(grammarAccess.getRelationAccess().getOneToOneParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalOaas.g:690:2: ( ruleManyToMany )
                    {
                    // InternalOaas.g:690:2: ( ruleManyToMany )
                    // InternalOaas.g:691:3: ruleManyToMany
                    {
                     before(grammarAccess.getRelationAccess().getManyToManyParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleManyToMany();

                    state._fsp--;

                     after(grammarAccess.getRelationAccess().getManyToManyParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalOaas.g:696:2: ( ruleOneToMany )
                    {
                    // InternalOaas.g:696:2: ( ruleOneToMany )
                    // InternalOaas.g:697:3: ruleOneToMany
                    {
                     before(grammarAccess.getRelationAccess().getOneToManyParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleOneToMany();

                    state._fsp--;

                     after(grammarAccess.getRelationAccess().getOneToManyParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Alternatives"


    // $ANTLR start "rule__Application__Group__0"
    // InternalOaas.g:706:1: rule__Application__Group__0 : rule__Application__Group__0__Impl rule__Application__Group__1 ;
    public final void rule__Application__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:710:1: ( rule__Application__Group__0__Impl rule__Application__Group__1 )
            // InternalOaas.g:711:2: rule__Application__Group__0__Impl rule__Application__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Application__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Application__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Application__Group__0"


    // $ANTLR start "rule__Application__Group__0__Impl"
    // InternalOaas.g:718:1: rule__Application__Group__0__Impl : ( ( rule__Application__ConfigurationAssignment_0 )? ) ;
    public final void rule__Application__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:722:1: ( ( ( rule__Application__ConfigurationAssignment_0 )? ) )
            // InternalOaas.g:723:1: ( ( rule__Application__ConfigurationAssignment_0 )? )
            {
            // InternalOaas.g:723:1: ( ( rule__Application__ConfigurationAssignment_0 )? )
            // InternalOaas.g:724:2: ( rule__Application__ConfigurationAssignment_0 )?
            {
             before(grammarAccess.getApplicationAccess().getConfigurationAssignment_0()); 
            // InternalOaas.g:725:2: ( rule__Application__ConfigurationAssignment_0 )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==11) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalOaas.g:725:3: rule__Application__ConfigurationAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Application__ConfigurationAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getApplicationAccess().getConfigurationAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Application__Group__0__Impl"


    // $ANTLR start "rule__Application__Group__1"
    // InternalOaas.g:733:1: rule__Application__Group__1 : rule__Application__Group__1__Impl ;
    public final void rule__Application__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:737:1: ( rule__Application__Group__1__Impl )
            // InternalOaas.g:738:2: rule__Application__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Application__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Application__Group__1"


    // $ANTLR start "rule__Application__Group__1__Impl"
    // InternalOaas.g:744:1: rule__Application__Group__1__Impl : ( ( rule__Application__AbstractElementsAssignment_1 )* ) ;
    public final void rule__Application__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:748:1: ( ( ( rule__Application__AbstractElementsAssignment_1 )* ) )
            // InternalOaas.g:749:1: ( ( rule__Application__AbstractElementsAssignment_1 )* )
            {
            // InternalOaas.g:749:1: ( ( rule__Application__AbstractElementsAssignment_1 )* )
            // InternalOaas.g:750:2: ( rule__Application__AbstractElementsAssignment_1 )*
            {
             before(grammarAccess.getApplicationAccess().getAbstractElementsAssignment_1()); 
            // InternalOaas.g:751:2: ( rule__Application__AbstractElementsAssignment_1 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==22||LA4_0==24||LA4_0==26||LA4_0==28) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalOaas.g:751:3: rule__Application__AbstractElementsAssignment_1
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__Application__AbstractElementsAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getApplicationAccess().getAbstractElementsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Application__Group__1__Impl"


    // $ANTLR start "rule__Configuration__Group__0"
    // InternalOaas.g:760:1: rule__Configuration__Group__0 : rule__Configuration__Group__0__Impl rule__Configuration__Group__1 ;
    public final void rule__Configuration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:764:1: ( rule__Configuration__Group__0__Impl rule__Configuration__Group__1 )
            // InternalOaas.g:765:2: rule__Configuration__Group__0__Impl rule__Configuration__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Configuration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__0"


    // $ANTLR start "rule__Configuration__Group__0__Impl"
    // InternalOaas.g:772:1: rule__Configuration__Group__0__Impl : ( 'Configuration' ) ;
    public final void rule__Configuration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:776:1: ( ( 'Configuration' ) )
            // InternalOaas.g:777:1: ( 'Configuration' )
            {
            // InternalOaas.g:777:1: ( 'Configuration' )
            // InternalOaas.g:778:2: 'Configuration'
            {
             before(grammarAccess.getConfigurationAccess().getConfigurationKeyword_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getConfigurationKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__0__Impl"


    // $ANTLR start "rule__Configuration__Group__1"
    // InternalOaas.g:787:1: rule__Configuration__Group__1 : rule__Configuration__Group__1__Impl rule__Configuration__Group__2 ;
    public final void rule__Configuration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:791:1: ( rule__Configuration__Group__1__Impl rule__Configuration__Group__2 )
            // InternalOaas.g:792:2: rule__Configuration__Group__1__Impl rule__Configuration__Group__2
            {
            pushFollow(FOLLOW_6);
            rule__Configuration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__1"


    // $ANTLR start "rule__Configuration__Group__1__Impl"
    // InternalOaas.g:799:1: rule__Configuration__Group__1__Impl : ( '{' ) ;
    public final void rule__Configuration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:803:1: ( ( '{' ) )
            // InternalOaas.g:804:1: ( '{' )
            {
            // InternalOaas.g:804:1: ( '{' )
            // InternalOaas.g:805:2: '{'
            {
             before(grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__1__Impl"


    // $ANTLR start "rule__Configuration__Group__2"
    // InternalOaas.g:814:1: rule__Configuration__Group__2 : rule__Configuration__Group__2__Impl rule__Configuration__Group__3 ;
    public final void rule__Configuration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:818:1: ( rule__Configuration__Group__2__Impl rule__Configuration__Group__3 )
            // InternalOaas.g:819:2: rule__Configuration__Group__2__Impl rule__Configuration__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__Configuration__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__2"


    // $ANTLR start "rule__Configuration__Group__2__Impl"
    // InternalOaas.g:826:1: rule__Configuration__Group__2__Impl : ( ( rule__Configuration__OntologyAssignment_2 ) ) ;
    public final void rule__Configuration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:830:1: ( ( ( rule__Configuration__OntologyAssignment_2 ) ) )
            // InternalOaas.g:831:1: ( ( rule__Configuration__OntologyAssignment_2 ) )
            {
            // InternalOaas.g:831:1: ( ( rule__Configuration__OntologyAssignment_2 ) )
            // InternalOaas.g:832:2: ( rule__Configuration__OntologyAssignment_2 )
            {
             before(grammarAccess.getConfigurationAccess().getOntologyAssignment_2()); 
            // InternalOaas.g:833:2: ( rule__Configuration__OntologyAssignment_2 )
            // InternalOaas.g:833:3: rule__Configuration__OntologyAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__OntologyAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getOntologyAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__2__Impl"


    // $ANTLR start "rule__Configuration__Group__3"
    // InternalOaas.g:841:1: rule__Configuration__Group__3 : rule__Configuration__Group__3__Impl rule__Configuration__Group__4 ;
    public final void rule__Configuration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:845:1: ( rule__Configuration__Group__3__Impl rule__Configuration__Group__4 )
            // InternalOaas.g:846:2: rule__Configuration__Group__3__Impl rule__Configuration__Group__4
            {
            pushFollow(FOLLOW_8);
            rule__Configuration__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__3"


    // $ANTLR start "rule__Configuration__Group__3__Impl"
    // InternalOaas.g:853:1: rule__Configuration__Group__3__Impl : ( ( rule__Configuration__OntologyShortNameAssignment_3 ) ) ;
    public final void rule__Configuration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:857:1: ( ( ( rule__Configuration__OntologyShortNameAssignment_3 ) ) )
            // InternalOaas.g:858:1: ( ( rule__Configuration__OntologyShortNameAssignment_3 ) )
            {
            // InternalOaas.g:858:1: ( ( rule__Configuration__OntologyShortNameAssignment_3 ) )
            // InternalOaas.g:859:2: ( rule__Configuration__OntologyShortNameAssignment_3 )
            {
             before(grammarAccess.getConfigurationAccess().getOntologyShortNameAssignment_3()); 
            // InternalOaas.g:860:2: ( rule__Configuration__OntologyShortNameAssignment_3 )
            // InternalOaas.g:860:3: rule__Configuration__OntologyShortNameAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__OntologyShortNameAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getOntologyShortNameAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__3__Impl"


    // $ANTLR start "rule__Configuration__Group__4"
    // InternalOaas.g:868:1: rule__Configuration__Group__4 : rule__Configuration__Group__4__Impl rule__Configuration__Group__5 ;
    public final void rule__Configuration__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:872:1: ( rule__Configuration__Group__4__Impl rule__Configuration__Group__5 )
            // InternalOaas.g:873:2: rule__Configuration__Group__4__Impl rule__Configuration__Group__5
            {
            pushFollow(FOLLOW_9);
            rule__Configuration__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__4"


    // $ANTLR start "rule__Configuration__Group__4__Impl"
    // InternalOaas.g:880:1: rule__Configuration__Group__4__Impl : ( ( rule__Configuration__LevelAssignment_4 ) ) ;
    public final void rule__Configuration__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:884:1: ( ( ( rule__Configuration__LevelAssignment_4 ) ) )
            // InternalOaas.g:885:1: ( ( rule__Configuration__LevelAssignment_4 ) )
            {
            // InternalOaas.g:885:1: ( ( rule__Configuration__LevelAssignment_4 ) )
            // InternalOaas.g:886:2: ( rule__Configuration__LevelAssignment_4 )
            {
             before(grammarAccess.getConfigurationAccess().getLevelAssignment_4()); 
            // InternalOaas.g:887:2: ( rule__Configuration__LevelAssignment_4 )
            // InternalOaas.g:887:3: rule__Configuration__LevelAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__LevelAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getLevelAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__4__Impl"


    // $ANTLR start "rule__Configuration__Group__5"
    // InternalOaas.g:895:1: rule__Configuration__Group__5 : rule__Configuration__Group__5__Impl rule__Configuration__Group__6 ;
    public final void rule__Configuration__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:899:1: ( rule__Configuration__Group__5__Impl rule__Configuration__Group__6 )
            // InternalOaas.g:900:2: rule__Configuration__Group__5__Impl rule__Configuration__Group__6
            {
            pushFollow(FOLLOW_10);
            rule__Configuration__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__5"


    // $ANTLR start "rule__Configuration__Group__5__Impl"
    // InternalOaas.g:907:1: rule__Configuration__Group__5__Impl : ( ( rule__Configuration__AboutAssignment_5 ) ) ;
    public final void rule__Configuration__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:911:1: ( ( ( rule__Configuration__AboutAssignment_5 ) ) )
            // InternalOaas.g:912:1: ( ( rule__Configuration__AboutAssignment_5 ) )
            {
            // InternalOaas.g:912:1: ( ( rule__Configuration__AboutAssignment_5 ) )
            // InternalOaas.g:913:2: ( rule__Configuration__AboutAssignment_5 )
            {
             before(grammarAccess.getConfigurationAccess().getAboutAssignment_5()); 
            // InternalOaas.g:914:2: ( rule__Configuration__AboutAssignment_5 )
            // InternalOaas.g:914:3: rule__Configuration__AboutAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__AboutAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getAboutAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__5__Impl"


    // $ANTLR start "rule__Configuration__Group__6"
    // InternalOaas.g:922:1: rule__Configuration__Group__6 : rule__Configuration__Group__6__Impl rule__Configuration__Group__7 ;
    public final void rule__Configuration__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:926:1: ( rule__Configuration__Group__6__Impl rule__Configuration__Group__7 )
            // InternalOaas.g:927:2: rule__Configuration__Group__6__Impl rule__Configuration__Group__7
            {
            pushFollow(FOLLOW_11);
            rule__Configuration__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__6"


    // $ANTLR start "rule__Configuration__Group__6__Impl"
    // InternalOaas.g:934:1: rule__Configuration__Group__6__Impl : ( ( rule__Configuration__SoftwareAssignment_6 ) ) ;
    public final void rule__Configuration__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:938:1: ( ( ( rule__Configuration__SoftwareAssignment_6 ) ) )
            // InternalOaas.g:939:1: ( ( rule__Configuration__SoftwareAssignment_6 ) )
            {
            // InternalOaas.g:939:1: ( ( rule__Configuration__SoftwareAssignment_6 ) )
            // InternalOaas.g:940:2: ( rule__Configuration__SoftwareAssignment_6 )
            {
             before(grammarAccess.getConfigurationAccess().getSoftwareAssignment_6()); 
            // InternalOaas.g:941:2: ( rule__Configuration__SoftwareAssignment_6 )
            // InternalOaas.g:941:3: rule__Configuration__SoftwareAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__SoftwareAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getSoftwareAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__6__Impl"


    // $ANTLR start "rule__Configuration__Group__7"
    // InternalOaas.g:949:1: rule__Configuration__Group__7 : rule__Configuration__Group__7__Impl rule__Configuration__Group__8 ;
    public final void rule__Configuration__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:953:1: ( rule__Configuration__Group__7__Impl rule__Configuration__Group__8 )
            // InternalOaas.g:954:2: rule__Configuration__Group__7__Impl rule__Configuration__Group__8
            {
            pushFollow(FOLLOW_12);
            rule__Configuration__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__7"


    // $ANTLR start "rule__Configuration__Group__7__Impl"
    // InternalOaas.g:961:1: rule__Configuration__Group__7__Impl : ( ( rule__Configuration__AuthorAssignment_7 ) ) ;
    public final void rule__Configuration__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:965:1: ( ( ( rule__Configuration__AuthorAssignment_7 ) ) )
            // InternalOaas.g:966:1: ( ( rule__Configuration__AuthorAssignment_7 ) )
            {
            // InternalOaas.g:966:1: ( ( rule__Configuration__AuthorAssignment_7 ) )
            // InternalOaas.g:967:2: ( rule__Configuration__AuthorAssignment_7 )
            {
             before(grammarAccess.getConfigurationAccess().getAuthorAssignment_7()); 
            // InternalOaas.g:968:2: ( rule__Configuration__AuthorAssignment_7 )
            // InternalOaas.g:968:3: rule__Configuration__AuthorAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__AuthorAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getAuthorAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__7__Impl"


    // $ANTLR start "rule__Configuration__Group__8"
    // InternalOaas.g:976:1: rule__Configuration__Group__8 : rule__Configuration__Group__8__Impl rule__Configuration__Group__9 ;
    public final void rule__Configuration__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:980:1: ( rule__Configuration__Group__8__Impl rule__Configuration__Group__9 )
            // InternalOaas.g:981:2: rule__Configuration__Group__8__Impl rule__Configuration__Group__9
            {
            pushFollow(FOLLOW_13);
            rule__Configuration__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__8"


    // $ANTLR start "rule__Configuration__Group__8__Impl"
    // InternalOaas.g:988:1: rule__Configuration__Group__8__Impl : ( ( rule__Configuration__Author_emailAssignment_8 ) ) ;
    public final void rule__Configuration__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:992:1: ( ( ( rule__Configuration__Author_emailAssignment_8 ) ) )
            // InternalOaas.g:993:1: ( ( rule__Configuration__Author_emailAssignment_8 ) )
            {
            // InternalOaas.g:993:1: ( ( rule__Configuration__Author_emailAssignment_8 ) )
            // InternalOaas.g:994:2: ( rule__Configuration__Author_emailAssignment_8 )
            {
             before(grammarAccess.getConfigurationAccess().getAuthor_emailAssignment_8()); 
            // InternalOaas.g:995:2: ( rule__Configuration__Author_emailAssignment_8 )
            // InternalOaas.g:995:3: rule__Configuration__Author_emailAssignment_8
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__Author_emailAssignment_8();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getAuthor_emailAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__8__Impl"


    // $ANTLR start "rule__Configuration__Group__9"
    // InternalOaas.g:1003:1: rule__Configuration__Group__9 : rule__Configuration__Group__9__Impl rule__Configuration__Group__10 ;
    public final void rule__Configuration__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1007:1: ( rule__Configuration__Group__9__Impl rule__Configuration__Group__10 )
            // InternalOaas.g:1008:2: rule__Configuration__Group__9__Impl rule__Configuration__Group__10
            {
            pushFollow(FOLLOW_14);
            rule__Configuration__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__9"


    // $ANTLR start "rule__Configuration__Group__9__Impl"
    // InternalOaas.g:1015:1: rule__Configuration__Group__9__Impl : ( ( rule__Configuration__RepositoryAssignment_9 ) ) ;
    public final void rule__Configuration__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1019:1: ( ( ( rule__Configuration__RepositoryAssignment_9 ) ) )
            // InternalOaas.g:1020:1: ( ( rule__Configuration__RepositoryAssignment_9 ) )
            {
            // InternalOaas.g:1020:1: ( ( rule__Configuration__RepositoryAssignment_9 ) )
            // InternalOaas.g:1021:2: ( rule__Configuration__RepositoryAssignment_9 )
            {
             before(grammarAccess.getConfigurationAccess().getRepositoryAssignment_9()); 
            // InternalOaas.g:1022:2: ( rule__Configuration__RepositoryAssignment_9 )
            // InternalOaas.g:1022:3: rule__Configuration__RepositoryAssignment_9
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__RepositoryAssignment_9();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getRepositoryAssignment_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__9__Impl"


    // $ANTLR start "rule__Configuration__Group__10"
    // InternalOaas.g:1030:1: rule__Configuration__Group__10 : rule__Configuration__Group__10__Impl ;
    public final void rule__Configuration__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1034:1: ( rule__Configuration__Group__10__Impl )
            // InternalOaas.g:1035:2: rule__Configuration__Group__10__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__Group__10__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__10"


    // $ANTLR start "rule__Configuration__Group__10__Impl"
    // InternalOaas.g:1041:1: rule__Configuration__Group__10__Impl : ( '}' ) ;
    public final void rule__Configuration__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1045:1: ( ( '}' ) )
            // InternalOaas.g:1046:1: ( '}' )
            {
            // InternalOaas.g:1046:1: ( '}' )
            // InternalOaas.g:1047:2: '}'
            {
             before(grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_10()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__10__Impl"


    // $ANTLR start "rule__Ontology__Group__0"
    // InternalOaas.g:1057:1: rule__Ontology__Group__0 : rule__Ontology__Group__0__Impl rule__Ontology__Group__1 ;
    public final void rule__Ontology__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1061:1: ( rule__Ontology__Group__0__Impl rule__Ontology__Group__1 )
            // InternalOaas.g:1062:2: rule__Ontology__Group__0__Impl rule__Ontology__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__Ontology__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Ontology__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ontology__Group__0"


    // $ANTLR start "rule__Ontology__Group__0__Impl"
    // InternalOaas.g:1069:1: rule__Ontology__Group__0__Impl : ( 'ontology_name:' ) ;
    public final void rule__Ontology__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1073:1: ( ( 'ontology_name:' ) )
            // InternalOaas.g:1074:1: ( 'ontology_name:' )
            {
            // InternalOaas.g:1074:1: ( 'ontology_name:' )
            // InternalOaas.g:1075:2: 'ontology_name:'
            {
             before(grammarAccess.getOntologyAccess().getOntology_nameKeyword_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getOntologyAccess().getOntology_nameKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ontology__Group__0__Impl"


    // $ANTLR start "rule__Ontology__Group__1"
    // InternalOaas.g:1084:1: rule__Ontology__Group__1 : rule__Ontology__Group__1__Impl ;
    public final void rule__Ontology__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1088:1: ( rule__Ontology__Group__1__Impl )
            // InternalOaas.g:1089:2: rule__Ontology__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Ontology__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ontology__Group__1"


    // $ANTLR start "rule__Ontology__Group__1__Impl"
    // InternalOaas.g:1095:1: rule__Ontology__Group__1__Impl : ( ( rule__Ontology__NameAssignment_1 ) ) ;
    public final void rule__Ontology__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1099:1: ( ( ( rule__Ontology__NameAssignment_1 ) ) )
            // InternalOaas.g:1100:1: ( ( rule__Ontology__NameAssignment_1 ) )
            {
            // InternalOaas.g:1100:1: ( ( rule__Ontology__NameAssignment_1 ) )
            // InternalOaas.g:1101:2: ( rule__Ontology__NameAssignment_1 )
            {
             before(grammarAccess.getOntologyAccess().getNameAssignment_1()); 
            // InternalOaas.g:1102:2: ( rule__Ontology__NameAssignment_1 )
            // InternalOaas.g:1102:3: rule__Ontology__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Ontology__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getOntologyAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ontology__Group__1__Impl"


    // $ANTLR start "rule__Level__Group__0"
    // InternalOaas.g:1111:1: rule__Level__Group__0 : rule__Level__Group__0__Impl rule__Level__Group__1 ;
    public final void rule__Level__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1115:1: ( rule__Level__Group__0__Impl rule__Level__Group__1 )
            // InternalOaas.g:1116:2: rule__Level__Group__0__Impl rule__Level__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__Level__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Level__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Level__Group__0"


    // $ANTLR start "rule__Level__Group__0__Impl"
    // InternalOaas.g:1123:1: rule__Level__Group__0__Impl : ( 'level:' ) ;
    public final void rule__Level__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1127:1: ( ( 'level:' ) )
            // InternalOaas.g:1128:1: ( 'level:' )
            {
            // InternalOaas.g:1128:1: ( 'level:' )
            // InternalOaas.g:1129:2: 'level:'
            {
             before(grammarAccess.getLevelAccess().getLevelKeyword_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getLevelAccess().getLevelKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Level__Group__0__Impl"


    // $ANTLR start "rule__Level__Group__1"
    // InternalOaas.g:1138:1: rule__Level__Group__1 : rule__Level__Group__1__Impl ;
    public final void rule__Level__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1142:1: ( rule__Level__Group__1__Impl )
            // InternalOaas.g:1143:2: rule__Level__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Level__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Level__Group__1"


    // $ANTLR start "rule__Level__Group__1__Impl"
    // InternalOaas.g:1149:1: rule__Level__Group__1__Impl : ( ( rule__Level__NameAssignment_1 ) ) ;
    public final void rule__Level__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1153:1: ( ( ( rule__Level__NameAssignment_1 ) ) )
            // InternalOaas.g:1154:1: ( ( rule__Level__NameAssignment_1 ) )
            {
            // InternalOaas.g:1154:1: ( ( rule__Level__NameAssignment_1 ) )
            // InternalOaas.g:1155:2: ( rule__Level__NameAssignment_1 )
            {
             before(grammarAccess.getLevelAccess().getNameAssignment_1()); 
            // InternalOaas.g:1156:2: ( rule__Level__NameAssignment_1 )
            // InternalOaas.g:1156:3: rule__Level__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Level__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getLevelAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Level__Group__1__Impl"


    // $ANTLR start "rule__Author__Group__0"
    // InternalOaas.g:1165:1: rule__Author__Group__0 : rule__Author__Group__0__Impl rule__Author__Group__1 ;
    public final void rule__Author__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1169:1: ( rule__Author__Group__0__Impl rule__Author__Group__1 )
            // InternalOaas.g:1170:2: rule__Author__Group__0__Impl rule__Author__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__Author__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Author__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Author__Group__0"


    // $ANTLR start "rule__Author__Group__0__Impl"
    // InternalOaas.g:1177:1: rule__Author__Group__0__Impl : ( 'author:' ) ;
    public final void rule__Author__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1181:1: ( ( 'author:' ) )
            // InternalOaas.g:1182:1: ( 'author:' )
            {
            // InternalOaas.g:1182:1: ( 'author:' )
            // InternalOaas.g:1183:2: 'author:'
            {
             before(grammarAccess.getAuthorAccess().getAuthorKeyword_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getAuthorAccess().getAuthorKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Author__Group__0__Impl"


    // $ANTLR start "rule__Author__Group__1"
    // InternalOaas.g:1192:1: rule__Author__Group__1 : rule__Author__Group__1__Impl ;
    public final void rule__Author__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1196:1: ( rule__Author__Group__1__Impl )
            // InternalOaas.g:1197:2: rule__Author__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Author__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Author__Group__1"


    // $ANTLR start "rule__Author__Group__1__Impl"
    // InternalOaas.g:1203:1: rule__Author__Group__1__Impl : ( ( rule__Author__NameAssignment_1 ) ) ;
    public final void rule__Author__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1207:1: ( ( ( rule__Author__NameAssignment_1 ) ) )
            // InternalOaas.g:1208:1: ( ( rule__Author__NameAssignment_1 ) )
            {
            // InternalOaas.g:1208:1: ( ( rule__Author__NameAssignment_1 ) )
            // InternalOaas.g:1209:2: ( rule__Author__NameAssignment_1 )
            {
             before(grammarAccess.getAuthorAccess().getNameAssignment_1()); 
            // InternalOaas.g:1210:2: ( rule__Author__NameAssignment_1 )
            // InternalOaas.g:1210:3: rule__Author__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Author__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAuthorAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Author__Group__1__Impl"


    // $ANTLR start "rule__Author_Email__Group__0"
    // InternalOaas.g:1219:1: rule__Author_Email__Group__0 : rule__Author_Email__Group__0__Impl rule__Author_Email__Group__1 ;
    public final void rule__Author_Email__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1223:1: ( rule__Author_Email__Group__0__Impl rule__Author_Email__Group__1 )
            // InternalOaas.g:1224:2: rule__Author_Email__Group__0__Impl rule__Author_Email__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__Author_Email__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Author_Email__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Author_Email__Group__0"


    // $ANTLR start "rule__Author_Email__Group__0__Impl"
    // InternalOaas.g:1231:1: rule__Author_Email__Group__0__Impl : ( 'author_email:' ) ;
    public final void rule__Author_Email__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1235:1: ( ( 'author_email:' ) )
            // InternalOaas.g:1236:1: ( 'author_email:' )
            {
            // InternalOaas.g:1236:1: ( 'author_email:' )
            // InternalOaas.g:1237:2: 'author_email:'
            {
             before(grammarAccess.getAuthor_EmailAccess().getAuthor_emailKeyword_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getAuthor_EmailAccess().getAuthor_emailKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Author_Email__Group__0__Impl"


    // $ANTLR start "rule__Author_Email__Group__1"
    // InternalOaas.g:1246:1: rule__Author_Email__Group__1 : rule__Author_Email__Group__1__Impl ;
    public final void rule__Author_Email__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1250:1: ( rule__Author_Email__Group__1__Impl )
            // InternalOaas.g:1251:2: rule__Author_Email__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Author_Email__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Author_Email__Group__1"


    // $ANTLR start "rule__Author_Email__Group__1__Impl"
    // InternalOaas.g:1257:1: rule__Author_Email__Group__1__Impl : ( ( rule__Author_Email__NameAssignment_1 ) ) ;
    public final void rule__Author_Email__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1261:1: ( ( ( rule__Author_Email__NameAssignment_1 ) ) )
            // InternalOaas.g:1262:1: ( ( rule__Author_Email__NameAssignment_1 ) )
            {
            // InternalOaas.g:1262:1: ( ( rule__Author_Email__NameAssignment_1 ) )
            // InternalOaas.g:1263:2: ( rule__Author_Email__NameAssignment_1 )
            {
             before(grammarAccess.getAuthor_EmailAccess().getNameAssignment_1()); 
            // InternalOaas.g:1264:2: ( rule__Author_Email__NameAssignment_1 )
            // InternalOaas.g:1264:3: rule__Author_Email__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Author_Email__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAuthor_EmailAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Author_Email__Group__1__Impl"


    // $ANTLR start "rule__Repository__Group__0"
    // InternalOaas.g:1273:1: rule__Repository__Group__0 : rule__Repository__Group__0__Impl rule__Repository__Group__1 ;
    public final void rule__Repository__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1277:1: ( rule__Repository__Group__0__Impl rule__Repository__Group__1 )
            // InternalOaas.g:1278:2: rule__Repository__Group__0__Impl rule__Repository__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__Repository__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Repository__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group__0"


    // $ANTLR start "rule__Repository__Group__0__Impl"
    // InternalOaas.g:1285:1: rule__Repository__Group__0__Impl : ( 'repository:' ) ;
    public final void rule__Repository__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1289:1: ( ( 'repository:' ) )
            // InternalOaas.g:1290:1: ( 'repository:' )
            {
            // InternalOaas.g:1290:1: ( 'repository:' )
            // InternalOaas.g:1291:2: 'repository:'
            {
             before(grammarAccess.getRepositoryAccess().getRepositoryKeyword_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getRepositoryAccess().getRepositoryKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group__0__Impl"


    // $ANTLR start "rule__Repository__Group__1"
    // InternalOaas.g:1300:1: rule__Repository__Group__1 : rule__Repository__Group__1__Impl ;
    public final void rule__Repository__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1304:1: ( rule__Repository__Group__1__Impl )
            // InternalOaas.g:1305:2: rule__Repository__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Repository__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group__1"


    // $ANTLR start "rule__Repository__Group__1__Impl"
    // InternalOaas.g:1311:1: rule__Repository__Group__1__Impl : ( ( rule__Repository__NameAssignment_1 ) ) ;
    public final void rule__Repository__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1315:1: ( ( ( rule__Repository__NameAssignment_1 ) ) )
            // InternalOaas.g:1316:1: ( ( rule__Repository__NameAssignment_1 ) )
            {
            // InternalOaas.g:1316:1: ( ( rule__Repository__NameAssignment_1 ) )
            // InternalOaas.g:1317:2: ( rule__Repository__NameAssignment_1 )
            {
             before(grammarAccess.getRepositoryAccess().getNameAssignment_1()); 
            // InternalOaas.g:1318:2: ( rule__Repository__NameAssignment_1 )
            // InternalOaas.g:1318:3: rule__Repository__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Repository__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getRepositoryAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group__1__Impl"


    // $ANTLR start "rule__OntologyShortName__Group__0"
    // InternalOaas.g:1327:1: rule__OntologyShortName__Group__0 : rule__OntologyShortName__Group__0__Impl rule__OntologyShortName__Group__1 ;
    public final void rule__OntologyShortName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1331:1: ( rule__OntologyShortName__Group__0__Impl rule__OntologyShortName__Group__1 )
            // InternalOaas.g:1332:2: rule__OntologyShortName__Group__0__Impl rule__OntologyShortName__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__OntologyShortName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OntologyShortName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OntologyShortName__Group__0"


    // $ANTLR start "rule__OntologyShortName__Group__0__Impl"
    // InternalOaas.g:1339:1: rule__OntologyShortName__Group__0__Impl : ( 'ontology_short_name:' ) ;
    public final void rule__OntologyShortName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1343:1: ( ( 'ontology_short_name:' ) )
            // InternalOaas.g:1344:1: ( 'ontology_short_name:' )
            {
            // InternalOaas.g:1344:1: ( 'ontology_short_name:' )
            // InternalOaas.g:1345:2: 'ontology_short_name:'
            {
             before(grammarAccess.getOntologyShortNameAccess().getOntology_short_nameKeyword_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getOntologyShortNameAccess().getOntology_short_nameKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OntologyShortName__Group__0__Impl"


    // $ANTLR start "rule__OntologyShortName__Group__1"
    // InternalOaas.g:1354:1: rule__OntologyShortName__Group__1 : rule__OntologyShortName__Group__1__Impl ;
    public final void rule__OntologyShortName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1358:1: ( rule__OntologyShortName__Group__1__Impl )
            // InternalOaas.g:1359:2: rule__OntologyShortName__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OntologyShortName__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OntologyShortName__Group__1"


    // $ANTLR start "rule__OntologyShortName__Group__1__Impl"
    // InternalOaas.g:1365:1: rule__OntologyShortName__Group__1__Impl : ( ( rule__OntologyShortName__NameAssignment_1 ) ) ;
    public final void rule__OntologyShortName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1369:1: ( ( ( rule__OntologyShortName__NameAssignment_1 ) ) )
            // InternalOaas.g:1370:1: ( ( rule__OntologyShortName__NameAssignment_1 ) )
            {
            // InternalOaas.g:1370:1: ( ( rule__OntologyShortName__NameAssignment_1 ) )
            // InternalOaas.g:1371:2: ( rule__OntologyShortName__NameAssignment_1 )
            {
             before(grammarAccess.getOntologyShortNameAccess().getNameAssignment_1()); 
            // InternalOaas.g:1372:2: ( rule__OntologyShortName__NameAssignment_1 )
            // InternalOaas.g:1372:3: rule__OntologyShortName__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__OntologyShortName__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getOntologyShortNameAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OntologyShortName__Group__1__Impl"


    // $ANTLR start "rule__Software__Group__0"
    // InternalOaas.g:1381:1: rule__Software__Group__0 : rule__Software__Group__0__Impl rule__Software__Group__1 ;
    public final void rule__Software__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1385:1: ( rule__Software__Group__0__Impl rule__Software__Group__1 )
            // InternalOaas.g:1386:2: rule__Software__Group__0__Impl rule__Software__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__Software__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Software__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Software__Group__0"


    // $ANTLR start "rule__Software__Group__0__Impl"
    // InternalOaas.g:1393:1: rule__Software__Group__0__Impl : ( 'software:' ) ;
    public final void rule__Software__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1397:1: ( ( 'software:' ) )
            // InternalOaas.g:1398:1: ( 'software:' )
            {
            // InternalOaas.g:1398:1: ( 'software:' )
            // InternalOaas.g:1399:2: 'software:'
            {
             before(grammarAccess.getSoftwareAccess().getSoftwareKeyword_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getSoftwareAccess().getSoftwareKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Software__Group__0__Impl"


    // $ANTLR start "rule__Software__Group__1"
    // InternalOaas.g:1408:1: rule__Software__Group__1 : rule__Software__Group__1__Impl ;
    public final void rule__Software__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1412:1: ( rule__Software__Group__1__Impl )
            // InternalOaas.g:1413:2: rule__Software__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Software__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Software__Group__1"


    // $ANTLR start "rule__Software__Group__1__Impl"
    // InternalOaas.g:1419:1: rule__Software__Group__1__Impl : ( ( rule__Software__NameAssignment_1 ) ) ;
    public final void rule__Software__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1423:1: ( ( ( rule__Software__NameAssignment_1 ) ) )
            // InternalOaas.g:1424:1: ( ( rule__Software__NameAssignment_1 ) )
            {
            // InternalOaas.g:1424:1: ( ( rule__Software__NameAssignment_1 ) )
            // InternalOaas.g:1425:2: ( rule__Software__NameAssignment_1 )
            {
             before(grammarAccess.getSoftwareAccess().getNameAssignment_1()); 
            // InternalOaas.g:1426:2: ( rule__Software__NameAssignment_1 )
            // InternalOaas.g:1426:3: rule__Software__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Software__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getSoftwareAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Software__Group__1__Impl"


    // $ANTLR start "rule__About__Group__0"
    // InternalOaas.g:1435:1: rule__About__Group__0 : rule__About__Group__0__Impl rule__About__Group__1 ;
    public final void rule__About__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1439:1: ( rule__About__Group__0__Impl rule__About__Group__1 )
            // InternalOaas.g:1440:2: rule__About__Group__0__Impl rule__About__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__About__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__About__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__About__Group__0"


    // $ANTLR start "rule__About__Group__0__Impl"
    // InternalOaas.g:1447:1: rule__About__Group__0__Impl : ( 'about:' ) ;
    public final void rule__About__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1451:1: ( ( 'about:' ) )
            // InternalOaas.g:1452:1: ( 'about:' )
            {
            // InternalOaas.g:1452:1: ( 'about:' )
            // InternalOaas.g:1453:2: 'about:'
            {
             before(grammarAccess.getAboutAccess().getAboutKeyword_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getAboutAccess().getAboutKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__About__Group__0__Impl"


    // $ANTLR start "rule__About__Group__1"
    // InternalOaas.g:1462:1: rule__About__Group__1 : rule__About__Group__1__Impl ;
    public final void rule__About__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1466:1: ( rule__About__Group__1__Impl )
            // InternalOaas.g:1467:2: rule__About__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__About__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__About__Group__1"


    // $ANTLR start "rule__About__Group__1__Impl"
    // InternalOaas.g:1473:1: rule__About__Group__1__Impl : ( ( rule__About__NameAssignment_1 ) ) ;
    public final void rule__About__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1477:1: ( ( ( rule__About__NameAssignment_1 ) ) )
            // InternalOaas.g:1478:1: ( ( rule__About__NameAssignment_1 ) )
            {
            // InternalOaas.g:1478:1: ( ( rule__About__NameAssignment_1 ) )
            // InternalOaas.g:1479:2: ( rule__About__NameAssignment_1 )
            {
             before(grammarAccess.getAboutAccess().getNameAssignment_1()); 
            // InternalOaas.g:1480:2: ( rule__About__NameAssignment_1 )
            // InternalOaas.g:1480:3: rule__About__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__About__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAboutAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__About__Group__1__Impl"


    // $ANTLR start "rule__Description__Group__0"
    // InternalOaas.g:1489:1: rule__Description__Group__0 : rule__Description__Group__0__Impl rule__Description__Group__1 ;
    public final void rule__Description__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1493:1: ( rule__Description__Group__0__Impl rule__Description__Group__1 )
            // InternalOaas.g:1494:2: rule__Description__Group__0__Impl rule__Description__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__Description__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Description__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__0"


    // $ANTLR start "rule__Description__Group__0__Impl"
    // InternalOaas.g:1501:1: rule__Description__Group__0__Impl : ( '#' ) ;
    public final void rule__Description__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1505:1: ( ( '#' ) )
            // InternalOaas.g:1506:1: ( '#' )
            {
            // InternalOaas.g:1506:1: ( '#' )
            // InternalOaas.g:1507:2: '#'
            {
             before(grammarAccess.getDescriptionAccess().getNumberSignKeyword_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getDescriptionAccess().getNumberSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__0__Impl"


    // $ANTLR start "rule__Description__Group__1"
    // InternalOaas.g:1516:1: rule__Description__Group__1 : rule__Description__Group__1__Impl ;
    public final void rule__Description__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1520:1: ( rule__Description__Group__1__Impl )
            // InternalOaas.g:1521:2: rule__Description__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Description__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__1"


    // $ANTLR start "rule__Description__Group__1__Impl"
    // InternalOaas.g:1527:1: rule__Description__Group__1__Impl : ( ( rule__Description__TextfieldAssignment_1 ) ) ;
    public final void rule__Description__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1531:1: ( ( ( rule__Description__TextfieldAssignment_1 ) ) )
            // InternalOaas.g:1532:1: ( ( rule__Description__TextfieldAssignment_1 ) )
            {
            // InternalOaas.g:1532:1: ( ( rule__Description__TextfieldAssignment_1 ) )
            // InternalOaas.g:1533:2: ( rule__Description__TextfieldAssignment_1 )
            {
             before(grammarAccess.getDescriptionAccess().getTextfieldAssignment_1()); 
            // InternalOaas.g:1534:2: ( rule__Description__TextfieldAssignment_1 )
            // InternalOaas.g:1534:3: rule__Description__TextfieldAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Description__TextfieldAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDescriptionAccess().getTextfieldAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__1__Impl"


    // $ANTLR start "rule__Entity_Type__Group__0"
    // InternalOaas.g:1543:1: rule__Entity_Type__Group__0 : rule__Entity_Type__Group__0__Impl rule__Entity_Type__Group__1 ;
    public final void rule__Entity_Type__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1547:1: ( rule__Entity_Type__Group__0__Impl rule__Entity_Type__Group__1 )
            // InternalOaas.g:1548:2: rule__Entity_Type__Group__0__Impl rule__Entity_Type__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__Entity_Type__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity_Type__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity_Type__Group__0"


    // $ANTLR start "rule__Entity_Type__Group__0__Impl"
    // InternalOaas.g:1555:1: rule__Entity_Type__Group__0__Impl : ( 'is:' ) ;
    public final void rule__Entity_Type__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1559:1: ( ( 'is:' ) )
            // InternalOaas.g:1560:1: ( 'is:' )
            {
            // InternalOaas.g:1560:1: ( 'is:' )
            // InternalOaas.g:1561:2: 'is:'
            {
             before(grammarAccess.getEntity_TypeAccess().getIsKeyword_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getEntity_TypeAccess().getIsKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity_Type__Group__0__Impl"


    // $ANTLR start "rule__Entity_Type__Group__1"
    // InternalOaas.g:1570:1: rule__Entity_Type__Group__1 : rule__Entity_Type__Group__1__Impl ;
    public final void rule__Entity_Type__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1574:1: ( rule__Entity_Type__Group__1__Impl )
            // InternalOaas.g:1575:2: rule__Entity_Type__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Entity_Type__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity_Type__Group__1"


    // $ANTLR start "rule__Entity_Type__Group__1__Impl"
    // InternalOaas.g:1581:1: rule__Entity_Type__Group__1__Impl : ( ( rule__Entity_Type__NameAssignment_1 ) ) ;
    public final void rule__Entity_Type__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1585:1: ( ( ( rule__Entity_Type__NameAssignment_1 ) ) )
            // InternalOaas.g:1586:1: ( ( rule__Entity_Type__NameAssignment_1 ) )
            {
            // InternalOaas.g:1586:1: ( ( rule__Entity_Type__NameAssignment_1 ) )
            // InternalOaas.g:1587:2: ( rule__Entity_Type__NameAssignment_1 )
            {
             before(grammarAccess.getEntity_TypeAccess().getNameAssignment_1()); 
            // InternalOaas.g:1588:2: ( rule__Entity_Type__NameAssignment_1 )
            // InternalOaas.g:1588:3: rule__Entity_Type__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Entity_Type__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getEntity_TypeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity_Type__Group__1__Impl"


    // $ANTLR start "rule__Module__Group__0"
    // InternalOaas.g:1597:1: rule__Module__Group__0 : rule__Module__Group__0__Impl rule__Module__Group__1 ;
    public final void rule__Module__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1601:1: ( rule__Module__Group__0__Impl rule__Module__Group__1 )
            // InternalOaas.g:1602:2: rule__Module__Group__0__Impl rule__Module__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__Module__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Module__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__0"


    // $ANTLR start "rule__Module__Group__0__Impl"
    // InternalOaas.g:1609:1: rule__Module__Group__0__Impl : ( ( rule__Module__DescriptionAssignment_0 )? ) ;
    public final void rule__Module__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1613:1: ( ( ( rule__Module__DescriptionAssignment_0 )? ) )
            // InternalOaas.g:1614:1: ( ( rule__Module__DescriptionAssignment_0 )? )
            {
            // InternalOaas.g:1614:1: ( ( rule__Module__DescriptionAssignment_0 )? )
            // InternalOaas.g:1615:2: ( rule__Module__DescriptionAssignment_0 )?
            {
             before(grammarAccess.getModuleAccess().getDescriptionAssignment_0()); 
            // InternalOaas.g:1616:2: ( rule__Module__DescriptionAssignment_0 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==22) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalOaas.g:1616:3: rule__Module__DescriptionAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Module__DescriptionAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getModuleAccess().getDescriptionAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__0__Impl"


    // $ANTLR start "rule__Module__Group__1"
    // InternalOaas.g:1624:1: rule__Module__Group__1 : rule__Module__Group__1__Impl rule__Module__Group__2 ;
    public final void rule__Module__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1628:1: ( rule__Module__Group__1__Impl rule__Module__Group__2 )
            // InternalOaas.g:1629:2: rule__Module__Group__1__Impl rule__Module__Group__2
            {
            pushFollow(FOLLOW_17);
            rule__Module__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Module__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__1"


    // $ANTLR start "rule__Module__Group__1__Impl"
    // InternalOaas.g:1636:1: rule__Module__Group__1__Impl : ( 'module' ) ;
    public final void rule__Module__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1640:1: ( ( 'module' ) )
            // InternalOaas.g:1641:1: ( 'module' )
            {
            // InternalOaas.g:1641:1: ( 'module' )
            // InternalOaas.g:1642:2: 'module'
            {
             before(grammarAccess.getModuleAccess().getModuleKeyword_1()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getModuleAccess().getModuleKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__1__Impl"


    // $ANTLR start "rule__Module__Group__2"
    // InternalOaas.g:1651:1: rule__Module__Group__2 : rule__Module__Group__2__Impl rule__Module__Group__3 ;
    public final void rule__Module__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1655:1: ( rule__Module__Group__2__Impl rule__Module__Group__3 )
            // InternalOaas.g:1656:2: rule__Module__Group__2__Impl rule__Module__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Module__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Module__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__2"


    // $ANTLR start "rule__Module__Group__2__Impl"
    // InternalOaas.g:1663:1: rule__Module__Group__2__Impl : ( ( rule__Module__NameAssignment_2 ) ) ;
    public final void rule__Module__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1667:1: ( ( ( rule__Module__NameAssignment_2 ) ) )
            // InternalOaas.g:1668:1: ( ( rule__Module__NameAssignment_2 ) )
            {
            // InternalOaas.g:1668:1: ( ( rule__Module__NameAssignment_2 ) )
            // InternalOaas.g:1669:2: ( rule__Module__NameAssignment_2 )
            {
             before(grammarAccess.getModuleAccess().getNameAssignment_2()); 
            // InternalOaas.g:1670:2: ( rule__Module__NameAssignment_2 )
            // InternalOaas.g:1670:3: rule__Module__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Module__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getModuleAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__2__Impl"


    // $ANTLR start "rule__Module__Group__3"
    // InternalOaas.g:1678:1: rule__Module__Group__3 : rule__Module__Group__3__Impl rule__Module__Group__4 ;
    public final void rule__Module__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1682:1: ( rule__Module__Group__3__Impl rule__Module__Group__4 )
            // InternalOaas.g:1683:2: rule__Module__Group__3__Impl rule__Module__Group__4
            {
            pushFollow(FOLLOW_18);
            rule__Module__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Module__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__3"


    // $ANTLR start "rule__Module__Group__3__Impl"
    // InternalOaas.g:1690:1: rule__Module__Group__3__Impl : ( '{' ) ;
    public final void rule__Module__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1694:1: ( ( '{' ) )
            // InternalOaas.g:1695:1: ( '{' )
            {
            // InternalOaas.g:1695:1: ( '{' )
            // InternalOaas.g:1696:2: '{'
            {
             before(grammarAccess.getModuleAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getModuleAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__3__Impl"


    // $ANTLR start "rule__Module__Group__4"
    // InternalOaas.g:1705:1: rule__Module__Group__4 : rule__Module__Group__4__Impl rule__Module__Group__5 ;
    public final void rule__Module__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1709:1: ( rule__Module__Group__4__Impl rule__Module__Group__5 )
            // InternalOaas.g:1710:2: rule__Module__Group__4__Impl rule__Module__Group__5
            {
            pushFollow(FOLLOW_18);
            rule__Module__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Module__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__4"


    // $ANTLR start "rule__Module__Group__4__Impl"
    // InternalOaas.g:1717:1: rule__Module__Group__4__Impl : ( ( rule__Module__ElementsAssignment_4 )* ) ;
    public final void rule__Module__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1721:1: ( ( ( rule__Module__ElementsAssignment_4 )* ) )
            // InternalOaas.g:1722:1: ( ( rule__Module__ElementsAssignment_4 )* )
            {
            // InternalOaas.g:1722:1: ( ( rule__Module__ElementsAssignment_4 )* )
            // InternalOaas.g:1723:2: ( rule__Module__ElementsAssignment_4 )*
            {
             before(grammarAccess.getModuleAccess().getElementsAssignment_4()); 
            // InternalOaas.g:1724:2: ( rule__Module__ElementsAssignment_4 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==22||LA6_0==24||LA6_0==26||LA6_0==28) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalOaas.g:1724:3: rule__Module__ElementsAssignment_4
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__Module__ElementsAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getModuleAccess().getElementsAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__4__Impl"


    // $ANTLR start "rule__Module__Group__5"
    // InternalOaas.g:1732:1: rule__Module__Group__5 : rule__Module__Group__5__Impl ;
    public final void rule__Module__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1736:1: ( rule__Module__Group__5__Impl )
            // InternalOaas.g:1737:2: rule__Module__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Module__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__5"


    // $ANTLR start "rule__Module__Group__5__Impl"
    // InternalOaas.g:1743:1: rule__Module__Group__5__Impl : ( '}' ) ;
    public final void rule__Module__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1747:1: ( ( '}' ) )
            // InternalOaas.g:1748:1: ( '}' )
            {
            // InternalOaas.g:1748:1: ( '}' )
            // InternalOaas.g:1749:2: '}'
            {
             before(grammarAccess.getModuleAccess().getRightCurlyBracketKeyword_5()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getModuleAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__5__Impl"


    // $ANTLR start "rule__QualifiedName__Group__0"
    // InternalOaas.g:1759:1: rule__QualifiedName__Group__0 : rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 ;
    public final void rule__QualifiedName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1763:1: ( rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 )
            // InternalOaas.g:1764:2: rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__QualifiedName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0"


    // $ANTLR start "rule__QualifiedName__Group__0__Impl"
    // InternalOaas.g:1771:1: rule__QualifiedName__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1775:1: ( ( RULE_ID ) )
            // InternalOaas.g:1776:1: ( RULE_ID )
            {
            // InternalOaas.g:1776:1: ( RULE_ID )
            // InternalOaas.g:1777:2: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group__1"
    // InternalOaas.g:1786:1: rule__QualifiedName__Group__1 : rule__QualifiedName__Group__1__Impl ;
    public final void rule__QualifiedName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1790:1: ( rule__QualifiedName__Group__1__Impl )
            // InternalOaas.g:1791:2: rule__QualifiedName__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1"


    // $ANTLR start "rule__QualifiedName__Group__1__Impl"
    // InternalOaas.g:1797:1: rule__QualifiedName__Group__1__Impl : ( ( rule__QualifiedName__Group_1__0 )* ) ;
    public final void rule__QualifiedName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1801:1: ( ( ( rule__QualifiedName__Group_1__0 )* ) )
            // InternalOaas.g:1802:1: ( ( rule__QualifiedName__Group_1__0 )* )
            {
            // InternalOaas.g:1802:1: ( ( rule__QualifiedName__Group_1__0 )* )
            // InternalOaas.g:1803:2: ( rule__QualifiedName__Group_1__0 )*
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup_1()); 
            // InternalOaas.g:1804:2: ( rule__QualifiedName__Group_1__0 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==25) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalOaas.g:1804:3: rule__QualifiedName__Group_1__0
            	    {
            	    pushFollow(FOLLOW_20);
            	    rule__QualifiedName__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getQualifiedNameAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__0"
    // InternalOaas.g:1813:1: rule__QualifiedName__Group_1__0 : rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 ;
    public final void rule__QualifiedName__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1817:1: ( rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 )
            // InternalOaas.g:1818:2: rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1
            {
            pushFollow(FOLLOW_17);
            rule__QualifiedName__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0"


    // $ANTLR start "rule__QualifiedName__Group_1__0__Impl"
    // InternalOaas.g:1825:1: rule__QualifiedName__Group_1__0__Impl : ( '.' ) ;
    public final void rule__QualifiedName__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1829:1: ( ( '.' ) )
            // InternalOaas.g:1830:1: ( '.' )
            {
            // InternalOaas.g:1830:1: ( '.' )
            // InternalOaas.g:1831:2: '.'
            {
             before(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__1"
    // InternalOaas.g:1840:1: rule__QualifiedName__Group_1__1 : rule__QualifiedName__Group_1__1__Impl ;
    public final void rule__QualifiedName__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1844:1: ( rule__QualifiedName__Group_1__1__Impl )
            // InternalOaas.g:1845:2: rule__QualifiedName__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1"


    // $ANTLR start "rule__QualifiedName__Group_1__1__Impl"
    // InternalOaas.g:1851:1: rule__QualifiedName__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1855:1: ( ( RULE_ID ) )
            // InternalOaas.g:1856:1: ( RULE_ID )
            {
            // InternalOaas.g:1856:1: ( RULE_ID )
            // InternalOaas.g:1857:2: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1__Impl"


    // $ANTLR start "rule__Import__Group__0"
    // InternalOaas.g:1867:1: rule__Import__Group__0 : rule__Import__Group__0__Impl rule__Import__Group__1 ;
    public final void rule__Import__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1871:1: ( rule__Import__Group__0__Impl rule__Import__Group__1 )
            // InternalOaas.g:1872:2: rule__Import__Group__0__Impl rule__Import__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__Import__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Import__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0"


    // $ANTLR start "rule__Import__Group__0__Impl"
    // InternalOaas.g:1879:1: rule__Import__Group__0__Impl : ( 'import' ) ;
    public final void rule__Import__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1883:1: ( ( 'import' ) )
            // InternalOaas.g:1884:1: ( 'import' )
            {
            // InternalOaas.g:1884:1: ( 'import' )
            // InternalOaas.g:1885:2: 'import'
            {
             before(grammarAccess.getImportAccess().getImportKeyword_0()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getImportAccess().getImportKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0__Impl"


    // $ANTLR start "rule__Import__Group__1"
    // InternalOaas.g:1894:1: rule__Import__Group__1 : rule__Import__Group__1__Impl ;
    public final void rule__Import__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1898:1: ( rule__Import__Group__1__Impl )
            // InternalOaas.g:1899:2: rule__Import__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1"


    // $ANTLR start "rule__Import__Group__1__Impl"
    // InternalOaas.g:1905:1: rule__Import__Group__1__Impl : ( ( rule__Import__ImportedNamespaceAssignment_1 ) ) ;
    public final void rule__Import__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1909:1: ( ( ( rule__Import__ImportedNamespaceAssignment_1 ) ) )
            // InternalOaas.g:1910:1: ( ( rule__Import__ImportedNamespaceAssignment_1 ) )
            {
            // InternalOaas.g:1910:1: ( ( rule__Import__ImportedNamespaceAssignment_1 ) )
            // InternalOaas.g:1911:2: ( rule__Import__ImportedNamespaceAssignment_1 )
            {
             before(grammarAccess.getImportAccess().getImportedNamespaceAssignment_1()); 
            // InternalOaas.g:1912:2: ( rule__Import__ImportedNamespaceAssignment_1 )
            // InternalOaas.g:1912:3: rule__Import__ImportedNamespaceAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Import__ImportedNamespaceAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getImportedNamespaceAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__0"
    // InternalOaas.g:1921:1: rule__QualifiedNameWithWildcard__Group__0 : rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1 ;
    public final void rule__QualifiedNameWithWildcard__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1925:1: ( rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1 )
            // InternalOaas.g:1926:2: rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1
            {
            pushFollow(FOLLOW_21);
            rule__QualifiedNameWithWildcard__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedNameWithWildcard__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__0"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__0__Impl"
    // InternalOaas.g:1933:1: rule__QualifiedNameWithWildcard__Group__0__Impl : ( ruleQualifiedName ) ;
    public final void rule__QualifiedNameWithWildcard__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1937:1: ( ( ruleQualifiedName ) )
            // InternalOaas.g:1938:1: ( ruleQualifiedName )
            {
            // InternalOaas.g:1938:1: ( ruleQualifiedName )
            // InternalOaas.g:1939:2: ruleQualifiedName
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__0__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__1"
    // InternalOaas.g:1948:1: rule__QualifiedNameWithWildcard__Group__1 : rule__QualifiedNameWithWildcard__Group__1__Impl ;
    public final void rule__QualifiedNameWithWildcard__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1952:1: ( rule__QualifiedNameWithWildcard__Group__1__Impl )
            // InternalOaas.g:1953:2: rule__QualifiedNameWithWildcard__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedNameWithWildcard__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__1"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__1__Impl"
    // InternalOaas.g:1959:1: rule__QualifiedNameWithWildcard__Group__1__Impl : ( ( '.*' )? ) ;
    public final void rule__QualifiedNameWithWildcard__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1963:1: ( ( ( '.*' )? ) )
            // InternalOaas.g:1964:1: ( ( '.*' )? )
            {
            // InternalOaas.g:1964:1: ( ( '.*' )? )
            // InternalOaas.g:1965:2: ( '.*' )?
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1()); 
            // InternalOaas.g:1966:2: ( '.*' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==27) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalOaas.g:1966:3: '.*'
                    {
                    match(input,27,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__1__Impl"


    // $ANTLR start "rule__Entity__Group__0"
    // InternalOaas.g:1975:1: rule__Entity__Group__0 : rule__Entity__Group__0__Impl rule__Entity__Group__1 ;
    public final void rule__Entity__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1979:1: ( rule__Entity__Group__0__Impl rule__Entity__Group__1 )
            // InternalOaas.g:1980:2: rule__Entity__Group__0__Impl rule__Entity__Group__1
            {
            pushFollow(FOLLOW_22);
            rule__Entity__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__0"


    // $ANTLR start "rule__Entity__Group__0__Impl"
    // InternalOaas.g:1987:1: rule__Entity__Group__0__Impl : ( ( rule__Entity__DescriptionAssignment_0 )? ) ;
    public final void rule__Entity__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:1991:1: ( ( ( rule__Entity__DescriptionAssignment_0 )? ) )
            // InternalOaas.g:1992:1: ( ( rule__Entity__DescriptionAssignment_0 )? )
            {
            // InternalOaas.g:1992:1: ( ( rule__Entity__DescriptionAssignment_0 )? )
            // InternalOaas.g:1993:2: ( rule__Entity__DescriptionAssignment_0 )?
            {
             before(grammarAccess.getEntityAccess().getDescriptionAssignment_0()); 
            // InternalOaas.g:1994:2: ( rule__Entity__DescriptionAssignment_0 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==22) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalOaas.g:1994:3: rule__Entity__DescriptionAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Entity__DescriptionAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEntityAccess().getDescriptionAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__0__Impl"


    // $ANTLR start "rule__Entity__Group__1"
    // InternalOaas.g:2002:1: rule__Entity__Group__1 : rule__Entity__Group__1__Impl rule__Entity__Group__2 ;
    public final void rule__Entity__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2006:1: ( rule__Entity__Group__1__Impl rule__Entity__Group__2 )
            // InternalOaas.g:2007:2: rule__Entity__Group__1__Impl rule__Entity__Group__2
            {
            pushFollow(FOLLOW_17);
            rule__Entity__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__1"


    // $ANTLR start "rule__Entity__Group__1__Impl"
    // InternalOaas.g:2014:1: rule__Entity__Group__1__Impl : ( 'entity' ) ;
    public final void rule__Entity__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2018:1: ( ( 'entity' ) )
            // InternalOaas.g:2019:1: ( 'entity' )
            {
            // InternalOaas.g:2019:1: ( 'entity' )
            // InternalOaas.g:2020:2: 'entity'
            {
             before(grammarAccess.getEntityAccess().getEntityKeyword_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getEntityAccess().getEntityKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__1__Impl"


    // $ANTLR start "rule__Entity__Group__2"
    // InternalOaas.g:2029:1: rule__Entity__Group__2 : rule__Entity__Group__2__Impl rule__Entity__Group__3 ;
    public final void rule__Entity__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2033:1: ( rule__Entity__Group__2__Impl rule__Entity__Group__3 )
            // InternalOaas.g:2034:2: rule__Entity__Group__2__Impl rule__Entity__Group__3
            {
            pushFollow(FOLLOW_23);
            rule__Entity__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__2"


    // $ANTLR start "rule__Entity__Group__2__Impl"
    // InternalOaas.g:2041:1: rule__Entity__Group__2__Impl : ( ( rule__Entity__NameAssignment_2 ) ) ;
    public final void rule__Entity__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2045:1: ( ( ( rule__Entity__NameAssignment_2 ) ) )
            // InternalOaas.g:2046:1: ( ( rule__Entity__NameAssignment_2 ) )
            {
            // InternalOaas.g:2046:1: ( ( rule__Entity__NameAssignment_2 ) )
            // InternalOaas.g:2047:2: ( rule__Entity__NameAssignment_2 )
            {
             before(grammarAccess.getEntityAccess().getNameAssignment_2()); 
            // InternalOaas.g:2048:2: ( rule__Entity__NameAssignment_2 )
            // InternalOaas.g:2048:3: rule__Entity__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Entity__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getEntityAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__2__Impl"


    // $ANTLR start "rule__Entity__Group__3"
    // InternalOaas.g:2056:1: rule__Entity__Group__3 : rule__Entity__Group__3__Impl rule__Entity__Group__4 ;
    public final void rule__Entity__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2060:1: ( rule__Entity__Group__3__Impl rule__Entity__Group__4 )
            // InternalOaas.g:2061:2: rule__Entity__Group__3__Impl rule__Entity__Group__4
            {
            pushFollow(FOLLOW_23);
            rule__Entity__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__3"


    // $ANTLR start "rule__Entity__Group__3__Impl"
    // InternalOaas.g:2068:1: rule__Entity__Group__3__Impl : ( ( rule__Entity__Group_3__0 )? ) ;
    public final void rule__Entity__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2072:1: ( ( ( rule__Entity__Group_3__0 )? ) )
            // InternalOaas.g:2073:1: ( ( rule__Entity__Group_3__0 )? )
            {
            // InternalOaas.g:2073:1: ( ( rule__Entity__Group_3__0 )? )
            // InternalOaas.g:2074:2: ( rule__Entity__Group_3__0 )?
            {
             before(grammarAccess.getEntityAccess().getGroup_3()); 
            // InternalOaas.g:2075:2: ( rule__Entity__Group_3__0 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==29) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalOaas.g:2075:3: rule__Entity__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Entity__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEntityAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__3__Impl"


    // $ANTLR start "rule__Entity__Group__4"
    // InternalOaas.g:2083:1: rule__Entity__Group__4 : rule__Entity__Group__4__Impl rule__Entity__Group__5 ;
    public final void rule__Entity__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2087:1: ( rule__Entity__Group__4__Impl rule__Entity__Group__5 )
            // InternalOaas.g:2088:2: rule__Entity__Group__4__Impl rule__Entity__Group__5
            {
            pushFollow(FOLLOW_24);
            rule__Entity__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__4"


    // $ANTLR start "rule__Entity__Group__4__Impl"
    // InternalOaas.g:2095:1: rule__Entity__Group__4__Impl : ( '{' ) ;
    public final void rule__Entity__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2099:1: ( ( '{' ) )
            // InternalOaas.g:2100:1: ( '{' )
            {
            // InternalOaas.g:2100:1: ( '{' )
            // InternalOaas.g:2101:2: '{'
            {
             before(grammarAccess.getEntityAccess().getLeftCurlyBracketKeyword_4()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getEntityAccess().getLeftCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__4__Impl"


    // $ANTLR start "rule__Entity__Group__5"
    // InternalOaas.g:2110:1: rule__Entity__Group__5 : rule__Entity__Group__5__Impl rule__Entity__Group__6 ;
    public final void rule__Entity__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2114:1: ( rule__Entity__Group__5__Impl rule__Entity__Group__6 )
            // InternalOaas.g:2115:2: rule__Entity__Group__5__Impl rule__Entity__Group__6
            {
            pushFollow(FOLLOW_25);
            rule__Entity__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__5"


    // $ANTLR start "rule__Entity__Group__5__Impl"
    // InternalOaas.g:2122:1: rule__Entity__Group__5__Impl : ( ( rule__Entity__Entity_typeAssignment_5 ) ) ;
    public final void rule__Entity__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2126:1: ( ( ( rule__Entity__Entity_typeAssignment_5 ) ) )
            // InternalOaas.g:2127:1: ( ( rule__Entity__Entity_typeAssignment_5 ) )
            {
            // InternalOaas.g:2127:1: ( ( rule__Entity__Entity_typeAssignment_5 ) )
            // InternalOaas.g:2128:2: ( rule__Entity__Entity_typeAssignment_5 )
            {
             before(grammarAccess.getEntityAccess().getEntity_typeAssignment_5()); 
            // InternalOaas.g:2129:2: ( rule__Entity__Entity_typeAssignment_5 )
            // InternalOaas.g:2129:3: rule__Entity__Entity_typeAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Entity__Entity_typeAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getEntityAccess().getEntity_typeAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__5__Impl"


    // $ANTLR start "rule__Entity__Group__6"
    // InternalOaas.g:2137:1: rule__Entity__Group__6 : rule__Entity__Group__6__Impl rule__Entity__Group__7 ;
    public final void rule__Entity__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2141:1: ( rule__Entity__Group__6__Impl rule__Entity__Group__7 )
            // InternalOaas.g:2142:2: rule__Entity__Group__6__Impl rule__Entity__Group__7
            {
            pushFollow(FOLLOW_25);
            rule__Entity__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__6"


    // $ANTLR start "rule__Entity__Group__6__Impl"
    // InternalOaas.g:2149:1: rule__Entity__Group__6__Impl : ( ( rule__Entity__AttributesAssignment_6 )* ) ;
    public final void rule__Entity__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2153:1: ( ( ( rule__Entity__AttributesAssignment_6 )* ) )
            // InternalOaas.g:2154:1: ( ( rule__Entity__AttributesAssignment_6 )* )
            {
            // InternalOaas.g:2154:1: ( ( rule__Entity__AttributesAssignment_6 )* )
            // InternalOaas.g:2155:2: ( rule__Entity__AttributesAssignment_6 )*
            {
             before(grammarAccess.getEntityAccess().getAttributesAssignment_6()); 
            // InternalOaas.g:2156:2: ( rule__Entity__AttributesAssignment_6 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==22) ) {
                    int LA11_1 = input.LA(2);

                    if ( (LA11_1==RULE_STRING) ) {
                        int LA11_4 = input.LA(3);

                        if ( (LA11_4==RULE_ID) ) {
                            alt11=1;
                        }


                    }


                }
                else if ( (LA11_0==RULE_ID) ) {
                    int LA11_3 = input.LA(2);

                    if ( (LA11_3==30) ) {
                        alt11=1;
                    }


                }


                switch (alt11) {
            	case 1 :
            	    // InternalOaas.g:2156:3: rule__Entity__AttributesAssignment_6
            	    {
            	    pushFollow(FOLLOW_26);
            	    rule__Entity__AttributesAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getEntityAccess().getAttributesAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__6__Impl"


    // $ANTLR start "rule__Entity__Group__7"
    // InternalOaas.g:2164:1: rule__Entity__Group__7 : rule__Entity__Group__7__Impl rule__Entity__Group__8 ;
    public final void rule__Entity__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2168:1: ( rule__Entity__Group__7__Impl rule__Entity__Group__8 )
            // InternalOaas.g:2169:2: rule__Entity__Group__7__Impl rule__Entity__Group__8
            {
            pushFollow(FOLLOW_25);
            rule__Entity__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__7"


    // $ANTLR start "rule__Entity__Group__7__Impl"
    // InternalOaas.g:2176:1: rule__Entity__Group__7__Impl : ( ( rule__Entity__FunctionsAssignment_7 )* ) ;
    public final void rule__Entity__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2180:1: ( ( ( rule__Entity__FunctionsAssignment_7 )* ) )
            // InternalOaas.g:2181:1: ( ( rule__Entity__FunctionsAssignment_7 )* )
            {
            // InternalOaas.g:2181:1: ( ( rule__Entity__FunctionsAssignment_7 )* )
            // InternalOaas.g:2182:2: ( rule__Entity__FunctionsAssignment_7 )*
            {
             before(grammarAccess.getEntityAccess().getFunctionsAssignment_7()); 
            // InternalOaas.g:2183:2: ( rule__Entity__FunctionsAssignment_7 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==22||LA12_0==34) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalOaas.g:2183:3: rule__Entity__FunctionsAssignment_7
            	    {
            	    pushFollow(FOLLOW_27);
            	    rule__Entity__FunctionsAssignment_7();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

             after(grammarAccess.getEntityAccess().getFunctionsAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__7__Impl"


    // $ANTLR start "rule__Entity__Group__8"
    // InternalOaas.g:2191:1: rule__Entity__Group__8 : rule__Entity__Group__8__Impl rule__Entity__Group__9 ;
    public final void rule__Entity__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2195:1: ( rule__Entity__Group__8__Impl rule__Entity__Group__9 )
            // InternalOaas.g:2196:2: rule__Entity__Group__8__Impl rule__Entity__Group__9
            {
            pushFollow(FOLLOW_25);
            rule__Entity__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__8"


    // $ANTLR start "rule__Entity__Group__8__Impl"
    // InternalOaas.g:2203:1: rule__Entity__Group__8__Impl : ( ( rule__Entity__RelationsAssignment_8 )* ) ;
    public final void rule__Entity__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2207:1: ( ( ( rule__Entity__RelationsAssignment_8 )* ) )
            // InternalOaas.g:2208:1: ( ( rule__Entity__RelationsAssignment_8 )* )
            {
            // InternalOaas.g:2208:1: ( ( rule__Entity__RelationsAssignment_8 )* )
            // InternalOaas.g:2209:2: ( rule__Entity__RelationsAssignment_8 )*
            {
             before(grammarAccess.getEntityAccess().getRelationsAssignment_8()); 
            // InternalOaas.g:2210:2: ( rule__Entity__RelationsAssignment_8 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==RULE_ID) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalOaas.g:2210:3: rule__Entity__RelationsAssignment_8
            	    {
            	    pushFollow(FOLLOW_28);
            	    rule__Entity__RelationsAssignment_8();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getEntityAccess().getRelationsAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__8__Impl"


    // $ANTLR start "rule__Entity__Group__9"
    // InternalOaas.g:2218:1: rule__Entity__Group__9 : rule__Entity__Group__9__Impl ;
    public final void rule__Entity__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2222:1: ( rule__Entity__Group__9__Impl )
            // InternalOaas.g:2223:2: rule__Entity__Group__9__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Entity__Group__9__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__9"


    // $ANTLR start "rule__Entity__Group__9__Impl"
    // InternalOaas.g:2229:1: rule__Entity__Group__9__Impl : ( '}' ) ;
    public final void rule__Entity__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2233:1: ( ( '}' ) )
            // InternalOaas.g:2234:1: ( '}' )
            {
            // InternalOaas.g:2234:1: ( '}' )
            // InternalOaas.g:2235:2: '}'
            {
             before(grammarAccess.getEntityAccess().getRightCurlyBracketKeyword_9()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getEntityAccess().getRightCurlyBracketKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__9__Impl"


    // $ANTLR start "rule__Entity__Group_3__0"
    // InternalOaas.g:2245:1: rule__Entity__Group_3__0 : rule__Entity__Group_3__0__Impl rule__Entity__Group_3__1 ;
    public final void rule__Entity__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2249:1: ( rule__Entity__Group_3__0__Impl rule__Entity__Group_3__1 )
            // InternalOaas.g:2250:2: rule__Entity__Group_3__0__Impl rule__Entity__Group_3__1
            {
            pushFollow(FOLLOW_17);
            rule__Entity__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group_3__0"


    // $ANTLR start "rule__Entity__Group_3__0__Impl"
    // InternalOaas.g:2257:1: rule__Entity__Group_3__0__Impl : ( 'extends' ) ;
    public final void rule__Entity__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2261:1: ( ( 'extends' ) )
            // InternalOaas.g:2262:1: ( 'extends' )
            {
            // InternalOaas.g:2262:1: ( 'extends' )
            // InternalOaas.g:2263:2: 'extends'
            {
             before(grammarAccess.getEntityAccess().getExtendsKeyword_3_0()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getEntityAccess().getExtendsKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group_3__0__Impl"


    // $ANTLR start "rule__Entity__Group_3__1"
    // InternalOaas.g:2272:1: rule__Entity__Group_3__1 : rule__Entity__Group_3__1__Impl ;
    public final void rule__Entity__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2276:1: ( rule__Entity__Group_3__1__Impl )
            // InternalOaas.g:2277:2: rule__Entity__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Entity__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group_3__1"


    // $ANTLR start "rule__Entity__Group_3__1__Impl"
    // InternalOaas.g:2283:1: rule__Entity__Group_3__1__Impl : ( ( rule__Entity__SuperTypeAssignment_3_1 ) ) ;
    public final void rule__Entity__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2287:1: ( ( ( rule__Entity__SuperTypeAssignment_3_1 ) ) )
            // InternalOaas.g:2288:1: ( ( rule__Entity__SuperTypeAssignment_3_1 ) )
            {
            // InternalOaas.g:2288:1: ( ( rule__Entity__SuperTypeAssignment_3_1 ) )
            // InternalOaas.g:2289:2: ( rule__Entity__SuperTypeAssignment_3_1 )
            {
             before(grammarAccess.getEntityAccess().getSuperTypeAssignment_3_1()); 
            // InternalOaas.g:2290:2: ( rule__Entity__SuperTypeAssignment_3_1 )
            // InternalOaas.g:2290:3: rule__Entity__SuperTypeAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Entity__SuperTypeAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getEntityAccess().getSuperTypeAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group_3__1__Impl"


    // $ANTLR start "rule__Attribute__Group__0"
    // InternalOaas.g:2299:1: rule__Attribute__Group__0 : rule__Attribute__Group__0__Impl rule__Attribute__Group__1 ;
    public final void rule__Attribute__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2303:1: ( rule__Attribute__Group__0__Impl rule__Attribute__Group__1 )
            // InternalOaas.g:2304:2: rule__Attribute__Group__0__Impl rule__Attribute__Group__1
            {
            pushFollow(FOLLOW_29);
            rule__Attribute__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attribute__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__0"


    // $ANTLR start "rule__Attribute__Group__0__Impl"
    // InternalOaas.g:2311:1: rule__Attribute__Group__0__Impl : ( ( rule__Attribute__DescriptionAssignment_0 )? ) ;
    public final void rule__Attribute__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2315:1: ( ( ( rule__Attribute__DescriptionAssignment_0 )? ) )
            // InternalOaas.g:2316:1: ( ( rule__Attribute__DescriptionAssignment_0 )? )
            {
            // InternalOaas.g:2316:1: ( ( rule__Attribute__DescriptionAssignment_0 )? )
            // InternalOaas.g:2317:2: ( rule__Attribute__DescriptionAssignment_0 )?
            {
             before(grammarAccess.getAttributeAccess().getDescriptionAssignment_0()); 
            // InternalOaas.g:2318:2: ( rule__Attribute__DescriptionAssignment_0 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==22) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalOaas.g:2318:3: rule__Attribute__DescriptionAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Attribute__DescriptionAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAttributeAccess().getDescriptionAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__0__Impl"


    // $ANTLR start "rule__Attribute__Group__1"
    // InternalOaas.g:2326:1: rule__Attribute__Group__1 : rule__Attribute__Group__1__Impl rule__Attribute__Group__2 ;
    public final void rule__Attribute__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2330:1: ( rule__Attribute__Group__1__Impl rule__Attribute__Group__2 )
            // InternalOaas.g:2331:2: rule__Attribute__Group__1__Impl rule__Attribute__Group__2
            {
            pushFollow(FOLLOW_30);
            rule__Attribute__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attribute__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__1"


    // $ANTLR start "rule__Attribute__Group__1__Impl"
    // InternalOaas.g:2338:1: rule__Attribute__Group__1__Impl : ( ( rule__Attribute__NameAssignment_1 ) ) ;
    public final void rule__Attribute__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2342:1: ( ( ( rule__Attribute__NameAssignment_1 ) ) )
            // InternalOaas.g:2343:1: ( ( rule__Attribute__NameAssignment_1 ) )
            {
            // InternalOaas.g:2343:1: ( ( rule__Attribute__NameAssignment_1 ) )
            // InternalOaas.g:2344:2: ( rule__Attribute__NameAssignment_1 )
            {
             before(grammarAccess.getAttributeAccess().getNameAssignment_1()); 
            // InternalOaas.g:2345:2: ( rule__Attribute__NameAssignment_1 )
            // InternalOaas.g:2345:3: rule__Attribute__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Attribute__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__1__Impl"


    // $ANTLR start "rule__Attribute__Group__2"
    // InternalOaas.g:2353:1: rule__Attribute__Group__2 : rule__Attribute__Group__2__Impl rule__Attribute__Group__3 ;
    public final void rule__Attribute__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2357:1: ( rule__Attribute__Group__2__Impl rule__Attribute__Group__3 )
            // InternalOaas.g:2358:2: rule__Attribute__Group__2__Impl rule__Attribute__Group__3
            {
            pushFollow(FOLLOW_17);
            rule__Attribute__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attribute__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__2"


    // $ANTLR start "rule__Attribute__Group__2__Impl"
    // InternalOaas.g:2365:1: rule__Attribute__Group__2__Impl : ( ':' ) ;
    public final void rule__Attribute__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2369:1: ( ( ':' ) )
            // InternalOaas.g:2370:1: ( ':' )
            {
            // InternalOaas.g:2370:1: ( ':' )
            // InternalOaas.g:2371:2: ':'
            {
             before(grammarAccess.getAttributeAccess().getColonKeyword_2()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getAttributeAccess().getColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__2__Impl"


    // $ANTLR start "rule__Attribute__Group__3"
    // InternalOaas.g:2380:1: rule__Attribute__Group__3 : rule__Attribute__Group__3__Impl ;
    public final void rule__Attribute__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2384:1: ( rule__Attribute__Group__3__Impl )
            // InternalOaas.g:2385:2: rule__Attribute__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Attribute__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__3"


    // $ANTLR start "rule__Attribute__Group__3__Impl"
    // InternalOaas.g:2391:1: rule__Attribute__Group__3__Impl : ( ( rule__Attribute__TypeAssignment_3 ) ) ;
    public final void rule__Attribute__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2395:1: ( ( ( rule__Attribute__TypeAssignment_3 ) ) )
            // InternalOaas.g:2396:1: ( ( rule__Attribute__TypeAssignment_3 ) )
            {
            // InternalOaas.g:2396:1: ( ( rule__Attribute__TypeAssignment_3 ) )
            // InternalOaas.g:2397:2: ( rule__Attribute__TypeAssignment_3 )
            {
             before(grammarAccess.getAttributeAccess().getTypeAssignment_3()); 
            // InternalOaas.g:2398:2: ( rule__Attribute__TypeAssignment_3 )
            // InternalOaas.g:2398:3: rule__Attribute__TypeAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Attribute__TypeAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getTypeAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__3__Impl"


    // $ANTLR start "rule__OneToOne__Group__0"
    // InternalOaas.g:2407:1: rule__OneToOne__Group__0 : rule__OneToOne__Group__0__Impl rule__OneToOne__Group__1 ;
    public final void rule__OneToOne__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2411:1: ( rule__OneToOne__Group__0__Impl rule__OneToOne__Group__1 )
            // InternalOaas.g:2412:2: rule__OneToOne__Group__0__Impl rule__OneToOne__Group__1
            {
            pushFollow(FOLLOW_31);
            rule__OneToOne__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OneToOne__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToOne__Group__0"


    // $ANTLR start "rule__OneToOne__Group__0__Impl"
    // InternalOaas.g:2419:1: rule__OneToOne__Group__0__Impl : ( ( rule__OneToOne__NameAssignment_0 ) ) ;
    public final void rule__OneToOne__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2423:1: ( ( ( rule__OneToOne__NameAssignment_0 ) ) )
            // InternalOaas.g:2424:1: ( ( rule__OneToOne__NameAssignment_0 ) )
            {
            // InternalOaas.g:2424:1: ( ( rule__OneToOne__NameAssignment_0 ) )
            // InternalOaas.g:2425:2: ( rule__OneToOne__NameAssignment_0 )
            {
             before(grammarAccess.getOneToOneAccess().getNameAssignment_0()); 
            // InternalOaas.g:2426:2: ( rule__OneToOne__NameAssignment_0 )
            // InternalOaas.g:2426:3: rule__OneToOne__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__OneToOne__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getOneToOneAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToOne__Group__0__Impl"


    // $ANTLR start "rule__OneToOne__Group__1"
    // InternalOaas.g:2434:1: rule__OneToOne__Group__1 : rule__OneToOne__Group__1__Impl rule__OneToOne__Group__2 ;
    public final void rule__OneToOne__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2438:1: ( rule__OneToOne__Group__1__Impl rule__OneToOne__Group__2 )
            // InternalOaas.g:2439:2: rule__OneToOne__Group__1__Impl rule__OneToOne__Group__2
            {
            pushFollow(FOLLOW_17);
            rule__OneToOne__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OneToOne__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToOne__Group__1"


    // $ANTLR start "rule__OneToOne__Group__1__Impl"
    // InternalOaas.g:2446:1: rule__OneToOne__Group__1__Impl : ( 'OneToOne' ) ;
    public final void rule__OneToOne__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2450:1: ( ( 'OneToOne' ) )
            // InternalOaas.g:2451:1: ( 'OneToOne' )
            {
            // InternalOaas.g:2451:1: ( 'OneToOne' )
            // InternalOaas.g:2452:2: 'OneToOne'
            {
             before(grammarAccess.getOneToOneAccess().getOneToOneKeyword_1()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getOneToOneAccess().getOneToOneKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToOne__Group__1__Impl"


    // $ANTLR start "rule__OneToOne__Group__2"
    // InternalOaas.g:2461:1: rule__OneToOne__Group__2 : rule__OneToOne__Group__2__Impl ;
    public final void rule__OneToOne__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2465:1: ( rule__OneToOne__Group__2__Impl )
            // InternalOaas.g:2466:2: rule__OneToOne__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OneToOne__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToOne__Group__2"


    // $ANTLR start "rule__OneToOne__Group__2__Impl"
    // InternalOaas.g:2472:1: rule__OneToOne__Group__2__Impl : ( ( rule__OneToOne__TypeAssignment_2 ) ) ;
    public final void rule__OneToOne__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2476:1: ( ( ( rule__OneToOne__TypeAssignment_2 ) ) )
            // InternalOaas.g:2477:1: ( ( rule__OneToOne__TypeAssignment_2 ) )
            {
            // InternalOaas.g:2477:1: ( ( rule__OneToOne__TypeAssignment_2 ) )
            // InternalOaas.g:2478:2: ( rule__OneToOne__TypeAssignment_2 )
            {
             before(grammarAccess.getOneToOneAccess().getTypeAssignment_2()); 
            // InternalOaas.g:2479:2: ( rule__OneToOne__TypeAssignment_2 )
            // InternalOaas.g:2479:3: rule__OneToOne__TypeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__OneToOne__TypeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getOneToOneAccess().getTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToOne__Group__2__Impl"


    // $ANTLR start "rule__ManyToMany__Group__0"
    // InternalOaas.g:2488:1: rule__ManyToMany__Group__0 : rule__ManyToMany__Group__0__Impl rule__ManyToMany__Group__1 ;
    public final void rule__ManyToMany__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2492:1: ( rule__ManyToMany__Group__0__Impl rule__ManyToMany__Group__1 )
            // InternalOaas.g:2493:2: rule__ManyToMany__Group__0__Impl rule__ManyToMany__Group__1
            {
            pushFollow(FOLLOW_32);
            rule__ManyToMany__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ManyToMany__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyToMany__Group__0"


    // $ANTLR start "rule__ManyToMany__Group__0__Impl"
    // InternalOaas.g:2500:1: rule__ManyToMany__Group__0__Impl : ( ( rule__ManyToMany__NameAssignment_0 ) ) ;
    public final void rule__ManyToMany__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2504:1: ( ( ( rule__ManyToMany__NameAssignment_0 ) ) )
            // InternalOaas.g:2505:1: ( ( rule__ManyToMany__NameAssignment_0 ) )
            {
            // InternalOaas.g:2505:1: ( ( rule__ManyToMany__NameAssignment_0 ) )
            // InternalOaas.g:2506:2: ( rule__ManyToMany__NameAssignment_0 )
            {
             before(grammarAccess.getManyToManyAccess().getNameAssignment_0()); 
            // InternalOaas.g:2507:2: ( rule__ManyToMany__NameAssignment_0 )
            // InternalOaas.g:2507:3: rule__ManyToMany__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__ManyToMany__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getManyToManyAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyToMany__Group__0__Impl"


    // $ANTLR start "rule__ManyToMany__Group__1"
    // InternalOaas.g:2515:1: rule__ManyToMany__Group__1 : rule__ManyToMany__Group__1__Impl rule__ManyToMany__Group__2 ;
    public final void rule__ManyToMany__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2519:1: ( rule__ManyToMany__Group__1__Impl rule__ManyToMany__Group__2 )
            // InternalOaas.g:2520:2: rule__ManyToMany__Group__1__Impl rule__ManyToMany__Group__2
            {
            pushFollow(FOLLOW_17);
            rule__ManyToMany__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ManyToMany__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyToMany__Group__1"


    // $ANTLR start "rule__ManyToMany__Group__1__Impl"
    // InternalOaas.g:2527:1: rule__ManyToMany__Group__1__Impl : ( 'ManyToMany' ) ;
    public final void rule__ManyToMany__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2531:1: ( ( 'ManyToMany' ) )
            // InternalOaas.g:2532:1: ( 'ManyToMany' )
            {
            // InternalOaas.g:2532:1: ( 'ManyToMany' )
            // InternalOaas.g:2533:2: 'ManyToMany'
            {
             before(grammarAccess.getManyToManyAccess().getManyToManyKeyword_1()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getManyToManyAccess().getManyToManyKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyToMany__Group__1__Impl"


    // $ANTLR start "rule__ManyToMany__Group__2"
    // InternalOaas.g:2542:1: rule__ManyToMany__Group__2 : rule__ManyToMany__Group__2__Impl ;
    public final void rule__ManyToMany__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2546:1: ( rule__ManyToMany__Group__2__Impl )
            // InternalOaas.g:2547:2: rule__ManyToMany__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ManyToMany__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyToMany__Group__2"


    // $ANTLR start "rule__ManyToMany__Group__2__Impl"
    // InternalOaas.g:2553:1: rule__ManyToMany__Group__2__Impl : ( ( rule__ManyToMany__TypeAssignment_2 ) ) ;
    public final void rule__ManyToMany__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2557:1: ( ( ( rule__ManyToMany__TypeAssignment_2 ) ) )
            // InternalOaas.g:2558:1: ( ( rule__ManyToMany__TypeAssignment_2 ) )
            {
            // InternalOaas.g:2558:1: ( ( rule__ManyToMany__TypeAssignment_2 ) )
            // InternalOaas.g:2559:2: ( rule__ManyToMany__TypeAssignment_2 )
            {
             before(grammarAccess.getManyToManyAccess().getTypeAssignment_2()); 
            // InternalOaas.g:2560:2: ( rule__ManyToMany__TypeAssignment_2 )
            // InternalOaas.g:2560:3: rule__ManyToMany__TypeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__ManyToMany__TypeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getManyToManyAccess().getTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyToMany__Group__2__Impl"


    // $ANTLR start "rule__OneToMany__Group__0"
    // InternalOaas.g:2569:1: rule__OneToMany__Group__0 : rule__OneToMany__Group__0__Impl rule__OneToMany__Group__1 ;
    public final void rule__OneToMany__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2573:1: ( rule__OneToMany__Group__0__Impl rule__OneToMany__Group__1 )
            // InternalOaas.g:2574:2: rule__OneToMany__Group__0__Impl rule__OneToMany__Group__1
            {
            pushFollow(FOLLOW_33);
            rule__OneToMany__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OneToMany__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToMany__Group__0"


    // $ANTLR start "rule__OneToMany__Group__0__Impl"
    // InternalOaas.g:2581:1: rule__OneToMany__Group__0__Impl : ( ( rule__OneToMany__NameAssignment_0 ) ) ;
    public final void rule__OneToMany__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2585:1: ( ( ( rule__OneToMany__NameAssignment_0 ) ) )
            // InternalOaas.g:2586:1: ( ( rule__OneToMany__NameAssignment_0 ) )
            {
            // InternalOaas.g:2586:1: ( ( rule__OneToMany__NameAssignment_0 ) )
            // InternalOaas.g:2587:2: ( rule__OneToMany__NameAssignment_0 )
            {
             before(grammarAccess.getOneToManyAccess().getNameAssignment_0()); 
            // InternalOaas.g:2588:2: ( rule__OneToMany__NameAssignment_0 )
            // InternalOaas.g:2588:3: rule__OneToMany__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__OneToMany__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getOneToManyAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToMany__Group__0__Impl"


    // $ANTLR start "rule__OneToMany__Group__1"
    // InternalOaas.g:2596:1: rule__OneToMany__Group__1 : rule__OneToMany__Group__1__Impl rule__OneToMany__Group__2 ;
    public final void rule__OneToMany__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2600:1: ( rule__OneToMany__Group__1__Impl rule__OneToMany__Group__2 )
            // InternalOaas.g:2601:2: rule__OneToMany__Group__1__Impl rule__OneToMany__Group__2
            {
            pushFollow(FOLLOW_17);
            rule__OneToMany__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OneToMany__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToMany__Group__1"


    // $ANTLR start "rule__OneToMany__Group__1__Impl"
    // InternalOaas.g:2608:1: rule__OneToMany__Group__1__Impl : ( 'OneToMany' ) ;
    public final void rule__OneToMany__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2612:1: ( ( 'OneToMany' ) )
            // InternalOaas.g:2613:1: ( 'OneToMany' )
            {
            // InternalOaas.g:2613:1: ( 'OneToMany' )
            // InternalOaas.g:2614:2: 'OneToMany'
            {
             before(grammarAccess.getOneToManyAccess().getOneToManyKeyword_1()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getOneToManyAccess().getOneToManyKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToMany__Group__1__Impl"


    // $ANTLR start "rule__OneToMany__Group__2"
    // InternalOaas.g:2623:1: rule__OneToMany__Group__2 : rule__OneToMany__Group__2__Impl ;
    public final void rule__OneToMany__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2627:1: ( rule__OneToMany__Group__2__Impl )
            // InternalOaas.g:2628:2: rule__OneToMany__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OneToMany__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToMany__Group__2"


    // $ANTLR start "rule__OneToMany__Group__2__Impl"
    // InternalOaas.g:2634:1: rule__OneToMany__Group__2__Impl : ( ( rule__OneToMany__TypeAssignment_2 ) ) ;
    public final void rule__OneToMany__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2638:1: ( ( ( rule__OneToMany__TypeAssignment_2 ) ) )
            // InternalOaas.g:2639:1: ( ( rule__OneToMany__TypeAssignment_2 ) )
            {
            // InternalOaas.g:2639:1: ( ( rule__OneToMany__TypeAssignment_2 ) )
            // InternalOaas.g:2640:2: ( rule__OneToMany__TypeAssignment_2 )
            {
             before(grammarAccess.getOneToManyAccess().getTypeAssignment_2()); 
            // InternalOaas.g:2641:2: ( rule__OneToMany__TypeAssignment_2 )
            // InternalOaas.g:2641:3: rule__OneToMany__TypeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__OneToMany__TypeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getOneToManyAccess().getTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToMany__Group__2__Impl"


    // $ANTLR start "rule__Function__Group__0"
    // InternalOaas.g:2650:1: rule__Function__Group__0 : rule__Function__Group__0__Impl rule__Function__Group__1 ;
    public final void rule__Function__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2654:1: ( rule__Function__Group__0__Impl rule__Function__Group__1 )
            // InternalOaas.g:2655:2: rule__Function__Group__0__Impl rule__Function__Group__1
            {
            pushFollow(FOLLOW_34);
            rule__Function__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__0"


    // $ANTLR start "rule__Function__Group__0__Impl"
    // InternalOaas.g:2662:1: rule__Function__Group__0__Impl : ( ( rule__Function__DescriptionAssignment_0 )? ) ;
    public final void rule__Function__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2666:1: ( ( ( rule__Function__DescriptionAssignment_0 )? ) )
            // InternalOaas.g:2667:1: ( ( rule__Function__DescriptionAssignment_0 )? )
            {
            // InternalOaas.g:2667:1: ( ( rule__Function__DescriptionAssignment_0 )? )
            // InternalOaas.g:2668:2: ( rule__Function__DescriptionAssignment_0 )?
            {
             before(grammarAccess.getFunctionAccess().getDescriptionAssignment_0()); 
            // InternalOaas.g:2669:2: ( rule__Function__DescriptionAssignment_0 )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==22) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalOaas.g:2669:3: rule__Function__DescriptionAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Function__DescriptionAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFunctionAccess().getDescriptionAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__0__Impl"


    // $ANTLR start "rule__Function__Group__1"
    // InternalOaas.g:2677:1: rule__Function__Group__1 : rule__Function__Group__1__Impl rule__Function__Group__2 ;
    public final void rule__Function__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2681:1: ( rule__Function__Group__1__Impl rule__Function__Group__2 )
            // InternalOaas.g:2682:2: rule__Function__Group__1__Impl rule__Function__Group__2
            {
            pushFollow(FOLLOW_17);
            rule__Function__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__1"


    // $ANTLR start "rule__Function__Group__1__Impl"
    // InternalOaas.g:2689:1: rule__Function__Group__1__Impl : ( 'function' ) ;
    public final void rule__Function__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2693:1: ( ( 'function' ) )
            // InternalOaas.g:2694:1: ( 'function' )
            {
            // InternalOaas.g:2694:1: ( 'function' )
            // InternalOaas.g:2695:2: 'function'
            {
             before(grammarAccess.getFunctionAccess().getFunctionKeyword_1()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getFunctionKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__1__Impl"


    // $ANTLR start "rule__Function__Group__2"
    // InternalOaas.g:2704:1: rule__Function__Group__2 : rule__Function__Group__2__Impl rule__Function__Group__3 ;
    public final void rule__Function__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2708:1: ( rule__Function__Group__2__Impl rule__Function__Group__3 )
            // InternalOaas.g:2709:2: rule__Function__Group__2__Impl rule__Function__Group__3
            {
            pushFollow(FOLLOW_35);
            rule__Function__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__2"


    // $ANTLR start "rule__Function__Group__2__Impl"
    // InternalOaas.g:2716:1: rule__Function__Group__2__Impl : ( ( rule__Function__NameAssignment_2 ) ) ;
    public final void rule__Function__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2720:1: ( ( ( rule__Function__NameAssignment_2 ) ) )
            // InternalOaas.g:2721:1: ( ( rule__Function__NameAssignment_2 ) )
            {
            // InternalOaas.g:2721:1: ( ( rule__Function__NameAssignment_2 ) )
            // InternalOaas.g:2722:2: ( rule__Function__NameAssignment_2 )
            {
             before(grammarAccess.getFunctionAccess().getNameAssignment_2()); 
            // InternalOaas.g:2723:2: ( rule__Function__NameAssignment_2 )
            // InternalOaas.g:2723:3: rule__Function__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Function__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__2__Impl"


    // $ANTLR start "rule__Function__Group__3"
    // InternalOaas.g:2731:1: rule__Function__Group__3 : rule__Function__Group__3__Impl rule__Function__Group__4 ;
    public final void rule__Function__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2735:1: ( rule__Function__Group__3__Impl rule__Function__Group__4 )
            // InternalOaas.g:2736:2: rule__Function__Group__3__Impl rule__Function__Group__4
            {
            pushFollow(FOLLOW_36);
            rule__Function__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__3"


    // $ANTLR start "rule__Function__Group__3__Impl"
    // InternalOaas.g:2743:1: rule__Function__Group__3__Impl : ( '(' ) ;
    public final void rule__Function__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2747:1: ( ( '(' ) )
            // InternalOaas.g:2748:1: ( '(' )
            {
            // InternalOaas.g:2748:1: ( '(' )
            // InternalOaas.g:2749:2: '('
            {
             before(grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_3()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__3__Impl"


    // $ANTLR start "rule__Function__Group__4"
    // InternalOaas.g:2758:1: rule__Function__Group__4 : rule__Function__Group__4__Impl rule__Function__Group__5 ;
    public final void rule__Function__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2762:1: ( rule__Function__Group__4__Impl rule__Function__Group__5 )
            // InternalOaas.g:2763:2: rule__Function__Group__4__Impl rule__Function__Group__5
            {
            pushFollow(FOLLOW_36);
            rule__Function__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__4"


    // $ANTLR start "rule__Function__Group__4__Impl"
    // InternalOaas.g:2770:1: rule__Function__Group__4__Impl : ( ( rule__Function__Group_4__0 )? ) ;
    public final void rule__Function__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2774:1: ( ( ( rule__Function__Group_4__0 )? ) )
            // InternalOaas.g:2775:1: ( ( rule__Function__Group_4__0 )? )
            {
            // InternalOaas.g:2775:1: ( ( rule__Function__Group_4__0 )? )
            // InternalOaas.g:2776:2: ( rule__Function__Group_4__0 )?
            {
             before(grammarAccess.getFunctionAccess().getGroup_4()); 
            // InternalOaas.g:2777:2: ( rule__Function__Group_4__0 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==RULE_ID) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalOaas.g:2777:3: rule__Function__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Function__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFunctionAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__4__Impl"


    // $ANTLR start "rule__Function__Group__5"
    // InternalOaas.g:2785:1: rule__Function__Group__5 : rule__Function__Group__5__Impl rule__Function__Group__6 ;
    public final void rule__Function__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2789:1: ( rule__Function__Group__5__Impl rule__Function__Group__6 )
            // InternalOaas.g:2790:2: rule__Function__Group__5__Impl rule__Function__Group__6
            {
            pushFollow(FOLLOW_30);
            rule__Function__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__5"


    // $ANTLR start "rule__Function__Group__5__Impl"
    // InternalOaas.g:2797:1: rule__Function__Group__5__Impl : ( ')' ) ;
    public final void rule__Function__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2801:1: ( ( ')' ) )
            // InternalOaas.g:2802:1: ( ')' )
            {
            // InternalOaas.g:2802:1: ( ')' )
            // InternalOaas.g:2803:2: ')'
            {
             before(grammarAccess.getFunctionAccess().getRightParenthesisKeyword_5()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getRightParenthesisKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__5__Impl"


    // $ANTLR start "rule__Function__Group__6"
    // InternalOaas.g:2812:1: rule__Function__Group__6 : rule__Function__Group__6__Impl rule__Function__Group__7 ;
    public final void rule__Function__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2816:1: ( rule__Function__Group__6__Impl rule__Function__Group__7 )
            // InternalOaas.g:2817:2: rule__Function__Group__6__Impl rule__Function__Group__7
            {
            pushFollow(FOLLOW_17);
            rule__Function__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__6"


    // $ANTLR start "rule__Function__Group__6__Impl"
    // InternalOaas.g:2824:1: rule__Function__Group__6__Impl : ( ':' ) ;
    public final void rule__Function__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2828:1: ( ( ':' ) )
            // InternalOaas.g:2829:1: ( ':' )
            {
            // InternalOaas.g:2829:1: ( ':' )
            // InternalOaas.g:2830:2: ':'
            {
             before(grammarAccess.getFunctionAccess().getColonKeyword_6()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getColonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__6__Impl"


    // $ANTLR start "rule__Function__Group__7"
    // InternalOaas.g:2839:1: rule__Function__Group__7 : rule__Function__Group__7__Impl ;
    public final void rule__Function__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2843:1: ( rule__Function__Group__7__Impl )
            // InternalOaas.g:2844:2: rule__Function__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Function__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__7"


    // $ANTLR start "rule__Function__Group__7__Impl"
    // InternalOaas.g:2850:1: rule__Function__Group__7__Impl : ( ( rule__Function__TypeAssignment_7 ) ) ;
    public final void rule__Function__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2854:1: ( ( ( rule__Function__TypeAssignment_7 ) ) )
            // InternalOaas.g:2855:1: ( ( rule__Function__TypeAssignment_7 ) )
            {
            // InternalOaas.g:2855:1: ( ( rule__Function__TypeAssignment_7 ) )
            // InternalOaas.g:2856:2: ( rule__Function__TypeAssignment_7 )
            {
             before(grammarAccess.getFunctionAccess().getTypeAssignment_7()); 
            // InternalOaas.g:2857:2: ( rule__Function__TypeAssignment_7 )
            // InternalOaas.g:2857:3: rule__Function__TypeAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__Function__TypeAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getTypeAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__7__Impl"


    // $ANTLR start "rule__Function__Group_4__0"
    // InternalOaas.g:2866:1: rule__Function__Group_4__0 : rule__Function__Group_4__0__Impl rule__Function__Group_4__1 ;
    public final void rule__Function__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2870:1: ( rule__Function__Group_4__0__Impl rule__Function__Group_4__1 )
            // InternalOaas.g:2871:2: rule__Function__Group_4__0__Impl rule__Function__Group_4__1
            {
            pushFollow(FOLLOW_37);
            rule__Function__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_4__0"


    // $ANTLR start "rule__Function__Group_4__0__Impl"
    // InternalOaas.g:2878:1: rule__Function__Group_4__0__Impl : ( ( rule__Function__ParamsAssignment_4_0 ) ) ;
    public final void rule__Function__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2882:1: ( ( ( rule__Function__ParamsAssignment_4_0 ) ) )
            // InternalOaas.g:2883:1: ( ( rule__Function__ParamsAssignment_4_0 ) )
            {
            // InternalOaas.g:2883:1: ( ( rule__Function__ParamsAssignment_4_0 ) )
            // InternalOaas.g:2884:2: ( rule__Function__ParamsAssignment_4_0 )
            {
             before(grammarAccess.getFunctionAccess().getParamsAssignment_4_0()); 
            // InternalOaas.g:2885:2: ( rule__Function__ParamsAssignment_4_0 )
            // InternalOaas.g:2885:3: rule__Function__ParamsAssignment_4_0
            {
            pushFollow(FOLLOW_2);
            rule__Function__ParamsAssignment_4_0();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getParamsAssignment_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_4__0__Impl"


    // $ANTLR start "rule__Function__Group_4__1"
    // InternalOaas.g:2893:1: rule__Function__Group_4__1 : rule__Function__Group_4__1__Impl ;
    public final void rule__Function__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2897:1: ( rule__Function__Group_4__1__Impl )
            // InternalOaas.g:2898:2: rule__Function__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Function__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_4__1"


    // $ANTLR start "rule__Function__Group_4__1__Impl"
    // InternalOaas.g:2904:1: rule__Function__Group_4__1__Impl : ( ( rule__Function__Group_4_1__0 )* ) ;
    public final void rule__Function__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2908:1: ( ( ( rule__Function__Group_4_1__0 )* ) )
            // InternalOaas.g:2909:1: ( ( rule__Function__Group_4_1__0 )* )
            {
            // InternalOaas.g:2909:1: ( ( rule__Function__Group_4_1__0 )* )
            // InternalOaas.g:2910:2: ( rule__Function__Group_4_1__0 )*
            {
             before(grammarAccess.getFunctionAccess().getGroup_4_1()); 
            // InternalOaas.g:2911:2: ( rule__Function__Group_4_1__0 )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==37) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalOaas.g:2911:3: rule__Function__Group_4_1__0
            	    {
            	    pushFollow(FOLLOW_38);
            	    rule__Function__Group_4_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

             after(grammarAccess.getFunctionAccess().getGroup_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_4__1__Impl"


    // $ANTLR start "rule__Function__Group_4_1__0"
    // InternalOaas.g:2920:1: rule__Function__Group_4_1__0 : rule__Function__Group_4_1__0__Impl rule__Function__Group_4_1__1 ;
    public final void rule__Function__Group_4_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2924:1: ( rule__Function__Group_4_1__0__Impl rule__Function__Group_4_1__1 )
            // InternalOaas.g:2925:2: rule__Function__Group_4_1__0__Impl rule__Function__Group_4_1__1
            {
            pushFollow(FOLLOW_17);
            rule__Function__Group_4_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group_4_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_4_1__0"


    // $ANTLR start "rule__Function__Group_4_1__0__Impl"
    // InternalOaas.g:2932:1: rule__Function__Group_4_1__0__Impl : ( ',' ) ;
    public final void rule__Function__Group_4_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2936:1: ( ( ',' ) )
            // InternalOaas.g:2937:1: ( ',' )
            {
            // InternalOaas.g:2937:1: ( ',' )
            // InternalOaas.g:2938:2: ','
            {
             before(grammarAccess.getFunctionAccess().getCommaKeyword_4_1_0()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getCommaKeyword_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_4_1__0__Impl"


    // $ANTLR start "rule__Function__Group_4_1__1"
    // InternalOaas.g:2947:1: rule__Function__Group_4_1__1 : rule__Function__Group_4_1__1__Impl ;
    public final void rule__Function__Group_4_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2951:1: ( rule__Function__Group_4_1__1__Impl )
            // InternalOaas.g:2952:2: rule__Function__Group_4_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Function__Group_4_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_4_1__1"


    // $ANTLR start "rule__Function__Group_4_1__1__Impl"
    // InternalOaas.g:2958:1: rule__Function__Group_4_1__1__Impl : ( ( rule__Function__ParamsAssignment_4_1_1 ) ) ;
    public final void rule__Function__Group_4_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2962:1: ( ( ( rule__Function__ParamsAssignment_4_1_1 ) ) )
            // InternalOaas.g:2963:1: ( ( rule__Function__ParamsAssignment_4_1_1 ) )
            {
            // InternalOaas.g:2963:1: ( ( rule__Function__ParamsAssignment_4_1_1 ) )
            // InternalOaas.g:2964:2: ( rule__Function__ParamsAssignment_4_1_1 )
            {
             before(grammarAccess.getFunctionAccess().getParamsAssignment_4_1_1()); 
            // InternalOaas.g:2965:2: ( rule__Function__ParamsAssignment_4_1_1 )
            // InternalOaas.g:2965:3: rule__Function__ParamsAssignment_4_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Function__ParamsAssignment_4_1_1();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getParamsAssignment_4_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_4_1__1__Impl"


    // $ANTLR start "rule__Application__ConfigurationAssignment_0"
    // InternalOaas.g:2974:1: rule__Application__ConfigurationAssignment_0 : ( ruleConfiguration ) ;
    public final void rule__Application__ConfigurationAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2978:1: ( ( ruleConfiguration ) )
            // InternalOaas.g:2979:2: ( ruleConfiguration )
            {
            // InternalOaas.g:2979:2: ( ruleConfiguration )
            // InternalOaas.g:2980:3: ruleConfiguration
            {
             before(grammarAccess.getApplicationAccess().getConfigurationConfigurationParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleConfiguration();

            state._fsp--;

             after(grammarAccess.getApplicationAccess().getConfigurationConfigurationParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Application__ConfigurationAssignment_0"


    // $ANTLR start "rule__Application__AbstractElementsAssignment_1"
    // InternalOaas.g:2989:1: rule__Application__AbstractElementsAssignment_1 : ( ruleAbstractElement ) ;
    public final void rule__Application__AbstractElementsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:2993:1: ( ( ruleAbstractElement ) )
            // InternalOaas.g:2994:2: ( ruleAbstractElement )
            {
            // InternalOaas.g:2994:2: ( ruleAbstractElement )
            // InternalOaas.g:2995:3: ruleAbstractElement
            {
             before(grammarAccess.getApplicationAccess().getAbstractElementsAbstractElementParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAbstractElement();

            state._fsp--;

             after(grammarAccess.getApplicationAccess().getAbstractElementsAbstractElementParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Application__AbstractElementsAssignment_1"


    // $ANTLR start "rule__Configuration__OntologyAssignment_2"
    // InternalOaas.g:3004:1: rule__Configuration__OntologyAssignment_2 : ( ruleOntology ) ;
    public final void rule__Configuration__OntologyAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3008:1: ( ( ruleOntology ) )
            // InternalOaas.g:3009:2: ( ruleOntology )
            {
            // InternalOaas.g:3009:2: ( ruleOntology )
            // InternalOaas.g:3010:3: ruleOntology
            {
             before(grammarAccess.getConfigurationAccess().getOntologyOntologyParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleOntology();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getOntologyOntologyParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__OntologyAssignment_2"


    // $ANTLR start "rule__Configuration__OntologyShortNameAssignment_3"
    // InternalOaas.g:3019:1: rule__Configuration__OntologyShortNameAssignment_3 : ( ruleOntologyShortName ) ;
    public final void rule__Configuration__OntologyShortNameAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3023:1: ( ( ruleOntologyShortName ) )
            // InternalOaas.g:3024:2: ( ruleOntologyShortName )
            {
            // InternalOaas.g:3024:2: ( ruleOntologyShortName )
            // InternalOaas.g:3025:3: ruleOntologyShortName
            {
             before(grammarAccess.getConfigurationAccess().getOntologyShortNameOntologyShortNameParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleOntologyShortName();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getOntologyShortNameOntologyShortNameParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__OntologyShortNameAssignment_3"


    // $ANTLR start "rule__Configuration__LevelAssignment_4"
    // InternalOaas.g:3034:1: rule__Configuration__LevelAssignment_4 : ( ruleLevel ) ;
    public final void rule__Configuration__LevelAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3038:1: ( ( ruleLevel ) )
            // InternalOaas.g:3039:2: ( ruleLevel )
            {
            // InternalOaas.g:3039:2: ( ruleLevel )
            // InternalOaas.g:3040:3: ruleLevel
            {
             before(grammarAccess.getConfigurationAccess().getLevelLevelParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleLevel();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getLevelLevelParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__LevelAssignment_4"


    // $ANTLR start "rule__Configuration__AboutAssignment_5"
    // InternalOaas.g:3049:1: rule__Configuration__AboutAssignment_5 : ( ruleAbout ) ;
    public final void rule__Configuration__AboutAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3053:1: ( ( ruleAbout ) )
            // InternalOaas.g:3054:2: ( ruleAbout )
            {
            // InternalOaas.g:3054:2: ( ruleAbout )
            // InternalOaas.g:3055:3: ruleAbout
            {
             before(grammarAccess.getConfigurationAccess().getAboutAboutParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleAbout();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getAboutAboutParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__AboutAssignment_5"


    // $ANTLR start "rule__Configuration__SoftwareAssignment_6"
    // InternalOaas.g:3064:1: rule__Configuration__SoftwareAssignment_6 : ( ruleSoftware ) ;
    public final void rule__Configuration__SoftwareAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3068:1: ( ( ruleSoftware ) )
            // InternalOaas.g:3069:2: ( ruleSoftware )
            {
            // InternalOaas.g:3069:2: ( ruleSoftware )
            // InternalOaas.g:3070:3: ruleSoftware
            {
             before(grammarAccess.getConfigurationAccess().getSoftwareSoftwareParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleSoftware();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getSoftwareSoftwareParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__SoftwareAssignment_6"


    // $ANTLR start "rule__Configuration__AuthorAssignment_7"
    // InternalOaas.g:3079:1: rule__Configuration__AuthorAssignment_7 : ( ruleAuthor ) ;
    public final void rule__Configuration__AuthorAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3083:1: ( ( ruleAuthor ) )
            // InternalOaas.g:3084:2: ( ruleAuthor )
            {
            // InternalOaas.g:3084:2: ( ruleAuthor )
            // InternalOaas.g:3085:3: ruleAuthor
            {
             before(grammarAccess.getConfigurationAccess().getAuthorAuthorParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleAuthor();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getAuthorAuthorParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__AuthorAssignment_7"


    // $ANTLR start "rule__Configuration__Author_emailAssignment_8"
    // InternalOaas.g:3094:1: rule__Configuration__Author_emailAssignment_8 : ( ruleAuthor_Email ) ;
    public final void rule__Configuration__Author_emailAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3098:1: ( ( ruleAuthor_Email ) )
            // InternalOaas.g:3099:2: ( ruleAuthor_Email )
            {
            // InternalOaas.g:3099:2: ( ruleAuthor_Email )
            // InternalOaas.g:3100:3: ruleAuthor_Email
            {
             before(grammarAccess.getConfigurationAccess().getAuthor_emailAuthor_EmailParserRuleCall_8_0()); 
            pushFollow(FOLLOW_2);
            ruleAuthor_Email();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getAuthor_emailAuthor_EmailParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Author_emailAssignment_8"


    // $ANTLR start "rule__Configuration__RepositoryAssignment_9"
    // InternalOaas.g:3109:1: rule__Configuration__RepositoryAssignment_9 : ( ruleRepository ) ;
    public final void rule__Configuration__RepositoryAssignment_9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3113:1: ( ( ruleRepository ) )
            // InternalOaas.g:3114:2: ( ruleRepository )
            {
            // InternalOaas.g:3114:2: ( ruleRepository )
            // InternalOaas.g:3115:3: ruleRepository
            {
             before(grammarAccess.getConfigurationAccess().getRepositoryRepositoryParserRuleCall_9_0()); 
            pushFollow(FOLLOW_2);
            ruleRepository();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getRepositoryRepositoryParserRuleCall_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__RepositoryAssignment_9"


    // $ANTLR start "rule__Ontology__NameAssignment_1"
    // InternalOaas.g:3124:1: rule__Ontology__NameAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Ontology__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3128:1: ( ( RULE_STRING ) )
            // InternalOaas.g:3129:2: ( RULE_STRING )
            {
            // InternalOaas.g:3129:2: ( RULE_STRING )
            // InternalOaas.g:3130:3: RULE_STRING
            {
             before(grammarAccess.getOntologyAccess().getNameSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getOntologyAccess().getNameSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ontology__NameAssignment_1"


    // $ANTLR start "rule__Level__NameAssignment_1"
    // InternalOaas.g:3139:1: rule__Level__NameAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Level__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3143:1: ( ( RULE_STRING ) )
            // InternalOaas.g:3144:2: ( RULE_STRING )
            {
            // InternalOaas.g:3144:2: ( RULE_STRING )
            // InternalOaas.g:3145:3: RULE_STRING
            {
             before(grammarAccess.getLevelAccess().getNameSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getLevelAccess().getNameSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Level__NameAssignment_1"


    // $ANTLR start "rule__Author__NameAssignment_1"
    // InternalOaas.g:3154:1: rule__Author__NameAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Author__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3158:1: ( ( RULE_STRING ) )
            // InternalOaas.g:3159:2: ( RULE_STRING )
            {
            // InternalOaas.g:3159:2: ( RULE_STRING )
            // InternalOaas.g:3160:3: RULE_STRING
            {
             before(grammarAccess.getAuthorAccess().getNameSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getAuthorAccess().getNameSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Author__NameAssignment_1"


    // $ANTLR start "rule__Author_Email__NameAssignment_1"
    // InternalOaas.g:3169:1: rule__Author_Email__NameAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Author_Email__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3173:1: ( ( RULE_STRING ) )
            // InternalOaas.g:3174:2: ( RULE_STRING )
            {
            // InternalOaas.g:3174:2: ( RULE_STRING )
            // InternalOaas.g:3175:3: RULE_STRING
            {
             before(grammarAccess.getAuthor_EmailAccess().getNameSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getAuthor_EmailAccess().getNameSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Author_Email__NameAssignment_1"


    // $ANTLR start "rule__Repository__NameAssignment_1"
    // InternalOaas.g:3184:1: rule__Repository__NameAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Repository__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3188:1: ( ( RULE_STRING ) )
            // InternalOaas.g:3189:2: ( RULE_STRING )
            {
            // InternalOaas.g:3189:2: ( RULE_STRING )
            // InternalOaas.g:3190:3: RULE_STRING
            {
             before(grammarAccess.getRepositoryAccess().getNameSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getRepositoryAccess().getNameSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__NameAssignment_1"


    // $ANTLR start "rule__OntologyShortName__NameAssignment_1"
    // InternalOaas.g:3199:1: rule__OntologyShortName__NameAssignment_1 : ( RULE_STRING ) ;
    public final void rule__OntologyShortName__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3203:1: ( ( RULE_STRING ) )
            // InternalOaas.g:3204:2: ( RULE_STRING )
            {
            // InternalOaas.g:3204:2: ( RULE_STRING )
            // InternalOaas.g:3205:3: RULE_STRING
            {
             before(grammarAccess.getOntologyShortNameAccess().getNameSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getOntologyShortNameAccess().getNameSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OntologyShortName__NameAssignment_1"


    // $ANTLR start "rule__Software__NameAssignment_1"
    // InternalOaas.g:3214:1: rule__Software__NameAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Software__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3218:1: ( ( RULE_STRING ) )
            // InternalOaas.g:3219:2: ( RULE_STRING )
            {
            // InternalOaas.g:3219:2: ( RULE_STRING )
            // InternalOaas.g:3220:3: RULE_STRING
            {
             before(grammarAccess.getSoftwareAccess().getNameSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getSoftwareAccess().getNameSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Software__NameAssignment_1"


    // $ANTLR start "rule__About__NameAssignment_1"
    // InternalOaas.g:3229:1: rule__About__NameAssignment_1 : ( RULE_STRING ) ;
    public final void rule__About__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3233:1: ( ( RULE_STRING ) )
            // InternalOaas.g:3234:2: ( RULE_STRING )
            {
            // InternalOaas.g:3234:2: ( RULE_STRING )
            // InternalOaas.g:3235:3: RULE_STRING
            {
             before(grammarAccess.getAboutAccess().getNameSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getAboutAccess().getNameSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__About__NameAssignment_1"


    // $ANTLR start "rule__Description__TextfieldAssignment_1"
    // InternalOaas.g:3244:1: rule__Description__TextfieldAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Description__TextfieldAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3248:1: ( ( RULE_STRING ) )
            // InternalOaas.g:3249:2: ( RULE_STRING )
            {
            // InternalOaas.g:3249:2: ( RULE_STRING )
            // InternalOaas.g:3250:3: RULE_STRING
            {
             before(grammarAccess.getDescriptionAccess().getTextfieldSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getDescriptionAccess().getTextfieldSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__TextfieldAssignment_1"


    // $ANTLR start "rule__Entity_Type__NameAssignment_1"
    // InternalOaas.g:3259:1: rule__Entity_Type__NameAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Entity_Type__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3263:1: ( ( RULE_STRING ) )
            // InternalOaas.g:3264:2: ( RULE_STRING )
            {
            // InternalOaas.g:3264:2: ( RULE_STRING )
            // InternalOaas.g:3265:3: RULE_STRING
            {
             before(grammarAccess.getEntity_TypeAccess().getNameSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getEntity_TypeAccess().getNameSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity_Type__NameAssignment_1"


    // $ANTLR start "rule__Module__DescriptionAssignment_0"
    // InternalOaas.g:3274:1: rule__Module__DescriptionAssignment_0 : ( ruleDescription ) ;
    public final void rule__Module__DescriptionAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3278:1: ( ( ruleDescription ) )
            // InternalOaas.g:3279:2: ( ruleDescription )
            {
            // InternalOaas.g:3279:2: ( ruleDescription )
            // InternalOaas.g:3280:3: ruleDescription
            {
             before(grammarAccess.getModuleAccess().getDescriptionDescriptionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleDescription();

            state._fsp--;

             after(grammarAccess.getModuleAccess().getDescriptionDescriptionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__DescriptionAssignment_0"


    // $ANTLR start "rule__Module__NameAssignment_2"
    // InternalOaas.g:3289:1: rule__Module__NameAssignment_2 : ( ruleQualifiedName ) ;
    public final void rule__Module__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3293:1: ( ( ruleQualifiedName ) )
            // InternalOaas.g:3294:2: ( ruleQualifiedName )
            {
            // InternalOaas.g:3294:2: ( ruleQualifiedName )
            // InternalOaas.g:3295:3: ruleQualifiedName
            {
             before(grammarAccess.getModuleAccess().getNameQualifiedNameParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getModuleAccess().getNameQualifiedNameParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__NameAssignment_2"


    // $ANTLR start "rule__Module__ElementsAssignment_4"
    // InternalOaas.g:3304:1: rule__Module__ElementsAssignment_4 : ( ruleAbstractElement ) ;
    public final void rule__Module__ElementsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3308:1: ( ( ruleAbstractElement ) )
            // InternalOaas.g:3309:2: ( ruleAbstractElement )
            {
            // InternalOaas.g:3309:2: ( ruleAbstractElement )
            // InternalOaas.g:3310:3: ruleAbstractElement
            {
             before(grammarAccess.getModuleAccess().getElementsAbstractElementParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleAbstractElement();

            state._fsp--;

             after(grammarAccess.getModuleAccess().getElementsAbstractElementParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__ElementsAssignment_4"


    // $ANTLR start "rule__Import__ImportedNamespaceAssignment_1"
    // InternalOaas.g:3319:1: rule__Import__ImportedNamespaceAssignment_1 : ( ruleQualifiedNameWithWildcard ) ;
    public final void rule__Import__ImportedNamespaceAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3323:1: ( ( ruleQualifiedNameWithWildcard ) )
            // InternalOaas.g:3324:2: ( ruleQualifiedNameWithWildcard )
            {
            // InternalOaas.g:3324:2: ( ruleQualifiedNameWithWildcard )
            // InternalOaas.g:3325:3: ruleQualifiedNameWithWildcard
            {
             before(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__ImportedNamespaceAssignment_1"


    // $ANTLR start "rule__Entity__DescriptionAssignment_0"
    // InternalOaas.g:3334:1: rule__Entity__DescriptionAssignment_0 : ( ruleDescription ) ;
    public final void rule__Entity__DescriptionAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3338:1: ( ( ruleDescription ) )
            // InternalOaas.g:3339:2: ( ruleDescription )
            {
            // InternalOaas.g:3339:2: ( ruleDescription )
            // InternalOaas.g:3340:3: ruleDescription
            {
             before(grammarAccess.getEntityAccess().getDescriptionDescriptionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleDescription();

            state._fsp--;

             after(grammarAccess.getEntityAccess().getDescriptionDescriptionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__DescriptionAssignment_0"


    // $ANTLR start "rule__Entity__NameAssignment_2"
    // InternalOaas.g:3349:1: rule__Entity__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Entity__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3353:1: ( ( RULE_ID ) )
            // InternalOaas.g:3354:2: ( RULE_ID )
            {
            // InternalOaas.g:3354:2: ( RULE_ID )
            // InternalOaas.g:3355:3: RULE_ID
            {
             before(grammarAccess.getEntityAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getEntityAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__NameAssignment_2"


    // $ANTLR start "rule__Entity__SuperTypeAssignment_3_1"
    // InternalOaas.g:3364:1: rule__Entity__SuperTypeAssignment_3_1 : ( ( ruleQualifiedName ) ) ;
    public final void rule__Entity__SuperTypeAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3368:1: ( ( ( ruleQualifiedName ) ) )
            // InternalOaas.g:3369:2: ( ( ruleQualifiedName ) )
            {
            // InternalOaas.g:3369:2: ( ( ruleQualifiedName ) )
            // InternalOaas.g:3370:3: ( ruleQualifiedName )
            {
             before(grammarAccess.getEntityAccess().getSuperTypeEntityCrossReference_3_1_0()); 
            // InternalOaas.g:3371:3: ( ruleQualifiedName )
            // InternalOaas.g:3372:4: ruleQualifiedName
            {
             before(grammarAccess.getEntityAccess().getSuperTypeEntityQualifiedNameParserRuleCall_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getEntityAccess().getSuperTypeEntityQualifiedNameParserRuleCall_3_1_0_1()); 

            }

             after(grammarAccess.getEntityAccess().getSuperTypeEntityCrossReference_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__SuperTypeAssignment_3_1"


    // $ANTLR start "rule__Entity__Entity_typeAssignment_5"
    // InternalOaas.g:3383:1: rule__Entity__Entity_typeAssignment_5 : ( ruleEntity_Type ) ;
    public final void rule__Entity__Entity_typeAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3387:1: ( ( ruleEntity_Type ) )
            // InternalOaas.g:3388:2: ( ruleEntity_Type )
            {
            // InternalOaas.g:3388:2: ( ruleEntity_Type )
            // InternalOaas.g:3389:3: ruleEntity_Type
            {
             before(grammarAccess.getEntityAccess().getEntity_typeEntity_TypeParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleEntity_Type();

            state._fsp--;

             after(grammarAccess.getEntityAccess().getEntity_typeEntity_TypeParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Entity_typeAssignment_5"


    // $ANTLR start "rule__Entity__AttributesAssignment_6"
    // InternalOaas.g:3398:1: rule__Entity__AttributesAssignment_6 : ( ruleAttribute ) ;
    public final void rule__Entity__AttributesAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3402:1: ( ( ruleAttribute ) )
            // InternalOaas.g:3403:2: ( ruleAttribute )
            {
            // InternalOaas.g:3403:2: ( ruleAttribute )
            // InternalOaas.g:3404:3: ruleAttribute
            {
             before(grammarAccess.getEntityAccess().getAttributesAttributeParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleAttribute();

            state._fsp--;

             after(grammarAccess.getEntityAccess().getAttributesAttributeParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__AttributesAssignment_6"


    // $ANTLR start "rule__Entity__FunctionsAssignment_7"
    // InternalOaas.g:3413:1: rule__Entity__FunctionsAssignment_7 : ( ruleFunction ) ;
    public final void rule__Entity__FunctionsAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3417:1: ( ( ruleFunction ) )
            // InternalOaas.g:3418:2: ( ruleFunction )
            {
            // InternalOaas.g:3418:2: ( ruleFunction )
            // InternalOaas.g:3419:3: ruleFunction
            {
             before(grammarAccess.getEntityAccess().getFunctionsFunctionParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleFunction();

            state._fsp--;

             after(grammarAccess.getEntityAccess().getFunctionsFunctionParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__FunctionsAssignment_7"


    // $ANTLR start "rule__Entity__RelationsAssignment_8"
    // InternalOaas.g:3428:1: rule__Entity__RelationsAssignment_8 : ( ruleRelation ) ;
    public final void rule__Entity__RelationsAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3432:1: ( ( ruleRelation ) )
            // InternalOaas.g:3433:2: ( ruleRelation )
            {
            // InternalOaas.g:3433:2: ( ruleRelation )
            // InternalOaas.g:3434:3: ruleRelation
            {
             before(grammarAccess.getEntityAccess().getRelationsRelationParserRuleCall_8_0()); 
            pushFollow(FOLLOW_2);
            ruleRelation();

            state._fsp--;

             after(grammarAccess.getEntityAccess().getRelationsRelationParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__RelationsAssignment_8"


    // $ANTLR start "rule__Attribute__DescriptionAssignment_0"
    // InternalOaas.g:3443:1: rule__Attribute__DescriptionAssignment_0 : ( ruleDescription ) ;
    public final void rule__Attribute__DescriptionAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3447:1: ( ( ruleDescription ) )
            // InternalOaas.g:3448:2: ( ruleDescription )
            {
            // InternalOaas.g:3448:2: ( ruleDescription )
            // InternalOaas.g:3449:3: ruleDescription
            {
             before(grammarAccess.getAttributeAccess().getDescriptionDescriptionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleDescription();

            state._fsp--;

             after(grammarAccess.getAttributeAccess().getDescriptionDescriptionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__DescriptionAssignment_0"


    // $ANTLR start "rule__Attribute__NameAssignment_1"
    // InternalOaas.g:3458:1: rule__Attribute__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Attribute__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3462:1: ( ( RULE_ID ) )
            // InternalOaas.g:3463:2: ( RULE_ID )
            {
            // InternalOaas.g:3463:2: ( RULE_ID )
            // InternalOaas.g:3464:3: RULE_ID
            {
             before(grammarAccess.getAttributeAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAttributeAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__NameAssignment_1"


    // $ANTLR start "rule__Attribute__TypeAssignment_3"
    // InternalOaas.g:3473:1: rule__Attribute__TypeAssignment_3 : ( RULE_ID ) ;
    public final void rule__Attribute__TypeAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3477:1: ( ( RULE_ID ) )
            // InternalOaas.g:3478:2: ( RULE_ID )
            {
            // InternalOaas.g:3478:2: ( RULE_ID )
            // InternalOaas.g:3479:3: RULE_ID
            {
             before(grammarAccess.getAttributeAccess().getTypeIDTerminalRuleCall_3_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAttributeAccess().getTypeIDTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__TypeAssignment_3"


    // $ANTLR start "rule__OneToOne__NameAssignment_0"
    // InternalOaas.g:3488:1: rule__OneToOne__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__OneToOne__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3492:1: ( ( RULE_ID ) )
            // InternalOaas.g:3493:2: ( RULE_ID )
            {
            // InternalOaas.g:3493:2: ( RULE_ID )
            // InternalOaas.g:3494:3: RULE_ID
            {
             before(grammarAccess.getOneToOneAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getOneToOneAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToOne__NameAssignment_0"


    // $ANTLR start "rule__OneToOne__TypeAssignment_2"
    // InternalOaas.g:3503:1: rule__OneToOne__TypeAssignment_2 : ( ( ruleQualifiedName ) ) ;
    public final void rule__OneToOne__TypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3507:1: ( ( ( ruleQualifiedName ) ) )
            // InternalOaas.g:3508:2: ( ( ruleQualifiedName ) )
            {
            // InternalOaas.g:3508:2: ( ( ruleQualifiedName ) )
            // InternalOaas.g:3509:3: ( ruleQualifiedName )
            {
             before(grammarAccess.getOneToOneAccess().getTypeEntityCrossReference_2_0()); 
            // InternalOaas.g:3510:3: ( ruleQualifiedName )
            // InternalOaas.g:3511:4: ruleQualifiedName
            {
             before(grammarAccess.getOneToOneAccess().getTypeEntityQualifiedNameParserRuleCall_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getOneToOneAccess().getTypeEntityQualifiedNameParserRuleCall_2_0_1()); 

            }

             after(grammarAccess.getOneToOneAccess().getTypeEntityCrossReference_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToOne__TypeAssignment_2"


    // $ANTLR start "rule__ManyToMany__NameAssignment_0"
    // InternalOaas.g:3522:1: rule__ManyToMany__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__ManyToMany__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3526:1: ( ( RULE_ID ) )
            // InternalOaas.g:3527:2: ( RULE_ID )
            {
            // InternalOaas.g:3527:2: ( RULE_ID )
            // InternalOaas.g:3528:3: RULE_ID
            {
             before(grammarAccess.getManyToManyAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getManyToManyAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyToMany__NameAssignment_0"


    // $ANTLR start "rule__ManyToMany__TypeAssignment_2"
    // InternalOaas.g:3537:1: rule__ManyToMany__TypeAssignment_2 : ( ( ruleQualifiedName ) ) ;
    public final void rule__ManyToMany__TypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3541:1: ( ( ( ruleQualifiedName ) ) )
            // InternalOaas.g:3542:2: ( ( ruleQualifiedName ) )
            {
            // InternalOaas.g:3542:2: ( ( ruleQualifiedName ) )
            // InternalOaas.g:3543:3: ( ruleQualifiedName )
            {
             before(grammarAccess.getManyToManyAccess().getTypeEntityCrossReference_2_0()); 
            // InternalOaas.g:3544:3: ( ruleQualifiedName )
            // InternalOaas.g:3545:4: ruleQualifiedName
            {
             before(grammarAccess.getManyToManyAccess().getTypeEntityQualifiedNameParserRuleCall_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getManyToManyAccess().getTypeEntityQualifiedNameParserRuleCall_2_0_1()); 

            }

             after(grammarAccess.getManyToManyAccess().getTypeEntityCrossReference_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyToMany__TypeAssignment_2"


    // $ANTLR start "rule__OneToMany__NameAssignment_0"
    // InternalOaas.g:3556:1: rule__OneToMany__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__OneToMany__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3560:1: ( ( RULE_ID ) )
            // InternalOaas.g:3561:2: ( RULE_ID )
            {
            // InternalOaas.g:3561:2: ( RULE_ID )
            // InternalOaas.g:3562:3: RULE_ID
            {
             before(grammarAccess.getOneToManyAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getOneToManyAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToMany__NameAssignment_0"


    // $ANTLR start "rule__OneToMany__TypeAssignment_2"
    // InternalOaas.g:3571:1: rule__OneToMany__TypeAssignment_2 : ( ( ruleQualifiedName ) ) ;
    public final void rule__OneToMany__TypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3575:1: ( ( ( ruleQualifiedName ) ) )
            // InternalOaas.g:3576:2: ( ( ruleQualifiedName ) )
            {
            // InternalOaas.g:3576:2: ( ( ruleQualifiedName ) )
            // InternalOaas.g:3577:3: ( ruleQualifiedName )
            {
             before(grammarAccess.getOneToManyAccess().getTypeEntityCrossReference_2_0()); 
            // InternalOaas.g:3578:3: ( ruleQualifiedName )
            // InternalOaas.g:3579:4: ruleQualifiedName
            {
             before(grammarAccess.getOneToManyAccess().getTypeEntityQualifiedNameParserRuleCall_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getOneToManyAccess().getTypeEntityQualifiedNameParserRuleCall_2_0_1()); 

            }

             after(grammarAccess.getOneToManyAccess().getTypeEntityCrossReference_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToMany__TypeAssignment_2"


    // $ANTLR start "rule__Function__DescriptionAssignment_0"
    // InternalOaas.g:3590:1: rule__Function__DescriptionAssignment_0 : ( ruleDescription ) ;
    public final void rule__Function__DescriptionAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3594:1: ( ( ruleDescription ) )
            // InternalOaas.g:3595:2: ( ruleDescription )
            {
            // InternalOaas.g:3595:2: ( ruleDescription )
            // InternalOaas.g:3596:3: ruleDescription
            {
             before(grammarAccess.getFunctionAccess().getDescriptionDescriptionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleDescription();

            state._fsp--;

             after(grammarAccess.getFunctionAccess().getDescriptionDescriptionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__DescriptionAssignment_0"


    // $ANTLR start "rule__Function__NameAssignment_2"
    // InternalOaas.g:3605:1: rule__Function__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Function__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3609:1: ( ( RULE_ID ) )
            // InternalOaas.g:3610:2: ( RULE_ID )
            {
            // InternalOaas.g:3610:2: ( RULE_ID )
            // InternalOaas.g:3611:3: RULE_ID
            {
             before(grammarAccess.getFunctionAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__NameAssignment_2"


    // $ANTLR start "rule__Function__ParamsAssignment_4_0"
    // InternalOaas.g:3620:1: rule__Function__ParamsAssignment_4_0 : ( RULE_ID ) ;
    public final void rule__Function__ParamsAssignment_4_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3624:1: ( ( RULE_ID ) )
            // InternalOaas.g:3625:2: ( RULE_ID )
            {
            // InternalOaas.g:3625:2: ( RULE_ID )
            // InternalOaas.g:3626:3: RULE_ID
            {
             before(grammarAccess.getFunctionAccess().getParamsIDTerminalRuleCall_4_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getParamsIDTerminalRuleCall_4_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__ParamsAssignment_4_0"


    // $ANTLR start "rule__Function__ParamsAssignment_4_1_1"
    // InternalOaas.g:3635:1: rule__Function__ParamsAssignment_4_1_1 : ( RULE_ID ) ;
    public final void rule__Function__ParamsAssignment_4_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3639:1: ( ( RULE_ID ) )
            // InternalOaas.g:3640:2: ( RULE_ID )
            {
            // InternalOaas.g:3640:2: ( RULE_ID )
            // InternalOaas.g:3641:3: RULE_ID
            {
             before(grammarAccess.getFunctionAccess().getParamsIDTerminalRuleCall_4_1_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getParamsIDTerminalRuleCall_4_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__ParamsAssignment_4_1_1"


    // $ANTLR start "rule__Function__TypeAssignment_7"
    // InternalOaas.g:3650:1: rule__Function__TypeAssignment_7 : ( RULE_ID ) ;
    public final void rule__Function__TypeAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalOaas.g:3654:1: ( ( RULE_ID ) )
            // InternalOaas.g:3655:2: ( RULE_ID )
            {
            // InternalOaas.g:3655:2: ( RULE_ID )
            // InternalOaas.g:3656:3: RULE_ID
            {
             before(grammarAccess.getFunctionAccess().getTypeIDTerminalRuleCall_7_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getTypeIDTerminalRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__TypeAssignment_7"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000015400000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000015400002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000001400000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000015402000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000010400000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000020001000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000400402010L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000000400012L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000400400002L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000000400010L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000400400000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000001000000010L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000002000000002L});

}