package br.ufes.nemo.seon.ontoloyasservice.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import br.ufes.nemo.seon.ontoloyasservice.services.OaasGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalOaasParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Configuration'", "'{'", "'}'", "'ontology_name:'", "'level:'", "'author:'", "'author_email:'", "'repository:'", "'ontology_short_name:'", "'software:'", "'about:'", "'#'", "'is:'", "'module'", "'.'", "'import'", "'.*'", "'entity'", "'extends'", "':'", "'OneToOne'", "'ManyToMany'", "'OneToMany'", "'function'", "'('", "','", "')'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalOaasParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalOaasParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalOaasParser.tokenNames; }
    public String getGrammarFileName() { return "InternalOaas.g"; }



     	private OaasGrammarAccess grammarAccess;

        public InternalOaasParser(TokenStream input, OaasGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Application";
       	}

       	@Override
       	protected OaasGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleApplication"
    // InternalOaas.g:64:1: entryRuleApplication returns [EObject current=null] : iv_ruleApplication= ruleApplication EOF ;
    public final EObject entryRuleApplication() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleApplication = null;


        try {
            // InternalOaas.g:64:52: (iv_ruleApplication= ruleApplication EOF )
            // InternalOaas.g:65:2: iv_ruleApplication= ruleApplication EOF
            {
             newCompositeNode(grammarAccess.getApplicationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleApplication=ruleApplication();

            state._fsp--;

             current =iv_ruleApplication; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleApplication"


    // $ANTLR start "ruleApplication"
    // InternalOaas.g:71:1: ruleApplication returns [EObject current=null] : ( ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_abstractElements_1_0= ruleAbstractElement ) )* ) ;
    public final EObject ruleApplication() throws RecognitionException {
        EObject current = null;

        EObject lv_configuration_0_0 = null;

        EObject lv_abstractElements_1_0 = null;



        	enterRule();

        try {
            // InternalOaas.g:77:2: ( ( ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_abstractElements_1_0= ruleAbstractElement ) )* ) )
            // InternalOaas.g:78:2: ( ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_abstractElements_1_0= ruleAbstractElement ) )* )
            {
            // InternalOaas.g:78:2: ( ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_abstractElements_1_0= ruleAbstractElement ) )* )
            // InternalOaas.g:79:3: ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_abstractElements_1_0= ruleAbstractElement ) )*
            {
            // InternalOaas.g:79:3: ( (lv_configuration_0_0= ruleConfiguration ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==11) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalOaas.g:80:4: (lv_configuration_0_0= ruleConfiguration )
                    {
                    // InternalOaas.g:80:4: (lv_configuration_0_0= ruleConfiguration )
                    // InternalOaas.g:81:5: lv_configuration_0_0= ruleConfiguration
                    {

                    					newCompositeNode(grammarAccess.getApplicationAccess().getConfigurationConfigurationParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_3);
                    lv_configuration_0_0=ruleConfiguration();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getApplicationRule());
                    					}
                    					set(
                    						current,
                    						"configuration",
                    						lv_configuration_0_0,
                    						"br.ufes.nemo.seon.ontoloyasservice.Oaas.Configuration");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalOaas.g:98:3: ( (lv_abstractElements_1_0= ruleAbstractElement ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==22||LA2_0==24||LA2_0==26||LA2_0==28) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalOaas.g:99:4: (lv_abstractElements_1_0= ruleAbstractElement )
            	    {
            	    // InternalOaas.g:99:4: (lv_abstractElements_1_0= ruleAbstractElement )
            	    // InternalOaas.g:100:5: lv_abstractElements_1_0= ruleAbstractElement
            	    {

            	    					newCompositeNode(grammarAccess.getApplicationAccess().getAbstractElementsAbstractElementParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_abstractElements_1_0=ruleAbstractElement();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getApplicationRule());
            	    					}
            	    					add(
            	    						current,
            	    						"abstractElements",
            	    						lv_abstractElements_1_0,
            	    						"br.ufes.nemo.seon.ontoloyasservice.Oaas.AbstractElement");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleApplication"


    // $ANTLR start "entryRuleConfiguration"
    // InternalOaas.g:121:1: entryRuleConfiguration returns [EObject current=null] : iv_ruleConfiguration= ruleConfiguration EOF ;
    public final EObject entryRuleConfiguration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConfiguration = null;


        try {
            // InternalOaas.g:121:54: (iv_ruleConfiguration= ruleConfiguration EOF )
            // InternalOaas.g:122:2: iv_ruleConfiguration= ruleConfiguration EOF
            {
             newCompositeNode(grammarAccess.getConfigurationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConfiguration=ruleConfiguration();

            state._fsp--;

             current =iv_ruleConfiguration; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConfiguration"


    // $ANTLR start "ruleConfiguration"
    // InternalOaas.g:128:1: ruleConfiguration returns [EObject current=null] : (otherlv_0= 'Configuration' otherlv_1= '{' ( (lv_ontology_2_0= ruleOntology ) ) ( (lv_ontologyShortName_3_0= ruleOntologyShortName ) ) ( (lv_level_4_0= ruleLevel ) ) ( (lv_about_5_0= ruleAbout ) ) ( (lv_software_6_0= ruleSoftware ) ) ( (lv_author_7_0= ruleAuthor ) ) ( (lv_author_email_8_0= ruleAuthor_Email ) ) ( (lv_repository_9_0= ruleRepository ) ) otherlv_10= '}' ) ;
    public final EObject ruleConfiguration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_10=null;
        EObject lv_ontology_2_0 = null;

        EObject lv_ontologyShortName_3_0 = null;

        EObject lv_level_4_0 = null;

        EObject lv_about_5_0 = null;

        EObject lv_software_6_0 = null;

        EObject lv_author_7_0 = null;

        EObject lv_author_email_8_0 = null;

        EObject lv_repository_9_0 = null;



        	enterRule();

        try {
            // InternalOaas.g:134:2: ( (otherlv_0= 'Configuration' otherlv_1= '{' ( (lv_ontology_2_0= ruleOntology ) ) ( (lv_ontologyShortName_3_0= ruleOntologyShortName ) ) ( (lv_level_4_0= ruleLevel ) ) ( (lv_about_5_0= ruleAbout ) ) ( (lv_software_6_0= ruleSoftware ) ) ( (lv_author_7_0= ruleAuthor ) ) ( (lv_author_email_8_0= ruleAuthor_Email ) ) ( (lv_repository_9_0= ruleRepository ) ) otherlv_10= '}' ) )
            // InternalOaas.g:135:2: (otherlv_0= 'Configuration' otherlv_1= '{' ( (lv_ontology_2_0= ruleOntology ) ) ( (lv_ontologyShortName_3_0= ruleOntologyShortName ) ) ( (lv_level_4_0= ruleLevel ) ) ( (lv_about_5_0= ruleAbout ) ) ( (lv_software_6_0= ruleSoftware ) ) ( (lv_author_7_0= ruleAuthor ) ) ( (lv_author_email_8_0= ruleAuthor_Email ) ) ( (lv_repository_9_0= ruleRepository ) ) otherlv_10= '}' )
            {
            // InternalOaas.g:135:2: (otherlv_0= 'Configuration' otherlv_1= '{' ( (lv_ontology_2_0= ruleOntology ) ) ( (lv_ontologyShortName_3_0= ruleOntologyShortName ) ) ( (lv_level_4_0= ruleLevel ) ) ( (lv_about_5_0= ruleAbout ) ) ( (lv_software_6_0= ruleSoftware ) ) ( (lv_author_7_0= ruleAuthor ) ) ( (lv_author_email_8_0= ruleAuthor_Email ) ) ( (lv_repository_9_0= ruleRepository ) ) otherlv_10= '}' )
            // InternalOaas.g:136:3: otherlv_0= 'Configuration' otherlv_1= '{' ( (lv_ontology_2_0= ruleOntology ) ) ( (lv_ontologyShortName_3_0= ruleOntologyShortName ) ) ( (lv_level_4_0= ruleLevel ) ) ( (lv_about_5_0= ruleAbout ) ) ( (lv_software_6_0= ruleSoftware ) ) ( (lv_author_7_0= ruleAuthor ) ) ( (lv_author_email_8_0= ruleAuthor_Email ) ) ( (lv_repository_9_0= ruleRepository ) ) otherlv_10= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getConfigurationAccess().getConfigurationKeyword_0());
            		
            otherlv_1=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalOaas.g:144:3: ( (lv_ontology_2_0= ruleOntology ) )
            // InternalOaas.g:145:4: (lv_ontology_2_0= ruleOntology )
            {
            // InternalOaas.g:145:4: (lv_ontology_2_0= ruleOntology )
            // InternalOaas.g:146:5: lv_ontology_2_0= ruleOntology
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getOntologyOntologyParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_6);
            lv_ontology_2_0=ruleOntology();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"ontology",
            						lv_ontology_2_0,
            						"br.ufes.nemo.seon.ontoloyasservice.Oaas.Ontology");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalOaas.g:163:3: ( (lv_ontologyShortName_3_0= ruleOntologyShortName ) )
            // InternalOaas.g:164:4: (lv_ontologyShortName_3_0= ruleOntologyShortName )
            {
            // InternalOaas.g:164:4: (lv_ontologyShortName_3_0= ruleOntologyShortName )
            // InternalOaas.g:165:5: lv_ontologyShortName_3_0= ruleOntologyShortName
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getOntologyShortNameOntologyShortNameParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_7);
            lv_ontologyShortName_3_0=ruleOntologyShortName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"ontologyShortName",
            						lv_ontologyShortName_3_0,
            						"br.ufes.nemo.seon.ontoloyasservice.Oaas.OntologyShortName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalOaas.g:182:3: ( (lv_level_4_0= ruleLevel ) )
            // InternalOaas.g:183:4: (lv_level_4_0= ruleLevel )
            {
            // InternalOaas.g:183:4: (lv_level_4_0= ruleLevel )
            // InternalOaas.g:184:5: lv_level_4_0= ruleLevel
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getLevelLevelParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_8);
            lv_level_4_0=ruleLevel();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"level",
            						lv_level_4_0,
            						"br.ufes.nemo.seon.ontoloyasservice.Oaas.Level");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalOaas.g:201:3: ( (lv_about_5_0= ruleAbout ) )
            // InternalOaas.g:202:4: (lv_about_5_0= ruleAbout )
            {
            // InternalOaas.g:202:4: (lv_about_5_0= ruleAbout )
            // InternalOaas.g:203:5: lv_about_5_0= ruleAbout
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getAboutAboutParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_9);
            lv_about_5_0=ruleAbout();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"about",
            						lv_about_5_0,
            						"br.ufes.nemo.seon.ontoloyasservice.Oaas.About");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalOaas.g:220:3: ( (lv_software_6_0= ruleSoftware ) )
            // InternalOaas.g:221:4: (lv_software_6_0= ruleSoftware )
            {
            // InternalOaas.g:221:4: (lv_software_6_0= ruleSoftware )
            // InternalOaas.g:222:5: lv_software_6_0= ruleSoftware
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getSoftwareSoftwareParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_10);
            lv_software_6_0=ruleSoftware();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"software",
            						lv_software_6_0,
            						"br.ufes.nemo.seon.ontoloyasservice.Oaas.Software");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalOaas.g:239:3: ( (lv_author_7_0= ruleAuthor ) )
            // InternalOaas.g:240:4: (lv_author_7_0= ruleAuthor )
            {
            // InternalOaas.g:240:4: (lv_author_7_0= ruleAuthor )
            // InternalOaas.g:241:5: lv_author_7_0= ruleAuthor
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getAuthorAuthorParserRuleCall_7_0());
            				
            pushFollow(FOLLOW_11);
            lv_author_7_0=ruleAuthor();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"author",
            						lv_author_7_0,
            						"br.ufes.nemo.seon.ontoloyasservice.Oaas.Author");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalOaas.g:258:3: ( (lv_author_email_8_0= ruleAuthor_Email ) )
            // InternalOaas.g:259:4: (lv_author_email_8_0= ruleAuthor_Email )
            {
            // InternalOaas.g:259:4: (lv_author_email_8_0= ruleAuthor_Email )
            // InternalOaas.g:260:5: lv_author_email_8_0= ruleAuthor_Email
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getAuthor_emailAuthor_EmailParserRuleCall_8_0());
            				
            pushFollow(FOLLOW_12);
            lv_author_email_8_0=ruleAuthor_Email();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"author_email",
            						lv_author_email_8_0,
            						"br.ufes.nemo.seon.ontoloyasservice.Oaas.Author_Email");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalOaas.g:277:3: ( (lv_repository_9_0= ruleRepository ) )
            // InternalOaas.g:278:4: (lv_repository_9_0= ruleRepository )
            {
            // InternalOaas.g:278:4: (lv_repository_9_0= ruleRepository )
            // InternalOaas.g:279:5: lv_repository_9_0= ruleRepository
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getRepositoryRepositoryParserRuleCall_9_0());
            				
            pushFollow(FOLLOW_13);
            lv_repository_9_0=ruleRepository();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"repository",
            						lv_repository_9_0,
            						"br.ufes.nemo.seon.ontoloyasservice.Oaas.Repository");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_10=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_10());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConfiguration"


    // $ANTLR start "entryRuleOntology"
    // InternalOaas.g:304:1: entryRuleOntology returns [EObject current=null] : iv_ruleOntology= ruleOntology EOF ;
    public final EObject entryRuleOntology() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOntology = null;


        try {
            // InternalOaas.g:304:49: (iv_ruleOntology= ruleOntology EOF )
            // InternalOaas.g:305:2: iv_ruleOntology= ruleOntology EOF
            {
             newCompositeNode(grammarAccess.getOntologyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOntology=ruleOntology();

            state._fsp--;

             current =iv_ruleOntology; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOntology"


    // $ANTLR start "ruleOntology"
    // InternalOaas.g:311:1: ruleOntology returns [EObject current=null] : (otherlv_0= 'ontology_name:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleOntology() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalOaas.g:317:2: ( (otherlv_0= 'ontology_name:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalOaas.g:318:2: (otherlv_0= 'ontology_name:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalOaas.g:318:2: (otherlv_0= 'ontology_name:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalOaas.g:319:3: otherlv_0= 'ontology_name:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,14,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getOntologyAccess().getOntology_nameKeyword_0());
            		
            // InternalOaas.g:323:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalOaas.g:324:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalOaas.g:324:4: (lv_name_1_0= RULE_STRING )
            // InternalOaas.g:325:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getOntologyAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOntologyRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOntology"


    // $ANTLR start "entryRuleLevel"
    // InternalOaas.g:345:1: entryRuleLevel returns [EObject current=null] : iv_ruleLevel= ruleLevel EOF ;
    public final EObject entryRuleLevel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLevel = null;


        try {
            // InternalOaas.g:345:46: (iv_ruleLevel= ruleLevel EOF )
            // InternalOaas.g:346:2: iv_ruleLevel= ruleLevel EOF
            {
             newCompositeNode(grammarAccess.getLevelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLevel=ruleLevel();

            state._fsp--;

             current =iv_ruleLevel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLevel"


    // $ANTLR start "ruleLevel"
    // InternalOaas.g:352:1: ruleLevel returns [EObject current=null] : (otherlv_0= 'level:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleLevel() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalOaas.g:358:2: ( (otherlv_0= 'level:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalOaas.g:359:2: (otherlv_0= 'level:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalOaas.g:359:2: (otherlv_0= 'level:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalOaas.g:360:3: otherlv_0= 'level:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,15,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getLevelAccess().getLevelKeyword_0());
            		
            // InternalOaas.g:364:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalOaas.g:365:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalOaas.g:365:4: (lv_name_1_0= RULE_STRING )
            // InternalOaas.g:366:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getLevelAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getLevelRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLevel"


    // $ANTLR start "entryRuleAuthor"
    // InternalOaas.g:386:1: entryRuleAuthor returns [EObject current=null] : iv_ruleAuthor= ruleAuthor EOF ;
    public final EObject entryRuleAuthor() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAuthor = null;


        try {
            // InternalOaas.g:386:47: (iv_ruleAuthor= ruleAuthor EOF )
            // InternalOaas.g:387:2: iv_ruleAuthor= ruleAuthor EOF
            {
             newCompositeNode(grammarAccess.getAuthorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAuthor=ruleAuthor();

            state._fsp--;

             current =iv_ruleAuthor; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAuthor"


    // $ANTLR start "ruleAuthor"
    // InternalOaas.g:393:1: ruleAuthor returns [EObject current=null] : (otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleAuthor() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalOaas.g:399:2: ( (otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalOaas.g:400:2: (otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalOaas.g:400:2: (otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalOaas.g:401:3: otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,16,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getAuthorAccess().getAuthorKeyword_0());
            		
            // InternalOaas.g:405:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalOaas.g:406:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalOaas.g:406:4: (lv_name_1_0= RULE_STRING )
            // InternalOaas.g:407:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAuthorAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAuthorRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAuthor"


    // $ANTLR start "entryRuleAuthor_Email"
    // InternalOaas.g:427:1: entryRuleAuthor_Email returns [EObject current=null] : iv_ruleAuthor_Email= ruleAuthor_Email EOF ;
    public final EObject entryRuleAuthor_Email() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAuthor_Email = null;


        try {
            // InternalOaas.g:427:53: (iv_ruleAuthor_Email= ruleAuthor_Email EOF )
            // InternalOaas.g:428:2: iv_ruleAuthor_Email= ruleAuthor_Email EOF
            {
             newCompositeNode(grammarAccess.getAuthor_EmailRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAuthor_Email=ruleAuthor_Email();

            state._fsp--;

             current =iv_ruleAuthor_Email; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAuthor_Email"


    // $ANTLR start "ruleAuthor_Email"
    // InternalOaas.g:434:1: ruleAuthor_Email returns [EObject current=null] : (otherlv_0= 'author_email:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleAuthor_Email() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalOaas.g:440:2: ( (otherlv_0= 'author_email:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalOaas.g:441:2: (otherlv_0= 'author_email:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalOaas.g:441:2: (otherlv_0= 'author_email:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalOaas.g:442:3: otherlv_0= 'author_email:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,17,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getAuthor_EmailAccess().getAuthor_emailKeyword_0());
            		
            // InternalOaas.g:446:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalOaas.g:447:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalOaas.g:447:4: (lv_name_1_0= RULE_STRING )
            // InternalOaas.g:448:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAuthor_EmailAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAuthor_EmailRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAuthor_Email"


    // $ANTLR start "entryRuleRepository"
    // InternalOaas.g:468:1: entryRuleRepository returns [EObject current=null] : iv_ruleRepository= ruleRepository EOF ;
    public final EObject entryRuleRepository() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRepository = null;


        try {
            // InternalOaas.g:468:51: (iv_ruleRepository= ruleRepository EOF )
            // InternalOaas.g:469:2: iv_ruleRepository= ruleRepository EOF
            {
             newCompositeNode(grammarAccess.getRepositoryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRepository=ruleRepository();

            state._fsp--;

             current =iv_ruleRepository; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRepository"


    // $ANTLR start "ruleRepository"
    // InternalOaas.g:475:1: ruleRepository returns [EObject current=null] : (otherlv_0= 'repository:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleRepository() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalOaas.g:481:2: ( (otherlv_0= 'repository:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalOaas.g:482:2: (otherlv_0= 'repository:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalOaas.g:482:2: (otherlv_0= 'repository:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalOaas.g:483:3: otherlv_0= 'repository:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,18,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getRepositoryAccess().getRepositoryKeyword_0());
            		
            // InternalOaas.g:487:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalOaas.g:488:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalOaas.g:488:4: (lv_name_1_0= RULE_STRING )
            // InternalOaas.g:489:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getRepositoryAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getRepositoryRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRepository"


    // $ANTLR start "entryRuleOntologyShortName"
    // InternalOaas.g:509:1: entryRuleOntologyShortName returns [EObject current=null] : iv_ruleOntologyShortName= ruleOntologyShortName EOF ;
    public final EObject entryRuleOntologyShortName() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOntologyShortName = null;


        try {
            // InternalOaas.g:509:58: (iv_ruleOntologyShortName= ruleOntologyShortName EOF )
            // InternalOaas.g:510:2: iv_ruleOntologyShortName= ruleOntologyShortName EOF
            {
             newCompositeNode(grammarAccess.getOntologyShortNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOntologyShortName=ruleOntologyShortName();

            state._fsp--;

             current =iv_ruleOntologyShortName; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOntologyShortName"


    // $ANTLR start "ruleOntologyShortName"
    // InternalOaas.g:516:1: ruleOntologyShortName returns [EObject current=null] : (otherlv_0= 'ontology_short_name:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleOntologyShortName() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalOaas.g:522:2: ( (otherlv_0= 'ontology_short_name:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalOaas.g:523:2: (otherlv_0= 'ontology_short_name:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalOaas.g:523:2: (otherlv_0= 'ontology_short_name:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalOaas.g:524:3: otherlv_0= 'ontology_short_name:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,19,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getOntologyShortNameAccess().getOntology_short_nameKeyword_0());
            		
            // InternalOaas.g:528:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalOaas.g:529:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalOaas.g:529:4: (lv_name_1_0= RULE_STRING )
            // InternalOaas.g:530:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getOntologyShortNameAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOntologyShortNameRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOntologyShortName"


    // $ANTLR start "entryRuleSoftware"
    // InternalOaas.g:550:1: entryRuleSoftware returns [EObject current=null] : iv_ruleSoftware= ruleSoftware EOF ;
    public final EObject entryRuleSoftware() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSoftware = null;


        try {
            // InternalOaas.g:550:49: (iv_ruleSoftware= ruleSoftware EOF )
            // InternalOaas.g:551:2: iv_ruleSoftware= ruleSoftware EOF
            {
             newCompositeNode(grammarAccess.getSoftwareRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSoftware=ruleSoftware();

            state._fsp--;

             current =iv_ruleSoftware; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSoftware"


    // $ANTLR start "ruleSoftware"
    // InternalOaas.g:557:1: ruleSoftware returns [EObject current=null] : (otherlv_0= 'software:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleSoftware() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalOaas.g:563:2: ( (otherlv_0= 'software:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalOaas.g:564:2: (otherlv_0= 'software:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalOaas.g:564:2: (otherlv_0= 'software:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalOaas.g:565:3: otherlv_0= 'software:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,20,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getSoftwareAccess().getSoftwareKeyword_0());
            		
            // InternalOaas.g:569:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalOaas.g:570:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalOaas.g:570:4: (lv_name_1_0= RULE_STRING )
            // InternalOaas.g:571:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getSoftwareAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSoftwareRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSoftware"


    // $ANTLR start "entryRuleAbout"
    // InternalOaas.g:591:1: entryRuleAbout returns [EObject current=null] : iv_ruleAbout= ruleAbout EOF ;
    public final EObject entryRuleAbout() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbout = null;


        try {
            // InternalOaas.g:591:46: (iv_ruleAbout= ruleAbout EOF )
            // InternalOaas.g:592:2: iv_ruleAbout= ruleAbout EOF
            {
             newCompositeNode(grammarAccess.getAboutRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAbout=ruleAbout();

            state._fsp--;

             current =iv_ruleAbout; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbout"


    // $ANTLR start "ruleAbout"
    // InternalOaas.g:598:1: ruleAbout returns [EObject current=null] : (otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleAbout() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalOaas.g:604:2: ( (otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalOaas.g:605:2: (otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalOaas.g:605:2: (otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalOaas.g:606:3: otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,21,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getAboutAccess().getAboutKeyword_0());
            		
            // InternalOaas.g:610:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalOaas.g:611:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalOaas.g:611:4: (lv_name_1_0= RULE_STRING )
            // InternalOaas.g:612:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAboutAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAboutRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbout"


    // $ANTLR start "entryRuleDescription"
    // InternalOaas.g:632:1: entryRuleDescription returns [EObject current=null] : iv_ruleDescription= ruleDescription EOF ;
    public final EObject entryRuleDescription() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDescription = null;


        try {
            // InternalOaas.g:632:52: (iv_ruleDescription= ruleDescription EOF )
            // InternalOaas.g:633:2: iv_ruleDescription= ruleDescription EOF
            {
             newCompositeNode(grammarAccess.getDescriptionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDescription=ruleDescription();

            state._fsp--;

             current =iv_ruleDescription; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDescription"


    // $ANTLR start "ruleDescription"
    // InternalOaas.g:639:1: ruleDescription returns [EObject current=null] : (otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleDescription() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_textfield_1_0=null;


        	enterRule();

        try {
            // InternalOaas.g:645:2: ( (otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) ) ) )
            // InternalOaas.g:646:2: (otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) ) )
            {
            // InternalOaas.g:646:2: (otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) ) )
            // InternalOaas.g:647:3: otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,22,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getDescriptionAccess().getNumberSignKeyword_0());
            		
            // InternalOaas.g:651:3: ( (lv_textfield_1_0= RULE_STRING ) )
            // InternalOaas.g:652:4: (lv_textfield_1_0= RULE_STRING )
            {
            // InternalOaas.g:652:4: (lv_textfield_1_0= RULE_STRING )
            // InternalOaas.g:653:5: lv_textfield_1_0= RULE_STRING
            {
            lv_textfield_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_textfield_1_0, grammarAccess.getDescriptionAccess().getTextfieldSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDescriptionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"textfield",
            						lv_textfield_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDescription"


    // $ANTLR start "entryRuleEntity_Type"
    // InternalOaas.g:673:1: entryRuleEntity_Type returns [EObject current=null] : iv_ruleEntity_Type= ruleEntity_Type EOF ;
    public final EObject entryRuleEntity_Type() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEntity_Type = null;


        try {
            // InternalOaas.g:673:52: (iv_ruleEntity_Type= ruleEntity_Type EOF )
            // InternalOaas.g:674:2: iv_ruleEntity_Type= ruleEntity_Type EOF
            {
             newCompositeNode(grammarAccess.getEntity_TypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEntity_Type=ruleEntity_Type();

            state._fsp--;

             current =iv_ruleEntity_Type; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEntity_Type"


    // $ANTLR start "ruleEntity_Type"
    // InternalOaas.g:680:1: ruleEntity_Type returns [EObject current=null] : (otherlv_0= 'is:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleEntity_Type() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalOaas.g:686:2: ( (otherlv_0= 'is:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalOaas.g:687:2: (otherlv_0= 'is:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalOaas.g:687:2: (otherlv_0= 'is:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalOaas.g:688:3: otherlv_0= 'is:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,23,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getEntity_TypeAccess().getIsKeyword_0());
            		
            // InternalOaas.g:692:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalOaas.g:693:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalOaas.g:693:4: (lv_name_1_0= RULE_STRING )
            // InternalOaas.g:694:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getEntity_TypeAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEntity_TypeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEntity_Type"


    // $ANTLR start "entryRuleModule"
    // InternalOaas.g:714:1: entryRuleModule returns [EObject current=null] : iv_ruleModule= ruleModule EOF ;
    public final EObject entryRuleModule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModule = null;


        try {
            // InternalOaas.g:714:47: (iv_ruleModule= ruleModule EOF )
            // InternalOaas.g:715:2: iv_ruleModule= ruleModule EOF
            {
             newCompositeNode(grammarAccess.getModuleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModule=ruleModule();

            state._fsp--;

             current =iv_ruleModule; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModule"


    // $ANTLR start "ruleModule"
    // InternalOaas.g:721:1: ruleModule returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'module' ( (lv_name_2_0= ruleQualifiedName ) ) otherlv_3= '{' ( (lv_elements_4_0= ruleAbstractElement ) )* otherlv_5= '}' ) ;
    public final EObject ruleModule() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_description_0_0 = null;

        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_elements_4_0 = null;



        	enterRule();

        try {
            // InternalOaas.g:727:2: ( ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'module' ( (lv_name_2_0= ruleQualifiedName ) ) otherlv_3= '{' ( (lv_elements_4_0= ruleAbstractElement ) )* otherlv_5= '}' ) )
            // InternalOaas.g:728:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'module' ( (lv_name_2_0= ruleQualifiedName ) ) otherlv_3= '{' ( (lv_elements_4_0= ruleAbstractElement ) )* otherlv_5= '}' )
            {
            // InternalOaas.g:728:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'module' ( (lv_name_2_0= ruleQualifiedName ) ) otherlv_3= '{' ( (lv_elements_4_0= ruleAbstractElement ) )* otherlv_5= '}' )
            // InternalOaas.g:729:3: ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'module' ( (lv_name_2_0= ruleQualifiedName ) ) otherlv_3= '{' ( (lv_elements_4_0= ruleAbstractElement ) )* otherlv_5= '}'
            {
            // InternalOaas.g:729:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==22) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalOaas.g:730:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalOaas.g:730:4: (lv_description_0_0= ruleDescription )
                    // InternalOaas.g:731:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getModuleAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_15);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getModuleRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.nemo.seon.ontoloyasservice.Oaas.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,24,FOLLOW_16); 

            			newLeafNode(otherlv_1, grammarAccess.getModuleAccess().getModuleKeyword_1());
            		
            // InternalOaas.g:752:3: ( (lv_name_2_0= ruleQualifiedName ) )
            // InternalOaas.g:753:4: (lv_name_2_0= ruleQualifiedName )
            {
            // InternalOaas.g:753:4: (lv_name_2_0= ruleQualifiedName )
            // InternalOaas.g:754:5: lv_name_2_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getModuleAccess().getNameQualifiedNameParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getModuleRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"br.ufes.nemo.seon.ontoloyasservice.Oaas.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_17); 

            			newLeafNode(otherlv_3, grammarAccess.getModuleAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalOaas.g:775:3: ( (lv_elements_4_0= ruleAbstractElement ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==22||LA4_0==24||LA4_0==26||LA4_0==28) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalOaas.g:776:4: (lv_elements_4_0= ruleAbstractElement )
            	    {
            	    // InternalOaas.g:776:4: (lv_elements_4_0= ruleAbstractElement )
            	    // InternalOaas.g:777:5: lv_elements_4_0= ruleAbstractElement
            	    {

            	    					newCompositeNode(grammarAccess.getModuleAccess().getElementsAbstractElementParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_17);
            	    lv_elements_4_0=ruleAbstractElement();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getModuleRule());
            	    					}
            	    					add(
            	    						current,
            	    						"elements",
            	    						lv_elements_4_0,
            	    						"br.ufes.nemo.seon.ontoloyasservice.Oaas.AbstractElement");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_5=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getModuleAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModule"


    // $ANTLR start "entryRuleAbstractElement"
    // InternalOaas.g:802:1: entryRuleAbstractElement returns [EObject current=null] : iv_ruleAbstractElement= ruleAbstractElement EOF ;
    public final EObject entryRuleAbstractElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractElement = null;


        try {
            // InternalOaas.g:802:56: (iv_ruleAbstractElement= ruleAbstractElement EOF )
            // InternalOaas.g:803:2: iv_ruleAbstractElement= ruleAbstractElement EOF
            {
             newCompositeNode(grammarAccess.getAbstractElementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAbstractElement=ruleAbstractElement();

            state._fsp--;

             current =iv_ruleAbstractElement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractElement"


    // $ANTLR start "ruleAbstractElement"
    // InternalOaas.g:809:1: ruleAbstractElement returns [EObject current=null] : (this_Module_0= ruleModule | this_Entity_1= ruleEntity | this_Import_2= ruleImport ) ;
    public final EObject ruleAbstractElement() throws RecognitionException {
        EObject current = null;

        EObject this_Module_0 = null;

        EObject this_Entity_1 = null;

        EObject this_Import_2 = null;



        	enterRule();

        try {
            // InternalOaas.g:815:2: ( (this_Module_0= ruleModule | this_Entity_1= ruleEntity | this_Import_2= ruleImport ) )
            // InternalOaas.g:816:2: (this_Module_0= ruleModule | this_Entity_1= ruleEntity | this_Import_2= ruleImport )
            {
            // InternalOaas.g:816:2: (this_Module_0= ruleModule | this_Entity_1= ruleEntity | this_Import_2= ruleImport )
            int alt5=3;
            switch ( input.LA(1) ) {
            case 22:
                {
                int LA5_1 = input.LA(2);

                if ( (LA5_1==RULE_STRING) ) {
                    int LA5_5 = input.LA(3);

                    if ( (LA5_5==28) ) {
                        alt5=2;
                    }
                    else if ( (LA5_5==24) ) {
                        alt5=1;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 5, 5, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 5, 1, input);

                    throw nvae;
                }
                }
                break;
            case 24:
                {
                alt5=1;
                }
                break;
            case 28:
                {
                alt5=2;
                }
                break;
            case 26:
                {
                alt5=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalOaas.g:817:3: this_Module_0= ruleModule
                    {

                    			newCompositeNode(grammarAccess.getAbstractElementAccess().getModuleParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Module_0=ruleModule();

                    state._fsp--;


                    			current = this_Module_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalOaas.g:826:3: this_Entity_1= ruleEntity
                    {

                    			newCompositeNode(grammarAccess.getAbstractElementAccess().getEntityParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Entity_1=ruleEntity();

                    state._fsp--;


                    			current = this_Entity_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalOaas.g:835:3: this_Import_2= ruleImport
                    {

                    			newCompositeNode(grammarAccess.getAbstractElementAccess().getImportParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Import_2=ruleImport();

                    state._fsp--;


                    			current = this_Import_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractElement"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalOaas.g:847:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalOaas.g:847:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalOaas.g:848:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalOaas.g:854:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalOaas.g:860:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalOaas.g:861:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalOaas.g:861:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalOaas.g:862:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_18); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0());
            		
            // InternalOaas.g:869:3: (kw= '.' this_ID_2= RULE_ID )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==25) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalOaas.g:870:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,25,FOLLOW_16); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_18); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleImport"
    // InternalOaas.g:887:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalOaas.g:887:47: (iv_ruleImport= ruleImport EOF )
            // InternalOaas.g:888:2: iv_ruleImport= ruleImport EOF
            {
             newCompositeNode(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;

             current =iv_ruleImport; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalOaas.g:894:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_importedNamespace_1_0 = null;



        	enterRule();

        try {
            // InternalOaas.g:900:2: ( (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) ) )
            // InternalOaas.g:901:2: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) )
            {
            // InternalOaas.g:901:2: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) )
            // InternalOaas.g:902:3: otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) )
            {
            otherlv_0=(Token)match(input,26,FOLLOW_16); 

            			newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
            		
            // InternalOaas.g:906:3: ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) )
            // InternalOaas.g:907:4: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
            {
            // InternalOaas.g:907:4: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
            // InternalOaas.g:908:5: lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard
            {

            					newCompositeNode(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_importedNamespace_1_0=ruleQualifiedNameWithWildcard();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getImportRule());
            					}
            					set(
            						current,
            						"importedNamespace",
            						lv_importedNamespace_1_0,
            						"br.ufes.nemo.seon.ontoloyasservice.Oaas.QualifiedNameWithWildcard");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // InternalOaas.g:929:1: entryRuleQualifiedNameWithWildcard returns [String current=null] : iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF ;
    public final String entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedNameWithWildcard = null;


        try {
            // InternalOaas.g:929:65: (iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF )
            // InternalOaas.g:930:2: iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameWithWildcardRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedNameWithWildcard=ruleQualifiedNameWithWildcard();

            state._fsp--;

             current =iv_ruleQualifiedNameWithWildcard.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // InternalOaas.g:936:1: ruleQualifiedNameWithWildcard returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedNameWithWildcard() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_QualifiedName_0 = null;



        	enterRule();

        try {
            // InternalOaas.g:942:2: ( (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) )
            // InternalOaas.g:943:2: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            {
            // InternalOaas.g:943:2: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            // InternalOaas.g:944:3: this_QualifiedName_0= ruleQualifiedName (kw= '.*' )?
            {

            			newCompositeNode(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0());
            		
            pushFollow(FOLLOW_19);
            this_QualifiedName_0=ruleQualifiedName();

            state._fsp--;


            			current.merge(this_QualifiedName_0);
            		

            			afterParserOrEnumRuleCall();
            		
            // InternalOaas.g:954:3: (kw= '.*' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==27) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalOaas.g:955:4: kw= '.*'
                    {
                    kw=(Token)match(input,27,FOLLOW_2); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleEntity"
    // InternalOaas.g:965:1: entryRuleEntity returns [EObject current=null] : iv_ruleEntity= ruleEntity EOF ;
    public final EObject entryRuleEntity() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEntity = null;


        try {
            // InternalOaas.g:965:47: (iv_ruleEntity= ruleEntity EOF )
            // InternalOaas.g:966:2: iv_ruleEntity= ruleEntity EOF
            {
             newCompositeNode(grammarAccess.getEntityRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEntity=ruleEntity();

            state._fsp--;

             current =iv_ruleEntity; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEntity"


    // $ANTLR start "ruleEntity"
    // InternalOaas.g:972:1: ruleEntity returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'entity' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )? otherlv_5= '{' ( (lv_entity_type_6_0= ruleEntity_Type ) ) ( (lv_attributes_7_0= ruleAttribute ) )* ( (lv_functions_8_0= ruleFunction ) )* ( (lv_relations_9_0= ruleRelation ) )* otherlv_10= '}' ) ;
    public final EObject ruleEntity() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_10=null;
        EObject lv_description_0_0 = null;

        EObject lv_entity_type_6_0 = null;

        EObject lv_attributes_7_0 = null;

        EObject lv_functions_8_0 = null;

        EObject lv_relations_9_0 = null;



        	enterRule();

        try {
            // InternalOaas.g:978:2: ( ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'entity' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )? otherlv_5= '{' ( (lv_entity_type_6_0= ruleEntity_Type ) ) ( (lv_attributes_7_0= ruleAttribute ) )* ( (lv_functions_8_0= ruleFunction ) )* ( (lv_relations_9_0= ruleRelation ) )* otherlv_10= '}' ) )
            // InternalOaas.g:979:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'entity' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )? otherlv_5= '{' ( (lv_entity_type_6_0= ruleEntity_Type ) ) ( (lv_attributes_7_0= ruleAttribute ) )* ( (lv_functions_8_0= ruleFunction ) )* ( (lv_relations_9_0= ruleRelation ) )* otherlv_10= '}' )
            {
            // InternalOaas.g:979:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'entity' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )? otherlv_5= '{' ( (lv_entity_type_6_0= ruleEntity_Type ) ) ( (lv_attributes_7_0= ruleAttribute ) )* ( (lv_functions_8_0= ruleFunction ) )* ( (lv_relations_9_0= ruleRelation ) )* otherlv_10= '}' )
            // InternalOaas.g:980:3: ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'entity' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )? otherlv_5= '{' ( (lv_entity_type_6_0= ruleEntity_Type ) ) ( (lv_attributes_7_0= ruleAttribute ) )* ( (lv_functions_8_0= ruleFunction ) )* ( (lv_relations_9_0= ruleRelation ) )* otherlv_10= '}'
            {
            // InternalOaas.g:980:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==22) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalOaas.g:981:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalOaas.g:981:4: (lv_description_0_0= ruleDescription )
                    // InternalOaas.g:982:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getEntityAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_20);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getEntityRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.nemo.seon.ontoloyasservice.Oaas.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,28,FOLLOW_16); 

            			newLeafNode(otherlv_1, grammarAccess.getEntityAccess().getEntityKeyword_1());
            		
            // InternalOaas.g:1003:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalOaas.g:1004:4: (lv_name_2_0= RULE_ID )
            {
            // InternalOaas.g:1004:4: (lv_name_2_0= RULE_ID )
            // InternalOaas.g:1005:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_21); 

            					newLeafNode(lv_name_2_0, grammarAccess.getEntityAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEntityRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalOaas.g:1021:3: (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==29) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalOaas.g:1022:4: otherlv_3= 'extends' ( ( ruleQualifiedName ) )
                    {
                    otherlv_3=(Token)match(input,29,FOLLOW_16); 

                    				newLeafNode(otherlv_3, grammarAccess.getEntityAccess().getExtendsKeyword_3_0());
                    			
                    // InternalOaas.g:1026:4: ( ( ruleQualifiedName ) )
                    // InternalOaas.g:1027:5: ( ruleQualifiedName )
                    {
                    // InternalOaas.g:1027:5: ( ruleQualifiedName )
                    // InternalOaas.g:1028:6: ruleQualifiedName
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getEntityRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getEntityAccess().getSuperTypeEntityCrossReference_3_1_0());
                    					
                    pushFollow(FOLLOW_4);
                    ruleQualifiedName();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,12,FOLLOW_22); 

            			newLeafNode(otherlv_5, grammarAccess.getEntityAccess().getLeftCurlyBracketKeyword_4());
            		
            // InternalOaas.g:1047:3: ( (lv_entity_type_6_0= ruleEntity_Type ) )
            // InternalOaas.g:1048:4: (lv_entity_type_6_0= ruleEntity_Type )
            {
            // InternalOaas.g:1048:4: (lv_entity_type_6_0= ruleEntity_Type )
            // InternalOaas.g:1049:5: lv_entity_type_6_0= ruleEntity_Type
            {

            					newCompositeNode(grammarAccess.getEntityAccess().getEntity_typeEntity_TypeParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_23);
            lv_entity_type_6_0=ruleEntity_Type();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEntityRule());
            					}
            					set(
            						current,
            						"entity_type",
            						lv_entity_type_6_0,
            						"br.ufes.nemo.seon.ontoloyasservice.Oaas.Entity_Type");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalOaas.g:1066:3: ( (lv_attributes_7_0= ruleAttribute ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==22) ) {
                    int LA10_1 = input.LA(2);

                    if ( (LA10_1==RULE_STRING) ) {
                        int LA10_4 = input.LA(3);

                        if ( (LA10_4==RULE_ID) ) {
                            alt10=1;
                        }


                    }


                }
                else if ( (LA10_0==RULE_ID) ) {
                    int LA10_3 = input.LA(2);

                    if ( (LA10_3==30) ) {
                        alt10=1;
                    }


                }


                switch (alt10) {
            	case 1 :
            	    // InternalOaas.g:1067:4: (lv_attributes_7_0= ruleAttribute )
            	    {
            	    // InternalOaas.g:1067:4: (lv_attributes_7_0= ruleAttribute )
            	    // InternalOaas.g:1068:5: lv_attributes_7_0= ruleAttribute
            	    {

            	    					newCompositeNode(grammarAccess.getEntityAccess().getAttributesAttributeParserRuleCall_6_0());
            	    				
            	    pushFollow(FOLLOW_23);
            	    lv_attributes_7_0=ruleAttribute();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEntityRule());
            	    					}
            	    					add(
            	    						current,
            	    						"attributes",
            	    						lv_attributes_7_0,
            	    						"br.ufes.nemo.seon.ontoloyasservice.Oaas.Attribute");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            // InternalOaas.g:1085:3: ( (lv_functions_8_0= ruleFunction ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==22||LA11_0==34) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalOaas.g:1086:4: (lv_functions_8_0= ruleFunction )
            	    {
            	    // InternalOaas.g:1086:4: (lv_functions_8_0= ruleFunction )
            	    // InternalOaas.g:1087:5: lv_functions_8_0= ruleFunction
            	    {

            	    					newCompositeNode(grammarAccess.getEntityAccess().getFunctionsFunctionParserRuleCall_7_0());
            	    				
            	    pushFollow(FOLLOW_23);
            	    lv_functions_8_0=ruleFunction();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEntityRule());
            	    					}
            	    					add(
            	    						current,
            	    						"functions",
            	    						lv_functions_8_0,
            	    						"br.ufes.nemo.seon.ontoloyasservice.Oaas.Function");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            // InternalOaas.g:1104:3: ( (lv_relations_9_0= ruleRelation ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==RULE_ID) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalOaas.g:1105:4: (lv_relations_9_0= ruleRelation )
            	    {
            	    // InternalOaas.g:1105:4: (lv_relations_9_0= ruleRelation )
            	    // InternalOaas.g:1106:5: lv_relations_9_0= ruleRelation
            	    {

            	    					newCompositeNode(grammarAccess.getEntityAccess().getRelationsRelationParserRuleCall_8_0());
            	    				
            	    pushFollow(FOLLOW_24);
            	    lv_relations_9_0=ruleRelation();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEntityRule());
            	    					}
            	    					add(
            	    						current,
            	    						"relations",
            	    						lv_relations_9_0,
            	    						"br.ufes.nemo.seon.ontoloyasservice.Oaas.Relation");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            otherlv_10=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getEntityAccess().getRightCurlyBracketKeyword_9());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEntity"


    // $ANTLR start "entryRuleAttribute"
    // InternalOaas.g:1131:1: entryRuleAttribute returns [EObject current=null] : iv_ruleAttribute= ruleAttribute EOF ;
    public final EObject entryRuleAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttribute = null;


        try {
            // InternalOaas.g:1131:50: (iv_ruleAttribute= ruleAttribute EOF )
            // InternalOaas.g:1132:2: iv_ruleAttribute= ruleAttribute EOF
            {
             newCompositeNode(grammarAccess.getAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttribute=ruleAttribute();

            state._fsp--;

             current =iv_ruleAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // InternalOaas.g:1138:1: ruleAttribute returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= RULE_ID ) ) ) ;
    public final EObject ruleAttribute() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_type_3_0=null;
        EObject lv_description_0_0 = null;



        	enterRule();

        try {
            // InternalOaas.g:1144:2: ( ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= RULE_ID ) ) ) )
            // InternalOaas.g:1145:2: ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= RULE_ID ) ) )
            {
            // InternalOaas.g:1145:2: ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= RULE_ID ) ) )
            // InternalOaas.g:1146:3: ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= RULE_ID ) )
            {
            // InternalOaas.g:1146:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==22) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalOaas.g:1147:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalOaas.g:1147:4: (lv_description_0_0= ruleDescription )
                    // InternalOaas.g:1148:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getAttributeAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_16);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getAttributeRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.nemo.seon.ontoloyasservice.Oaas.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalOaas.g:1165:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalOaas.g:1166:4: (lv_name_1_0= RULE_ID )
            {
            // InternalOaas.g:1166:4: (lv_name_1_0= RULE_ID )
            // InternalOaas.g:1167:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_25); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAttributeAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAttributeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,30,FOLLOW_16); 

            			newLeafNode(otherlv_2, grammarAccess.getAttributeAccess().getColonKeyword_2());
            		
            // InternalOaas.g:1187:3: ( (lv_type_3_0= RULE_ID ) )
            // InternalOaas.g:1188:4: (lv_type_3_0= RULE_ID )
            {
            // InternalOaas.g:1188:4: (lv_type_3_0= RULE_ID )
            // InternalOaas.g:1189:5: lv_type_3_0= RULE_ID
            {
            lv_type_3_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_type_3_0, grammarAccess.getAttributeAccess().getTypeIDTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAttributeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"type",
            						lv_type_3_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRuleRelation"
    // InternalOaas.g:1209:1: entryRuleRelation returns [EObject current=null] : iv_ruleRelation= ruleRelation EOF ;
    public final EObject entryRuleRelation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRelation = null;


        try {
            // InternalOaas.g:1209:49: (iv_ruleRelation= ruleRelation EOF )
            // InternalOaas.g:1210:2: iv_ruleRelation= ruleRelation EOF
            {
             newCompositeNode(grammarAccess.getRelationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRelation=ruleRelation();

            state._fsp--;

             current =iv_ruleRelation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelation"


    // $ANTLR start "ruleRelation"
    // InternalOaas.g:1216:1: ruleRelation returns [EObject current=null] : (this_OneToOne_0= ruleOneToOne | this_ManyToMany_1= ruleManyToMany | this_OneToMany_2= ruleOneToMany ) ;
    public final EObject ruleRelation() throws RecognitionException {
        EObject current = null;

        EObject this_OneToOne_0 = null;

        EObject this_ManyToMany_1 = null;

        EObject this_OneToMany_2 = null;



        	enterRule();

        try {
            // InternalOaas.g:1222:2: ( (this_OneToOne_0= ruleOneToOne | this_ManyToMany_1= ruleManyToMany | this_OneToMany_2= ruleOneToMany ) )
            // InternalOaas.g:1223:2: (this_OneToOne_0= ruleOneToOne | this_ManyToMany_1= ruleManyToMany | this_OneToMany_2= ruleOneToMany )
            {
            // InternalOaas.g:1223:2: (this_OneToOne_0= ruleOneToOne | this_ManyToMany_1= ruleManyToMany | this_OneToMany_2= ruleOneToMany )
            int alt14=3;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==RULE_ID) ) {
                switch ( input.LA(2) ) {
                case 31:
                    {
                    alt14=1;
                    }
                    break;
                case 33:
                    {
                    alt14=3;
                    }
                    break;
                case 32:
                    {
                    alt14=2;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 14, 1, input);

                    throw nvae;
                }

            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }
            switch (alt14) {
                case 1 :
                    // InternalOaas.g:1224:3: this_OneToOne_0= ruleOneToOne
                    {

                    			newCompositeNode(grammarAccess.getRelationAccess().getOneToOneParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_OneToOne_0=ruleOneToOne();

                    state._fsp--;


                    			current = this_OneToOne_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalOaas.g:1233:3: this_ManyToMany_1= ruleManyToMany
                    {

                    			newCompositeNode(grammarAccess.getRelationAccess().getManyToManyParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ManyToMany_1=ruleManyToMany();

                    state._fsp--;


                    			current = this_ManyToMany_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalOaas.g:1242:3: this_OneToMany_2= ruleOneToMany
                    {

                    			newCompositeNode(grammarAccess.getRelationAccess().getOneToManyParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_OneToMany_2=ruleOneToMany();

                    state._fsp--;


                    			current = this_OneToMany_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelation"


    // $ANTLR start "entryRuleOneToOne"
    // InternalOaas.g:1254:1: entryRuleOneToOne returns [EObject current=null] : iv_ruleOneToOne= ruleOneToOne EOF ;
    public final EObject entryRuleOneToOne() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOneToOne = null;


        try {
            // InternalOaas.g:1254:49: (iv_ruleOneToOne= ruleOneToOne EOF )
            // InternalOaas.g:1255:2: iv_ruleOneToOne= ruleOneToOne EOF
            {
             newCompositeNode(grammarAccess.getOneToOneRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOneToOne=ruleOneToOne();

            state._fsp--;

             current =iv_ruleOneToOne; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOneToOne"


    // $ANTLR start "ruleOneToOne"
    // InternalOaas.g:1261:1: ruleOneToOne returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToOne' ( ( ruleQualifiedName ) ) ) ;
    public final EObject ruleOneToOne() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalOaas.g:1267:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToOne' ( ( ruleQualifiedName ) ) ) )
            // InternalOaas.g:1268:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToOne' ( ( ruleQualifiedName ) ) )
            {
            // InternalOaas.g:1268:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToOne' ( ( ruleQualifiedName ) ) )
            // InternalOaas.g:1269:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToOne' ( ( ruleQualifiedName ) )
            {
            // InternalOaas.g:1269:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalOaas.g:1270:4: (lv_name_0_0= RULE_ID )
            {
            // InternalOaas.g:1270:4: (lv_name_0_0= RULE_ID )
            // InternalOaas.g:1271:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_26); 

            					newLeafNode(lv_name_0_0, grammarAccess.getOneToOneAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOneToOneRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,31,FOLLOW_16); 

            			newLeafNode(otherlv_1, grammarAccess.getOneToOneAccess().getOneToOneKeyword_1());
            		
            // InternalOaas.g:1291:3: ( ( ruleQualifiedName ) )
            // InternalOaas.g:1292:4: ( ruleQualifiedName )
            {
            // InternalOaas.g:1292:4: ( ruleQualifiedName )
            // InternalOaas.g:1293:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOneToOneRule());
            					}
            				

            					newCompositeNode(grammarAccess.getOneToOneAccess().getTypeEntityCrossReference_2_0());
            				
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOneToOne"


    // $ANTLR start "entryRuleManyToMany"
    // InternalOaas.g:1311:1: entryRuleManyToMany returns [EObject current=null] : iv_ruleManyToMany= ruleManyToMany EOF ;
    public final EObject entryRuleManyToMany() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleManyToMany = null;


        try {
            // InternalOaas.g:1311:51: (iv_ruleManyToMany= ruleManyToMany EOF )
            // InternalOaas.g:1312:2: iv_ruleManyToMany= ruleManyToMany EOF
            {
             newCompositeNode(grammarAccess.getManyToManyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleManyToMany=ruleManyToMany();

            state._fsp--;

             current =iv_ruleManyToMany; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleManyToMany"


    // $ANTLR start "ruleManyToMany"
    // InternalOaas.g:1318:1: ruleManyToMany returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToMany' ( ( ruleQualifiedName ) ) ) ;
    public final EObject ruleManyToMany() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalOaas.g:1324:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToMany' ( ( ruleQualifiedName ) ) ) )
            // InternalOaas.g:1325:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToMany' ( ( ruleQualifiedName ) ) )
            {
            // InternalOaas.g:1325:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToMany' ( ( ruleQualifiedName ) ) )
            // InternalOaas.g:1326:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToMany' ( ( ruleQualifiedName ) )
            {
            // InternalOaas.g:1326:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalOaas.g:1327:4: (lv_name_0_0= RULE_ID )
            {
            // InternalOaas.g:1327:4: (lv_name_0_0= RULE_ID )
            // InternalOaas.g:1328:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_27); 

            					newLeafNode(lv_name_0_0, grammarAccess.getManyToManyAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getManyToManyRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,32,FOLLOW_16); 

            			newLeafNode(otherlv_1, grammarAccess.getManyToManyAccess().getManyToManyKeyword_1());
            		
            // InternalOaas.g:1348:3: ( ( ruleQualifiedName ) )
            // InternalOaas.g:1349:4: ( ruleQualifiedName )
            {
            // InternalOaas.g:1349:4: ( ruleQualifiedName )
            // InternalOaas.g:1350:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getManyToManyRule());
            					}
            				

            					newCompositeNode(grammarAccess.getManyToManyAccess().getTypeEntityCrossReference_2_0());
            				
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleManyToMany"


    // $ANTLR start "entryRuleOneToMany"
    // InternalOaas.g:1368:1: entryRuleOneToMany returns [EObject current=null] : iv_ruleOneToMany= ruleOneToMany EOF ;
    public final EObject entryRuleOneToMany() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOneToMany = null;


        try {
            // InternalOaas.g:1368:50: (iv_ruleOneToMany= ruleOneToMany EOF )
            // InternalOaas.g:1369:2: iv_ruleOneToMany= ruleOneToMany EOF
            {
             newCompositeNode(grammarAccess.getOneToManyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOneToMany=ruleOneToMany();

            state._fsp--;

             current =iv_ruleOneToMany; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOneToMany"


    // $ANTLR start "ruleOneToMany"
    // InternalOaas.g:1375:1: ruleOneToMany returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToMany' ( ( ruleQualifiedName ) ) ) ;
    public final EObject ruleOneToMany() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalOaas.g:1381:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToMany' ( ( ruleQualifiedName ) ) ) )
            // InternalOaas.g:1382:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToMany' ( ( ruleQualifiedName ) ) )
            {
            // InternalOaas.g:1382:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToMany' ( ( ruleQualifiedName ) ) )
            // InternalOaas.g:1383:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToMany' ( ( ruleQualifiedName ) )
            {
            // InternalOaas.g:1383:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalOaas.g:1384:4: (lv_name_0_0= RULE_ID )
            {
            // InternalOaas.g:1384:4: (lv_name_0_0= RULE_ID )
            // InternalOaas.g:1385:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_28); 

            					newLeafNode(lv_name_0_0, grammarAccess.getOneToManyAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOneToManyRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,33,FOLLOW_16); 

            			newLeafNode(otherlv_1, grammarAccess.getOneToManyAccess().getOneToManyKeyword_1());
            		
            // InternalOaas.g:1405:3: ( ( ruleQualifiedName ) )
            // InternalOaas.g:1406:4: ( ruleQualifiedName )
            {
            // InternalOaas.g:1406:4: ( ruleQualifiedName )
            // InternalOaas.g:1407:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOneToManyRule());
            					}
            				

            					newCompositeNode(grammarAccess.getOneToManyAccess().getTypeEntityCrossReference_2_0());
            				
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOneToMany"


    // $ANTLR start "entryRuleFunction"
    // InternalOaas.g:1425:1: entryRuleFunction returns [EObject current=null] : iv_ruleFunction= ruleFunction EOF ;
    public final EObject entryRuleFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunction = null;


        try {
            // InternalOaas.g:1425:49: (iv_ruleFunction= ruleFunction EOF )
            // InternalOaas.g:1426:2: iv_ruleFunction= ruleFunction EOF
            {
             newCompositeNode(grammarAccess.getFunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFunction=ruleFunction();

            state._fsp--;

             current =iv_ruleFunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunction"


    // $ANTLR start "ruleFunction"
    // InternalOaas.g:1432:1: ruleFunction returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )* )? otherlv_7= ')' otherlv_8= ':' ( (lv_type_9_0= RULE_ID ) ) ) ;
    public final EObject ruleFunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token lv_params_4_0=null;
        Token otherlv_5=null;
        Token lv_params_6_0=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token lv_type_9_0=null;
        EObject lv_description_0_0 = null;



        	enterRule();

        try {
            // InternalOaas.g:1438:2: ( ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )* )? otherlv_7= ')' otherlv_8= ':' ( (lv_type_9_0= RULE_ID ) ) ) )
            // InternalOaas.g:1439:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )* )? otherlv_7= ')' otherlv_8= ':' ( (lv_type_9_0= RULE_ID ) ) )
            {
            // InternalOaas.g:1439:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )* )? otherlv_7= ')' otherlv_8= ':' ( (lv_type_9_0= RULE_ID ) ) )
            // InternalOaas.g:1440:3: ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )* )? otherlv_7= ')' otherlv_8= ':' ( (lv_type_9_0= RULE_ID ) )
            {
            // InternalOaas.g:1440:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==22) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalOaas.g:1441:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalOaas.g:1441:4: (lv_description_0_0= ruleDescription )
                    // InternalOaas.g:1442:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getFunctionAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_29);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getFunctionRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.nemo.seon.ontoloyasservice.Oaas.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,34,FOLLOW_16); 

            			newLeafNode(otherlv_1, grammarAccess.getFunctionAccess().getFunctionKeyword_1());
            		
            // InternalOaas.g:1463:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalOaas.g:1464:4: (lv_name_2_0= RULE_ID )
            {
            // InternalOaas.g:1464:4: (lv_name_2_0= RULE_ID )
            // InternalOaas.g:1465:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_30); 

            					newLeafNode(lv_name_2_0, grammarAccess.getFunctionAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFunctionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,35,FOLLOW_31); 

            			newLeafNode(otherlv_3, grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_3());
            		
            // InternalOaas.g:1485:3: ( ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )* )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==RULE_ID) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalOaas.g:1486:4: ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )*
                    {
                    // InternalOaas.g:1486:4: ( (lv_params_4_0= RULE_ID ) )
                    // InternalOaas.g:1487:5: (lv_params_4_0= RULE_ID )
                    {
                    // InternalOaas.g:1487:5: (lv_params_4_0= RULE_ID )
                    // InternalOaas.g:1488:6: lv_params_4_0= RULE_ID
                    {
                    lv_params_4_0=(Token)match(input,RULE_ID,FOLLOW_32); 

                    						newLeafNode(lv_params_4_0, grammarAccess.getFunctionAccess().getParamsIDTerminalRuleCall_4_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getFunctionRule());
                    						}
                    						addWithLastConsumed(
                    							current,
                    							"params",
                    							lv_params_4_0,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }


                    }

                    // InternalOaas.g:1504:4: (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )*
                    loop16:
                    do {
                        int alt16=2;
                        int LA16_0 = input.LA(1);

                        if ( (LA16_0==36) ) {
                            alt16=1;
                        }


                        switch (alt16) {
                    	case 1 :
                    	    // InternalOaas.g:1505:5: otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) )
                    	    {
                    	    otherlv_5=(Token)match(input,36,FOLLOW_16); 

                    	    					newLeafNode(otherlv_5, grammarAccess.getFunctionAccess().getCommaKeyword_4_1_0());
                    	    				
                    	    // InternalOaas.g:1509:5: ( (lv_params_6_0= RULE_ID ) )
                    	    // InternalOaas.g:1510:6: (lv_params_6_0= RULE_ID )
                    	    {
                    	    // InternalOaas.g:1510:6: (lv_params_6_0= RULE_ID )
                    	    // InternalOaas.g:1511:7: lv_params_6_0= RULE_ID
                    	    {
                    	    lv_params_6_0=(Token)match(input,RULE_ID,FOLLOW_32); 

                    	    							newLeafNode(lv_params_6_0, grammarAccess.getFunctionAccess().getParamsIDTerminalRuleCall_4_1_1_0());
                    	    						

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getFunctionRule());
                    	    							}
                    	    							addWithLastConsumed(
                    	    								current,
                    	    								"params",
                    	    								lv_params_6_0,
                    	    								"org.eclipse.xtext.common.Terminals.ID");
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop16;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_7=(Token)match(input,37,FOLLOW_25); 

            			newLeafNode(otherlv_7, grammarAccess.getFunctionAccess().getRightParenthesisKeyword_5());
            		
            otherlv_8=(Token)match(input,30,FOLLOW_16); 

            			newLeafNode(otherlv_8, grammarAccess.getFunctionAccess().getColonKeyword_6());
            		
            // InternalOaas.g:1537:3: ( (lv_type_9_0= RULE_ID ) )
            // InternalOaas.g:1538:4: (lv_type_9_0= RULE_ID )
            {
            // InternalOaas.g:1538:4: (lv_type_9_0= RULE_ID )
            // InternalOaas.g:1539:5: lv_type_9_0= RULE_ID
            {
            lv_type_9_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_type_9_0, grammarAccess.getFunctionAccess().getTypeIDTerminalRuleCall_7_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFunctionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"type",
            						lv_type_9_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunction"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000015400002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000015402000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000008000002L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000020001000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000400402020L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000002020L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000002000000020L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000003000000000L});

}