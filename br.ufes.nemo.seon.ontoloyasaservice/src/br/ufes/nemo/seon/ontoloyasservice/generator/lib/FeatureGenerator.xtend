package br.ufes.nemo.seon.ontoloyasservice.generator.lib

import br.ufes.nemo.seon.ontoloyasservice.oaas.Entity
import com.google.inject.Inject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import org.eclipse.xtext.naming.IQualifiedNameProvider

class FeatureGenerator extends AbstractGenerator {
	@Inject extension IQualifiedNameProvider
	
	var PATH_LIB_FEATURE = "lib/src/"
	
		
	override doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		
		for (e : resource.allContents.toIterable.filter(Entity)) {
			fsa.generateFile(
				PATH_LIB_FEATURE + "features" + "/" + e.fullyQualifiedName.toString("/").toLowerCase + ".feature",
				e.feature
			)
			
			fsa.generateFile(
				PATH_LIB_FEATURE + "features/steps/" + e.fullyQualifiedName.toString("/").toLowerCase + ".py",
				e.step
			)
		}
	}
	
	def feature(Entity e) '''
		Feature: Commmon Cases of «e.name»
		    
		    Scenario: Create a «e.name»
		    Given a organization with  «e.name»
		    When i request all «e.name»
		    Then i get a list of «e.name»s
		
		    #Fail test - just testing if the organization is wrong
		    Scenario: fail get all «e.name»
		    Given a false organization with  «e.name»
		    Then i fail in get a list of «e.name»s
		
	'''
	def step(Entity e) '''
		from jira_sro_etl import factories
		from behave import given, when, then
		
		global data
		global «e.name»
		
		@given(u'a organization with  «e.name»')
		def step_impl(context):
		    global data
		    user = "<USER>"
		    apikey = "<TEXT>"
		    server =  '<URL>
		    #Stairway to heaven project
		    data = {
		        'user': user,
		        'key': apikey,
		        'url': server
		    }
		
		
		@when(u'When i request all «e.name»')
		def step_impl(context):
		    global «e.name»
		    «e.name» = factories.«e.name»Factory()
		
		
		@then(u'i get a list of «e.name»s')
		def step_impl(context):
		    assert «e.name».do(data)
		
		#------Fail-----
		@given(u'a false organization with  «e.name»')
		def step_impl(context):
		    global data
		    «e.name» = "<EMAIL>"
		    apikey = "<KEY>"
		    server =  '<Serer>
		    
		    data = {
		        '«e.name»': «e.name»,
		        'key': apikey,
		        'url': server
		    }
		
		@then(u'i fail in get a list of «e.name»s')
		def step_impl(context):
		   try:       assert «e.name».do(data)
		   except Exception as e:
		       pass
		
		
		
	'''
}
