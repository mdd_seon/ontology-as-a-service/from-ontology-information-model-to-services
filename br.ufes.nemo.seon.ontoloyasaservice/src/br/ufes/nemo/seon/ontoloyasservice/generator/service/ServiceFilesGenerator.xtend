package br.ufes.nemo.seon.ontoloyasservice.generator.service

import br.ufes.nemo.seon.ontoloyasservice.oaas.Entity
import java.util.List
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext

class ServiceFilesGenerator extends AbstractGenerator {
	
	
	var PATHSERVICE = "service/src/"
	
	override doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		
		fsa.generateFile(PATHSERVICE +"pyproject.toml", createPyproject)
		fsa.generateFile(PATHSERVICE +"README.md", createReadme)
		fsa.generateFile(PATHSERVICE +"requirementes.txt", createRequirements)
		fsa.generateFile(PATHSERVICE +"run.sh", createRun)
		fsa.generateFile(PATHSERVICE +"VERSION.txt", createVersion)
		fsa.generateFile(PATHSERVICE +".gitignore", gitignore)
		
		fsa.generateFile(PATHSERVICE +"server.py", resource.allContents.toIterable.filter(Entity).toList.createServer)
		
	}
	
	private def createServer(List<Entity> entities)'''
	from pprint import pprint
	«FOR e: entities»
	from server.«e.name.toLowerCase»_server import «e.name.toFirstUpper»Server
	«ENDFOR»
	def main():
		pprint("EO: Begin")
		
	«FOR e: entities»
		«e.name.toLowerCase»_server = «e.name.toFirstUpper»Server()
		«e.name.toLowerCase»_server.start()
	«ENDFOR»	
	
		pprint("EO: End")
	
	if __name__ == "__main__":
		main()
	'''
	
	private def createPyproject()'''
		[tool.commitizen]
		name = "cz_conventional_commits"
		version = "0.0.0"
		tag_format = "v$version"
		bump_message = "release $current_version → $new_version"
		version_files = [
		    "pyproject.toml:version",
		    "VERSION.txt"
		]
		
		style = [
		    ["qmark", "fg:#ff9d00 bold"],
		    ["question", "bold"],
		    ["answer", "fg:#ff9d00 bold"],
		    ["pointer", "fg:#ff9d00 bold"],
		    ["highlighted", "fg:#ff9d00 bold"],
		    ["selected", "fg:#cc5454"],
		    ["separator", "fg:#cc5454"],
		    ["instruction", ""],
		    ["text", ""],
		    ["disabled", "fg:#858585 italic"]
		]
	'''
	
	private def createReadme()'''
		#readme
	''' 
	
	private def createRequirements()'''
		colorama==0.4.3
		commitizen==1.23.0
		decli==0.5.1
		xxxxxxxx
		factory-boy==2.12.0
		Faker==4.1.1
		Jinja2==2.11.2
		MarkupSafe==1.1.1
		packaging==20.4
		pika==1.1.0
		prompt-toolkit==3.0.5
		psycopg2==2.8.5
		pyparsing==2.4.7
		python-dateutil==2.8.1
		questionary==1.5.2
		rabbitmqX==1.10.18
		six==1.15.0
		SQLAlchemy==1.3.18
		SQLAlchemy-serializer==1.3.4.2
		SQLAlchemy-Utils==0.36.8
		termcolor==1.1.0
		text-unidecode==1.3
		tomlkit==0.5.11
		wcwidth==0.2.5
	'''
	
	private def createRun()'''
		python server.py
	'''
	
	private def createVersion()'''
		"v0.0.0"
	'''
	private def gitignore()'''
		# Byte-compiled / optimized / DLL files
		__pycache__/
		*.py[cod]
		*$py.class
		
		# C extensions
		*.so
		
		# Distribution / packaging
		.Python
		build/
		develop-eggs/
		dist/
		downloads/
		eggs/
		.eggs/
		lib/
		lib64/
		parts/
		sdist/
		var/
		wheels/
		share/python-wheels/
		*.egg-info/
		.installed.cfg
		*.egg
		MANIFEST
		
		# PyInstaller
		#  Usually these files are written by a python script from a template
		#  before PyInstaller builds the exe, so as to inject date/other infos into it.
		*.manifest
		*.spec
		
		# Installer logs
		pip-log.txt
		pip-delete-this-directory.txt
		
		# Unit test / coverage reports
		htmlcov/
		.tox/
		.nox/
		.coverage
		.coverage.*
		.cache
		nosetests.xml
		coverage.xml
		*.cover
		.hypothesis/
		.pytest_cache/
		
		# Translations
		*.mo
		*.pot
		
		# Django stuff:
		*.log
		local_settings.py
		db.sqlite3
		
		# Flask stuff:
		instance/
		.webassets-cache
		
		# Scrapy stuff:
		.scrapy
		
		# Sphinx documentation
		docs/_build/
		
		# PyBuilder
		target/
		
		# Jupyter Notebook
		.ipynb_checkpoints
		
		# IPython
		profile_default/
		ipython_config.py
		
		# pyenv
		.python-version
		
		# celery beat schedule file
		celerybeat-schedule
		
		# SageMath parsed files
		*.sage.py
		
		# Environments
		.env
		.venv
		env/
		venv/
		ENV/
		env.bak/
		venv.bak/
		
		# Spyder project settings
		.spyderproject
		.spyproject
		
		# Rope project settings
		.ropeproject
		
		# mkdocs documentation
		/site
		
		# mypy
		.mypy_cache/
		.dmypy.json
		dmypy.json
		
		# Pyre type checker
		.pyre/
	
	'''
}