package br.ufes.nemo.seon.ontoloyasservice.generator.service

import br.ufes.nemo.seon.ontoloyasservice.oaas.Attribute
import br.ufes.nemo.seon.ontoloyasservice.oaas.Configuration
import br.ufes.nemo.seon.ontoloyasservice.oaas.Entity
import br.ufes.nemo.seon.ontoloyasservice.oaas.OneToMany
import br.ufes.nemo.seon.ontoloyasservice.oaas.OneToOne
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext

class ServiceGenerator extends AbstractGenerator {
	
	var PATHSERVICE = "service/src/"
	
	override doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		
		var String ontology = "";
		var String level = "";
		
		for (configuration: resource.allContents.toIterable.filter(Configuration)){
			
			ontology = configuration.ontologyShortName.name.toLowerCase;
			level = configuration.level.name.toLowerCase;
		}
		
		for (entity : resource.allContents.toIterable.filter(Entity)){
			fsa.generateFile(PATHSERVICE +"server/"+entity.name.toLowerCase+"_server.py", entity.createServer(ontology))
			fsa.generateFile(PATHSERVICE +"service/"+entity.name.toLowerCase+"_service.py", entity.createService(ontology))
		}
		fsa.generateFile(PATHSERVICE +"server/__init__.py", "")
		fsa.generateFile(PATHSERVICE +"server/server_abstract.py", createAbstractServer)
		fsa.generateFile(PATHSERVICE +"service/__init__.py","")
		
		if (!ontology.isNullOrEmpty && !level.isNullOrEmpty){
			fsa.generateFile(PATHSERVICE +"service/service_abstract.py", createAbstractService(ontology,level))	
		}
	}
	
	private def createAbstractServer()'''
		from rabbitmqX.patterns.server.topic_server import Topic_Server
		
		class ServerAbstract():
		
		    def __init__(self, queue_name, service):
		        self.topic_server = Topic_Server(queue_name,service)
		        
		    def start(self):
		        self.topic_server.start()
	'''
	
	private def createAbstractService(String ontology, String level)'''
		import logging
		from pprint import pprint
		class ServiceAbstract():
		
		    def do(self,journal):
		        
		        try:
		            data = journal['data']
		            if data["ontology"] != "«ontology»" and data["level"] != "«level»":
		                if journal['action'] == 'create':
		                    self.create(data)
		                elif journal['action'] == 'update':
		                    self.update(data)
		                else:
		                    self.delete(data)
		
		        except Exception as e:
		            logging.error("OS error: {0}".format(e))
		            logging.error(e.__dict__)
		    
		    def create(self,data):
		        pass
		    
		    def update(self,data):
		        pass
		    
		    def delete(self,data):
		        pass
	'''
	
	private def createServer(Entity entity, String ontology)'''
		from .server_abstract import ServerAbstract
		from service.«entity.name.toLowerCase»_service import «entity.name.toFirstUpper»Service
		from pprint import pprint
		
		class «entity.name.toFirstUpper»Server(ServerAbstract):
		
		    def __init__(self):
				queue_name = "domain.*.«ontology.toLowerCase».«entity.name.toLowerCase»"
				service = «entity.name.toFirstUpper»Service()
				pprint("«entity.name.toFirstUpper» Server")
				super().__init__(queue_name=queue_name, service=service)
	'''
	
	private def createAtribute(Attribute attribute, String entityName)'''
		«entityName.toLowerCase».«attribute.name.toLowerCase» = data['«attribute.name.toLowerCase»']
	'''
	
	private def createRelation(OneToOne oneToOne, Entity entity)'''
		«oneToOne.name.toLowerCase»_uuid = data['«oneToOne.name.toLowerCase»']['uuid_']
		if «oneToOne.name.toLowerCase»_uuid is not None:
			«oneToOne.type.name.toLowerCase»_application = application_factory.«oneToOne.type.name»Factory()
			«oneToOne.type.name.toLowerCase» = «oneToOne.type.name.toLowerCase»_application.find_by_uuid(«oneToOne.name.toLowerCase»_uuid)
			«entity.name.toLowerCase».«oneToOne.name.toLowerCase» = «oneToOne.type.name.toLowerCase» 
	'''
	
	private def createRelation(OneToMany oneToMany, Entity entity)'''
		«oneToMany.name.toLowerCase»_array = data['«oneToMany.name.toLowerCase»']
		
		if «oneToMany.name.toLowerCase»_array is not None:
			«oneToMany.type.name.toLowerCase»_application = application_factory.«oneToMany.type.name»Factory()
			for «oneToMany.name.toLowerCase» in «oneToMany.name.toLowerCase»_array:
				«oneToMany.name.toLowerCase»_uuid = data['«oneToMany.name.toLowerCase»']['uuid_']
				«oneToMany.type.name.toLowerCase» = «oneToMany.type.name.toLowerCase»_application.find_by_uuid(«oneToMany.name.toLowerCase»_uuid)
				«entity.name.toLowerCase».«oneToMany.name.toLowerCase».append(«oneToMany.type.name.toLowerCase») 
	'''
	
	private def createAttributes(Entity entity)'''
		«FOR attribute : entity.attributes»
			«attribute.createAtribute(entity.name.toLowerCase)»
		«ENDFOR»
	'''
	
	private def createRelations(Entity entity)'''
		«FOR relationship : entity.relations»
			«IF relationship.eClass.instanceClass == OneToOne»
				« val OneToOne onetoone = relationship as OneToOne»
				«onetoone.createRelation(entity)»
			«ENDIF»
			«IF relationship.eClass.instanceClass == OneToMany»
				« val OneToMany onetoMany = relationship as OneToMany»
				«onetoMany.createRelation(entity)»
			«ENDIF»
		«ENDFOR»
	'''
	
	private def createService(Entity entity, String ontology)'''
		import logging
		from pprint import pprint
		
		from «ontology.toLowerCase»_lib.application import factories as application_factory
		from «ontology.toLowerCase»_lib.model import factories as model_factory
		from service.service_abstract import ServiceAbstract
		logging.basicConfig(format='%(process)d-%(levelname)s-%(message)s')
		
		class «entity.name.toFirstUpper»Service(ServiceAbstract):
		
		    def create(self,data):
		        try:
		            pprint ("«entity.name.toFirstUpper» Service") 
		            pprint ("Create")
		
		            «entity.name.toLowerCase» = model_factory.«entity.name.toFirstUpper»Factory()
		            «entity.name.toLowerCase».name = data['name']
		            «entity.name.toLowerCase».uuid = data['uuid']
		            «entity.name.toLowerCase».description = data['description']
		            «entity.createAttributes»
		            
		            «entity.createRelations»
		            
		            «entity.name.toLowerCase»_application = application_factory.«entity.name.toFirstUpper»Factory()
		            «entity.name.toLowerCase»_application.create («entity.name.toLowerCase»)
		            
		            pprint ("Created")
		            
		        except Exception as e:
		            logging.error("OS error: {0}".format(e))
		            logging.error(e.__dict__)
		            
		    def update(self,data):
		        try:
		            pprint ("«entity.name.toFirstUpper» Service")  
		            pprint ("Update")
		
		            «entity.name.toLowerCase»_application = application_factory.«entity.name.toFirstUpper»Factory()
		            uuid = data['uuid']
		            «entity.name.toLowerCase» = «entity.name.toLowerCase»_application.find_by_uuid(uuid)
		            
		            «entity.name.toLowerCase».name = data['name']
					«entity.name.toLowerCase».description = data['description']
					«entity.createAttributes»
					
					«entity.createRelations»
					
		            «entity.name.toLowerCase»_application.update («entity.name.toLowerCase»)
		
		            pprint ("«entity.name.toFirstUpper»  Service: Updated")
		        except Exception as e:
		            logging.error("OS error: {0}".format(e))
		            logging.error(e.__dict__)
		
		    def delete(self, data):
		        try:
		            pprint ("«entity.name.toFirstUpper» Service")  
		            pprint ("Delete")   
		            
		            «entity.name.toLowerCase»_application = application_factory.«entity.name.toFirstUpper»Factory()
		            
		            uuid = data['uuid']
		            «entity.name.toLowerCase»_application.delete(uuid)
		            
		            pprint ("«entity.name.toFirstUpper» Service": Deleted")
		        except Exception as e:
		            logging.error("OS error: {0}".format(e))
		            logging.error(e.__dict__)

	''' 
	
}