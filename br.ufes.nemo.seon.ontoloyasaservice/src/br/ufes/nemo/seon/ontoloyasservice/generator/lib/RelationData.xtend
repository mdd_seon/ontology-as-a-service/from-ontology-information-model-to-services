package br.ufes.nemo.seon.ontoloyasservice.generator.lib

import org.eclipse.xtend.lib.annotations.Data

@Data
class RelationData {
	
	String relationsthipname
	String sourceClass
	String targetClass
	
}
