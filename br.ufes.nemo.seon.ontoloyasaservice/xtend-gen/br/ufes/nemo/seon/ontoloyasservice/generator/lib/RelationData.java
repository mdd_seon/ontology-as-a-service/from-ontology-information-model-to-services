package br.ufes.nemo.seon.ontoloyasservice.generator.lib;

import org.eclipse.xtend.lib.annotations.Data;
import org.eclipse.xtext.xbase.lib.Pure;
import org.eclipse.xtext.xbase.lib.util.ToStringBuilder;

@Data
@SuppressWarnings("all")
public class RelationData {
  private final String relationsthipname;
  
  private final String sourceClass;
  
  private final String targetClass;
  
  public RelationData(final String relationsthipname, final String sourceClass, final String targetClass) {
    super();
    this.relationsthipname = relationsthipname;
    this.sourceClass = sourceClass;
    this.targetClass = targetClass;
  }
  
  @Override
  @Pure
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((this.relationsthipname== null) ? 0 : this.relationsthipname.hashCode());
    result = prime * result + ((this.sourceClass== null) ? 0 : this.sourceClass.hashCode());
    return prime * result + ((this.targetClass== null) ? 0 : this.targetClass.hashCode());
  }
  
  @Override
  @Pure
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    RelationData other = (RelationData) obj;
    if (this.relationsthipname == null) {
      if (other.relationsthipname != null)
        return false;
    } else if (!this.relationsthipname.equals(other.relationsthipname))
      return false;
    if (this.sourceClass == null) {
      if (other.sourceClass != null)
        return false;
    } else if (!this.sourceClass.equals(other.sourceClass))
      return false;
    if (this.targetClass == null) {
      if (other.targetClass != null)
        return false;
    } else if (!this.targetClass.equals(other.targetClass))
      return false;
    return true;
  }
  
  @Override
  @Pure
  public String toString() {
    ToStringBuilder b = new ToStringBuilder(this);
    b.add("relationsthipname", this.relationsthipname);
    b.add("sourceClass", this.sourceClass);
    b.add("targetClass", this.targetClass);
    return b.toString();
  }
  
  @Pure
  public String getRelationsthipname() {
    return this.relationsthipname;
  }
  
  @Pure
  public String getSourceClass() {
    return this.sourceClass;
  }
  
  @Pure
  public String getTargetClass() {
    return this.targetClass;
  }
}
