package br.ufes.nemo.seon.ontoloyasservice.generator.webservice;

import br.ufes.nemo.seon.ontoloyasservice.oaas.Attribute;
import br.ufes.nemo.seon.ontoloyasservice.oaas.Configuration;
import br.ufes.nemo.seon.ontoloyasservice.oaas.Description;
import br.ufes.nemo.seon.ontoloyasservice.oaas.Entity;
import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.AbstractGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGeneratorContext;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class WebServiceGenerator extends AbstractGenerator {
  private String PATHWEBSERVICE = "webservice/src/";
  
  @Override
  public void doGenerate(final Resource resource, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
    final List<Configuration> configurations = IterableExtensions.<Configuration>toList(Iterables.<Configuration>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), Configuration.class));
    final Configuration configuration = configurations.get(0);
    final String lib_name = configuration.getOntologyShortName().getName().toLowerCase();
    fsa.generateFile((this.PATHWEBSERVICE + "config.py"), this.createConfigFile());
    fsa.generateFile((this.PATHWEBSERVICE + "run.py"), this.createRun());
    fsa.generateFile((this.PATHWEBSERVICE + "run.sh"), this.createRunScript());
    List<Entity> entities = IterableExtensions.<Entity>toList(Iterables.<Entity>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), Entity.class));
    fsa.generateFile((this.PATHWEBSERVICE + "app/__init__.py"), this.configurationInit(entities, configuration));
    for (final Entity e : entities) {
      {
        String _lowerCase = e.getName().toLowerCase();
        String _plus = ((this.PATHWEBSERVICE + "app/") + _lowerCase);
        String _plus_1 = (_plus + "/controllers.py");
        fsa.generateFile(_plus_1, this.createEntityRestful(e, lib_name));
        String _lowerCase_1 = e.getName().toLowerCase();
        String _plus_2 = ((this.PATHWEBSERVICE + "app/") + _lowerCase_1);
        String _plus_3 = (_plus_2 + "/__init__.py");
        fsa.generateFile(_plus_3, "");
      }
    }
  }
  
  public CharSequence createEntityRestful(final Entity entity, final String lib_name) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from flask import request, jsonify");
    _builder.newLine();
    _builder.append("from flask_restplus import Resource, fields");
    _builder.newLine();
    _builder.append("import json");
    _builder.newLine();
    _builder.append("from app import api");
    _builder.newLine();
    _builder.append("from ");
    _builder.append(lib_name);
    _builder.append(".application import factories");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    String _lowerCase = entity.getName().toLowerCase();
    _builder.append(_lowerCase);
    _builder.append("_model = api.model(\'");
    String _firstUpper = StringExtensions.toFirstUpper(entity.getName());
    _builder.append(_firstUpper);
    _builder.append("\', {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("\'uuid\': field.String,");
    _builder.newLine();
    {
      EList<Attribute> _attributes = entity.getAttributes();
      for(final Attribute attribute : _attributes) {
        _builder.append("\'");
        String _name = attribute.getName();
        _builder.append(_name);
        _builder.append("\': fields.");
        String _type = attribute.getType();
        _builder.append(_type);
        _builder.append(",");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      Entity _superType = entity.getSuperType();
      boolean _tripleNotEquals = (_superType != null);
      if (_tripleNotEquals) {
        {
          EList<Attribute> _attributes_1 = entity.getSuperType().getAttributes();
          for(final Attribute attribute_1 : _attributes_1) {
            _builder.append("\'");
            String _name_1 = attribute_1.getName();
            _builder.append(_name_1);
            _builder.append("\': fields.");
            String _type_1 = attribute_1.getType();
            _builder.append(_type_1);
            _builder.append(",");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.append("})");
    _builder.newLine();
    _builder.newLine();
    String _lowerCase_1 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_1);
    _builder.append("_namespace = api.namespace(\'");
    String _name_2 = entity.getName();
    _builder.append(_name_2);
    _builder.append("\', description=\'");
    {
      Description _description = entity.getDescription();
      boolean _notEquals = (!Objects.equal(_description, null));
      if (_notEquals) {
        String _textfield = entity.getDescription().getTextfield();
        _builder.append(_textfield);
      }
    }
    _builder.append("\')");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("@");
    String _lowerCase_2 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_2);
    _builder.append("_namespace.route(\'/\')");
    _builder.newLineIfNotEmpty();
    _builder.append("class ControllerGetPut(Resource):");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("def __init__(self, *args, **kwargs):");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("self.model_application = factories.");
    String _name_3 = entity.getName();
    _builder.append(_name_3, "\t\t");
    _builder.append("Factory()");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@api.response(200, \'Success\')");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("def get(self,uuid):");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("\"\"\"");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("Get  a ");
    String _lowerCase_3 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_3, "\t\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("\"\"\"        ");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("try:");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("result = self.model_application.find_by_uuid(uuid)");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("return jsonify(result.to_dict())");
    _builder.newLine();
    _builder.append("\t            ");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("except Exception as identifier:");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("return identifier");
    _builder.newLine();
    _builder.append("\t    ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@api.doc(body=");
    String _lowerCase_4 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_4, "\t");
    _builder.append("_model)");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("@api.response(200, \'Success\', ");
    String _lowerCase_5 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_5, "\t");
    _builder.append("_model)");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("@api.expect(");
    String _lowerCase_6 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_6, "\t");
    _builder.append("_model, validate=False)");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("def put(self,uuid):");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("\"\"\"");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("Udpate a ");
    String _lowerCase_7 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_7, "\t\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("\"\"\"");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("try:");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("result = self.model_application.find_by_uuid(uuid)");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("self.model_application.update(result)");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("return jsonify(result.to_dict())");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("except Exception as identifier:");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("return identifier");
    _builder.newLine();
    _builder.append("\t            ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@api.response(200, \'Success\')      ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("def delete(self,uuid):");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("\"\"\"");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("Delete a ");
    String _lowerCase_8 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_8, "\t\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("\"\"\"");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("try:");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("result = self.model_application.delete(uuid)    ");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("return jsonify(result)");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("except Exception as identifier:");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("return identifier");
    _builder.newLine();
    _builder.newLine();
    _builder.append("@");
    String _lowerCase_9 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_9);
    _builder.append("_namespace.route(\'/<uuid>\')");
    _builder.newLineIfNotEmpty();
    _builder.append("@");
    String _lowerCase_10 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_10);
    _builder.append("_namespace.param(\'uuid\', \'");
    String _name_4 = entity.getName();
    _builder.append(_name_4);
    _builder.append(" UUID\')");
    _builder.newLineIfNotEmpty();
    _builder.append("class ControllerGetListPost(Resource):");
    _builder.newLine();
    _builder.append("    ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("def __init__(self, *args, **kwargs):");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("self.model_application = factories.");
    String _name_5 = entity.getName();
    _builder.append(_name_5, "\t\t");
    _builder.append("Factory()");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@api.response(200, \'Success\', [");
    String _lowerCase_11 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_11, "\t");
    _builder.append("_model])");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("def get(self):");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("\"\"\"");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("Returns list of ");
    String _lowerCase_12 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_12, "\t\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("\"\"\"");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("try:");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("result = self.model_application.find_all()");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("response = []");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("for instance in result:");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("response.append(instance.to_dict())");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("return jsonify(response)");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("except Exception as identifier:");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("return json.dumps(identifier)");
    _builder.newLine();
    _builder.append("    ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@api.response(200, \'Success\', ");
    String _lowerCase_13 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_13, "\t");
    _builder.append("_model)");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("def post(self):");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("\"\"\"");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("Create a new ");
    String _lowerCase_14 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_14, "\t\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("\"\"\"");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("try:");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("model = request.get()");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("result = self.model_application.save(model)");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("return jsonify(result)");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("except Exception as identifier:");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("return json.dumps(identifier)");
    _builder.newLine();
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence configurationInit(final List<Entity> entities, final Configuration configuration) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from flask import Flask, Blueprint");
    _builder.newLine();
    _builder.append("from flask_restplus import Api");
    _builder.newLine();
    _builder.newLine();
    _builder.append("app = Flask(__name__)");
    _builder.newLine();
    _builder.append("api = Api(app, ");
    _builder.newLine();
    _builder.append("        ");
    _builder.append("version=\'1.0\', ");
    _builder.newLine();
    _builder.append("        ");
    _builder.append("title=\'");
    String _name = configuration.getSoftware().getName();
    _builder.append(_name, "        ");
    _builder.append("\',");
    _builder.newLineIfNotEmpty();
    _builder.append("    \t");
    _builder.append("description=\'");
    String _name_1 = configuration.getAbout().getName();
    _builder.append(_name_1, "    \t");
    _builder.append("\',");
    _builder.newLineIfNotEmpty();
    _builder.append(")");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Configurations");
    _builder.newLine();
    _builder.append("app.config.from_object(\'config\')");
    _builder.newLine();
    _builder.append("blueprint = Blueprint(\'api\', __name__, url_prefix=\'/api\')");
    _builder.newLine();
    _builder.append("api.init_app(blueprint)");
    _builder.newLine();
    _builder.newLine();
    {
      for(final Entity e : entities) {
        _builder.append("from app.");
        String _lowerCase = e.getName().toLowerCase();
        _builder.append(_lowerCase);
        _builder.append(".controllers import ");
        String _lowerCase_1 = e.getName().toLowerCase();
        _builder.append(_lowerCase_1);
        _builder.append("_namespace");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    {
      for(final Entity e_1 : entities) {
        _builder.append("api.add_namespace(");
        String _lowerCase_2 = e_1.getName().toLowerCase();
        _builder.append(_lowerCase_2);
        _builder.append("_namespace)");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("app.register_blueprint(blueprint)");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence createRunScript() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("python run.py");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence createRun() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from app import app");
    _builder.newLine();
    _builder.append("app.run(host=\'0.0.0.0\', port=8090, debug=True)");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence createConfigFile() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("DEBUG = True");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Define the application directory");
    _builder.newLine();
    _builder.append("import os");
    _builder.newLine();
    _builder.append("BASE_DIR = os.path.abspath(os.path.dirname(__file__))  ");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Application threads. A common general assumption is");
    _builder.newLine();
    _builder.append("# using 2 per available processor cores - to handle");
    _builder.newLine();
    _builder.append("# incoming requests using one and performing background");
    _builder.newLine();
    _builder.append("# operations using the other.");
    _builder.newLine();
    _builder.append("THREADS_PER_PAGE = 2");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Enable protection agains *Cross-site Request Forgery (CSRF)*");
    _builder.newLine();
    _builder.append("CSRF_ENABLED     = True");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Use a secure, unique and absolutely secret key for");
    _builder.newLine();
    _builder.append("# signing the data. ");
    _builder.newLine();
    _builder.append("CSRF_SESSION_KEY = \"secret\"");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Secret key for signing cookies");
    _builder.newLine();
    _builder.append("SECRET_KEY = \"secret\"");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    return _builder;
  }
}
