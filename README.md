# From Ontology information Model to services

This Model-Driven Transformation is responsible for transforming the ontology information modes in libraries, Webservice and Services that allow using ontology information modes as a distributed knowledge database. 

## Goal
This MDD plugin is reponsible for creating:
- library: responsible for creating and performing quering in database.
- Webservice: responsible for communication between external environmment and database. 
- Service: responsible for receiving and sending data between services.

## Environment

The plugin was built to perform in **Eclipse Modelling**.

## Usage

Peformes the following steps:
1. Create a java project
2. Create a file .oaas
3. The code will be generated in folder: **src-gen**
4. Enjoy!

```python

Configuration {
	ontology_name:"Enterprise Ontology"
	ontology_short_name:"eo"
	level: "Domain"
	about: "The Enterprise Ontology (EO) aims at establishing a common conceptualization on the Entreprise domain, including organizations, organizational units, people, roles, teams and projects. Currently, it as considered external to SEON."
	software: "EO_Ontology"
	author: "paulossjunior"
	author_email:"paulossjunior@gmail.com"
	repository: "https://gitlab.com/ontologyasaservice/data/core/enterprise-ontology-service"
}

# "Conceptual Model of the Teams."
module Teams {
	
	# "A human Physical Agent."
	entity Person{
		is: "person"
		email: String
		
	}
	# "An Organization "
	entity Organization {
		is: "organization "
		email: String 
		owner OneToOne Person
		people OneToMany Person
	}
	
	# "A Social Role, recognized by the Organization, assigned to Agents when they are hired, included in a team, allocated or  participating in activities.Ex.: System Analyst, Designer, Programmer, Client Organization."
	entity Organization_Role{
		is: "organizational_role"
		teammebership OneToMany TeamMembership
	}
	
	# "Relationship among Team member, organizational Role and team. "
	entity TeamMembership {
		is: "TeamMembership"
		date: Date
	}
	
	# "Social Agent representing a group of people with a defined purpose. Ex.: a Testing team, a Quality Assurance team, a Deployment team."
	entity Team {
		is: "Team"
		teammebership OneToMany TeamMembership
	}
	# "A Team with a project"
	entity ProjectTeam extends Team{
		is: "Team"
	}
	# "A Social Object as a temporary endeavor undertaken to create a unique product, service, or result. Ex.: A project to produce a software on demand."
	entity Project extends Team{
		is: "Project"
		team OneToMany ProjectTeam
	}
}


```
